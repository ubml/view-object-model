package com.inspur.edp.formserver.vmmanager.generatacmpcode.genutils;

import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class VoCommonUtilsGenerator {
    private final GspViewModel viewModel;
    private final GspMetadata metadata;
    private String relativePath;
    private String compAssemblyName;

    public VoCommonUtilsGenerator(GspViewModel viewModel, GspMetadata metadata) {
        this.relativePath = metadata.getRelativePath();
        this.compAssemblyName = viewModel.getGeneratedConfigID().toLowerCase() + ".common";
        this.viewModel = viewModel;
        this.metadata = metadata;
    }

    public void generateCommonUtils() {
        MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);
        ProcessMode mode = service.getProcessMode(relativePath);
        if (mode != ProcessMode.interpretation)
            return;
        //todo 暂时屏蔽 lcm需要提供新的pom添加依赖接口
//        String compModulePath = service.getJavaCompProjectPath(relativePath);
//        File folder =new File(compModulePath);
//        if(folder.exists()==false)
//            return;
//        compAssemblyName=compAssemblyName.replace(".vo.",".");
//        for (IGspCommonObject entityObject:viewModel.getAllObjectList())
//        {
//            new VoEntityUtilsGenerator((GspViewObject) entityObject,compAssemblyName,relativePath).generate();
//        }
    }
}
