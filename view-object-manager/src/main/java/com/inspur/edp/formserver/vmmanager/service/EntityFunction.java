package com.inspur.edp.formserver.vmmanager.service;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;

public interface EntityFunction {
    GspBusinessEntity getBizEntity(String path, String bePkgName, String beId);

    default String getInfo() {
        return "getBizEntity";
    }
}
