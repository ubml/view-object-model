package com.inspur.edp.formserver.vmmanager.util;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.Metadata4Ref;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationRtService;
import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataFilter;
import com.inspur.edp.metadata.rtcustomization.api.entity.SourceTypeEnum;
import com.inspur.edp.metadata.rtcustomization.serverapi.CustomizationRtServerService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SimplifyMetadataUtil {

    private static final Logger logger = LoggerFactory.getLogger(SimplifyMetadataUtil.class);

    public void Simplify() {

        CustomizationRtService customizationRtService = SpringBeanUtils.getBean(CustomizationRtService.class);
        CustomizationRtServerService customizationRtServerService = SpringBeanUtils.getBean(CustomizationRtServerService.class);
        List<String> metadataTypes = new ArrayList<>();
        metadataTypes.add("GSPBusinessEntity");
        metadataTypes.add("GSPViewModel");
        metadataTypes.add("UnifiedDataType");

        List<Metadata4Ref> metadataListByFilter = customizationRtService.getMetadataListByFilter(new MetadataFilter(SourceTypeEnum.MDPKG, metadataTypes));
        metadataListByFilter.forEach(metadata4Ref -> {
            try {
                GspMetadata metadata = customizationRtService.getMetadata(metadata4Ref.getMetadata().getHeader().getId(), false);
                customizationRtServerService.updateMetadata(metadata);
            } catch (Exception e) {
                //ignore
                logger.info("元数据ID为 " + metadata4Ref.getMetadata().getHeader().getId() + " 的元数据精简失败", e);
            }
        });
        logger.info("批量精简元数据完成");
    }
}
