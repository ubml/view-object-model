package com.inspur.edp.formserver.vmmanager.metadatacopy;

import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.util.HandleAssemblyNameUtil;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicParamKeyEnum;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicTypeEnum;
import com.inspur.edp.metadata.rtcustomization.spi.MetadataCopySpi;

import java.util.List;
import java.util.Map;

/**
 * 该类配置在server\config\platform\common\lcm_rtcustomization.json的MetadataCopy节点下，用于复制应用
 * 当前仅支持零代码场景在不同bo下的复制
 *
 * @author Kaixuan Shi
 * @since 2023/4/11
 */
public class VoMetadataCopyProvider implements MetadataCopySpi {
    //todo 低代码复制应使用此类，不再重复创建
    @Override
    public void metadataCopy(Map<MimicParamKeyEnum, String> map, GspMetadata gspMetadata) {
        if (gspMetadata.getContent() == null) {
            return;
        }
        GspViewModel vo = (GspViewModel) gspMetadata.getContent();
        vo.setDotnetGeneratingAssembly(gspMetadata.getHeader().getNameSpace() + ".Vo");
        vo.setGeneratingAssembly(HandleAssemblyNameUtil.convertToJavaPackageName(gspMetadata.getHeader().getNameSpace() + ".Vo"));
        this.erasureI18nResourceInfoPrefix(vo);
    }

    @Override
    public MimicTypeEnum getMimicTypeEnum() {
        return MimicTypeEnum.NO_CODE_APP_COPY;
    }

    private void erasureI18nResourceInfoPrefix(GspViewModel vo) {
        vo.setI18nResourceInfoPrefix(null);
        if (vo.getMainObject() != null) {
            vo.getMainObject().setI18nResourceInfoPrefix(null);
        }
        List<IGspCommonObject> voObjects = vo.getAllObjectList();
        if (voObjects.isEmpty()) {
            return;
        }
        for (IGspCommonObject voObject : voObjects) {
            voObject.setI18nResourceInfoPrefix(null);
        }
    }
}
