/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.compcodebutton;

import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaCommonDeterminationGenerator;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.ActionParamInfo;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionInfo;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.common.ViewModelActionUtil;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import com.inspur.edp.formserver.vmmanager.exception.VoManagerErrorCodes;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaMappedCdpActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.*;
import com.inspur.edp.formserver.vmmanager.service.VmManagerService;
import com.inspur.edp.formserver.vmmanager.util.CheckComUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class CompButton {

    private static final String javaCodeFileExtension = ".java";
    private static final String javaPathInfo = "java\\code\\comp\\src\\main\\java";

    public String getCompPath(ActionParamInfo info, GspViewModel vo, String type, String relativePath, ArrayList<String> actionList, String compAssemblyName) {
        String componentEntityId = "";
        String className = "";
        if (!"VarDeterminations".equals(type)) {
            MappedCdpAction action = (MappedCdpAction) getAction(actionList.get(0), vo, type);
            ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo(action, vo, info.getMetadata());
            className = VmManagerService.getComponentJavaClassName(actionInfo).replace(".", ViewModelUtils.getSeparator());

        } else {
            CommonDetermination dtm = getDtm(actionList.get(0), vo);
            ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo(dtm, vo, info.getMetadata());
            className = VmManagerService.getComponentJavaClassName(actionInfo).replace(".", ViewModelUtils.getSeparator());
        }
//        String classname = VmManagerService.getComponentJavaClassName(vo,componentEntityId).replace(".", ViewModelUtils.getSeparator());
        String path = ViewModelUtils.getCombinePath(javaPathInfo, className + javaCodeFileExtension);
        return path;
    }

    public String getCompCode(ActionParamInfo info, GspViewModel vo, String type, String relativePath, ArrayList<String> actionList, String compAssemblyName) {
        String code = "";
        if (!"VarDeterminations".equals(type)) {
            ViewModelAction action = getAction(actionList.get(0), vo, type);
            code = getActionCode(info, action, vo, compAssemblyName, relativePath, type);
        } else {
            CommonDetermination action = getDtm(actionList.get(0), vo);
            code = getDtmCode(info, action, vo, compAssemblyName, relativePath);
        }
        return code;
    }

    private String getDtmCode(ActionParamInfo info, CommonDetermination action, GspViewModel vo, String compAssemblyName, String relativePath) {
        JavaCommonDeterminationGenerator gen = new JavaCommonDeterminationGenerator(vo, action, compAssemblyName, relativePath);
        if (gen.getIsCommonGenerate()) {
            return gen.generateCommon();
        } else {
            return gen.generateExecute();
        }
    }

    private String getActionCode(ActionParamInfo info, ViewModelAction action, GspViewModel vo, String compAssemblyName, String relativePath, String type) {
        switch (type) {
            case "VMActions":
                JavaMappedCdpActionGenerator gen = new JavaMappedCdpActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (gen.getIsCommonGenerate()) {
                    return gen.generateCommon();
                } else {
                    return gen.generateExecute();
                }
            case "BeforeHelp":
                JavaMappedCdpActionGenerator gena = new JavaMappedCdpActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                gena.setBelongElement(true);
                if (gena.getIsCommonGenerate()) {
                    return gena.generateCommon();
                } else {
                    return gena.generateExecute();
                }
            case "AfterMultiDeleteActions":
                JavaAfterMultiDeleteActionGenerator genb = new JavaAfterMultiDeleteActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genb.getIsCommonGenerate()) {
                    return genb.generateCommon();
                } else {
                    return genb.generateExecute();
                }
            case "MultiDeleteActions":
                JavaAbstractMultiDeleteActionGenerator genc = new JavaAbstractMultiDeleteActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genc.getIsCommonGenerate()) {
                    return genc.generateCommon();
                } else {
                    return genc.generateExecute();
                }
            case "BeforeMultiDeleteActions":
                JavaBeforeMultiDeleteActionGenerator gend = new JavaBeforeMultiDeleteActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (gend.getIsCommonGenerate()) {
                    return gend.generateCommon();
                } else {
                    return gend.generateExecute();
                }
            case "ModifyActions":
                JavaAbstractModifyActionGenerator gene = new JavaAbstractModifyActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (gene.getIsCommonGenerate()) {
                    return gene.generateCommon();
                } else {
                    return gene.generateExecute();
                }
            case "AfterModifyActions":
                JavaAfterModifyActionGenerator genf = new JavaAfterModifyActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genf.getIsCommonGenerate()) {
                    return genf.generateCommon();
                } else {
                    return genf.generateExecute();
                }
            case "BeforeModifyActions":
                JavaBeforeModifyActionGenerator geng = new JavaBeforeModifyActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (geng.getIsCommonGenerate()) {
                    return geng.generateCommon();
                } else {
                    return geng.generateExecute();
                }
            case "BeforeSaveActions":
                JavaBeforeSaveActionGenerator genh = new JavaBeforeSaveActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genh.getIsCommonGenerate()) {
                    return genh.generateCommon();
                } else {
                    return genh.generateExecute();
                }
            case "AfterSaveActions":
                JavaAfterSaveActionGenerator geni = new JavaAfterSaveActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (geni.getIsCommonGenerate()) {
                    return geni.generateCommon();
                } else {
                    return geni.generateExecute();
                }
            case "BeforeCreateActions":
                JavaBeforeRetrieveDefaultActionGenerator genj = new JavaBeforeRetrieveDefaultActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genj.getIsCommonGenerate()) {
                    return genj.generateCommon();
                } else {
                    return genj.generateExecute();
                }
            case "CreateActions":
                JavaAbstractRetrieveDefaultActionGenerator genk = new JavaAbstractRetrieveDefaultActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genk.getIsCommonGenerate()) {
                    return genk.generateCommon();
                } else {
                    return genk.generateExecute();
                }
            case "AfterCreateActions":
                JavaAfterRetrieveDefaultActionGenerator genl = new JavaAfterRetrieveDefaultActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genl.getIsCommonGenerate()) {
                    return genl.generateCommon();
                } else {
                    return genl.generateExecute();
                }
            case "BeforeDeleteActions":
                JavaBeforeDeleteActionGenerator genm = new JavaBeforeDeleteActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genm.getIsCommonGenerate()) {
                    return genm.generateCommon();
                } else {
                    return genm.generateExecute();
                }
            case "DeleteActions":
                JavaAbstractDeleteActionGenerator genn = new JavaAbstractDeleteActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genn.getIsCommonGenerate()) {
                    return genn.generateCommon();
                } else {
                    return genn.generateExecute();
                }
            case "AfterDeleteActions":
                JavaAfterDeleteActionGenerator geno = new JavaAfterDeleteActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (geno.getIsCommonGenerate()) {
                    return geno.generateCommon();
                } else {
                    return geno.generateExecute();
                }
            case "BeforeQueryActions":
                JavaBeforeQueryActionGenerator genp = new JavaBeforeQueryActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genp.getIsCommonGenerate()) {
                    return genp.generateCommon();
                } else {
                    return genp.generateExecute();
                }
            case "QueryActions":
                JavaQueryActionGenerator genq = new JavaQueryActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genq.getIsCommonGenerate()) {
                    return genq.generateCommon();
                } else {
                    return genq.generateExecute();
                }
            case "AfterQueryActions":
                JavaAfterQueryActionGenerator genr = new JavaAfterQueryActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genr.getIsCommonGenerate()) {
                    return genr.generateCommon();
                } else {
                    return genr.generateExecute();
                }
            case "BeforeRetrieveActions":
                JavaBeforeRetrieveActionsGenerator gens = new JavaBeforeRetrieveActionsGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (gens.getIsCommonGenerate()) {
                    return gens.generateCommon();
                } else {
                    return gens.generateExecute();
                }
            case "RetrieveActions":
                JavaRetrieveActionGenerator gent = new JavaRetrieveActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (gent.getIsCommonGenerate()) {
                    return gent.generateCommon();
                } else {
                    return gent.generateExecute();
                }
            case "AfterRetrieveActions":
                JavaAfterRetrieveActionGenerator genu = new JavaAfterRetrieveActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genu.getIsCommonGenerate()) {
                    return genu.generateCommon();
                } else {
                    return genu.generateExecute();
                }
            case "DataMappingActions":
                JavaDataMappingActionsGenerator genv = new JavaDataMappingActionsGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genv.getIsCommonGenerate()) {
                    return genv.generateCommon();
                } else {
                    return genv.generateExecute();
                }
            case "ChangesetReversalMappingActions":
                JavaChangeReversalMappingActionGenerator genw = new JavaChangeReversalMappingActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genw.getIsCommonGenerate()) {
                    return genw.generateCommon();
                } else {
                    return genw.generateExecute();
                }
            case "DataReversalMappingActions":
                JavaDataReversalMappingActionsGenerator genx = new JavaDataReversalMappingActionsGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (genx.getIsCommonGenerate()) {
                    return genx.generateCommon();
                } else {
                    return genx.generateExecute();
                }
            case "ChangesetMappingActions":
                JavaChangeMappingActionGenerator geny = new JavaChangeMappingActionGenerator(info, vo, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName, relativePath);
                if (geny.getIsCommonGenerate()) {
                    return geny.generateCommon();
                } else {
                    return geny.generateExecute();
                }
            default:
                throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0006, null, vo.getCode(), vo.getName(), type);
        }
    }

    private ViewModelAction getAction(String code, GspViewModel vo, String type) {
        switch (type) {
            case "VMActions":
                for (ViewModelAction action : vo.getActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "BeforeHelp":
                for (ValueHelpConfig valueHelpConfig : vo.getValueHelpConfigs()) {
                    for (ViewModelAction action : valueHelpConfig.getHelpExtend().getBeforeHelp()) {
                        if (code.equals(action.getCode()))
                            return action;
                    }
                }
            case "AfterMultiDeleteActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getAfterMultiDeleteActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "MultiDeleteActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getMultiDeleteActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "BeforeMultiDeleteActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getBeforeMultiDeleteActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "ModifyActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getModifyActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "AfterModifyActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getAfterModifyActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "BeforeModifyActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getBeforeModifyActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "BeforeSaveActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getBeforeSaveActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "AfterSaveActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getAfterSaveActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "SaveActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getSaveActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "BeforeCreateActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getBeforeCreateActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "CreateActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getCreateActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "AfterCreateActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getAfterCreateActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "BeforeDeleteActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getBeforeDeleteActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "DeleteActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getDeleteActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "AfterDeleteActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getAfterDeleteActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "BeforeQueryActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getBeforeQueryActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "QueryActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getQueryActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "AfterQueryActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getAfterQueryActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "BeforeRetrieveActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getBeforeRetrieveActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "RetrieveActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getRetrieveActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "AfterRetrieveActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getAfterRetrieveActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "DataMappingActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getDataMappingActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "ChangesetReversalMappingActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getChangesetReversalMappingActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "DataReversalMappingActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getDataReversalMappingActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }
            case "ChangesetMappingActions":
                for (ViewModelAction action : vo.getDataExtendInfo().getChangesetMappingActions()) {
                    if (code.equals(action.getCode()))
                        return action;
                }

            default:
                throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0006, null, vo.getCode(), vo.getName(), type);
        }
    }

    private CommonDetermination getDtm(String code, GspViewModel vo) {
        for (CommonDetermination action : vo.getVariables().getDtmAfterModify()) {
            if (code.equals(action.getCode()))
                return action;
        }
        throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0007, null, vo.getCode(), vo.getName(), code);
    }


    //保存前校验遍历获取所有新增动作信息
    public void getNewActions(GspViewModel vo, List<CheckComUtil> checkInfo) {
        for (ViewModelAction action : vo.getActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("VMActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ValueHelpConfig valueHelpConfig : vo.getValueHelpConfigs()) {
            for (ViewModelAction action : valueHelpConfig.getHelpExtend().getBeforeHelp()) {
                if (StringUtils.isEmpty(action.getComponentName())) {
                    CheckComUtil info = new CheckComUtil();
                    info.setAction(action.getCode());
                    info.setType("BeforeHelp");
                    info.setActionName(action.getName());
                    checkInfo.add(info);
                }
            }
        }
        for (CommonDetermination action : vo.getVariables().getDtmAfterModify()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("VarDeterminations");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getMultiDeleteActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("MultiDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getBeforeMultiDeleteActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("BeforeMultiDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getAfterMultiDeleteActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setAction("AfterMultiDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getQueryActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("QueryActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getBeforeQueryActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setAction("BeforeQueryActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getAfterQueryActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setAction("AfterQueryActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getRetrieveActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("RetrieveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getBeforeRetrieveActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("BeforeRetrieveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getAfterRetrieveActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("AfterRetrieveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getCreateActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("CreateActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getBeforeCreateActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("BeforeCreateActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getAfterCreateActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("AfterCreateActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getDeleteActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("DeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getBeforeDeleteActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("BeforeDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getAfterDeleteActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("AfterDeleteActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getModifyActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("ModifyActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getBeforeModifyActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("BeforeModifyActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getAfterModifyActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("AfterModifyActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getBeforeSaveActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("BeforeSaveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getSaveActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("SaveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getAfterSaveActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("AfterSaveActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getChangesetMappingActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("ChangesetMappingActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getChangesetReversalMappingActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("ChangesetReversalMappingActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getDataReversalMappingActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("DataReversalMappingActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
        for (ViewModelAction action : vo.getDataExtendInfo().getDataMappingActions()) {
            if (StringUtils.isEmpty(action.getComponentName())) {
                CheckComUtil info = new CheckComUtil();
                info.setAction(action.getCode());
                info.setType("DataMappingActions");
                info.setActionName(action.getName());
                checkInfo.add(info);
            }
        }
    }

    public String getFilePath(GspViewModel vo, CheckComUtil info, String compModulePath, String compAssemblyName) {
        return getClassName(vo, info.getType(), compModulePath, compAssemblyName, info.getAction());
    }

    private String getClassName(GspViewModel vo, String type, String compModulePath, String compAssemblyName, String actionCode) {
        String classPath;
        if (!"VarDeterminations".equals(type)) {
            classPath = compModulePath + "\\com\\" + compAssemblyName.replace(".", ViewModelUtils.getSeparator()) + "\\" + vo.getName() + "\\" + "voactions" + "\\" + actionCode + "VOAction.java";
        } else {
            classPath = compModulePath + "\\com\\" + compAssemblyName.replace(".", ViewModelUtils.getSeparator()) + "\\" + vo.getName() + "\\" + "vardeterminations" + "\\" + vo.getName() + "Variable" + actionCode + "VODtm.java";
        }
        return classPath;
    }

}
