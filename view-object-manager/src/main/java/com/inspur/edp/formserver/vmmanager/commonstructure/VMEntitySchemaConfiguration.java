package com.inspur.edp.formserver.vmmanager.commonstructure;

import com.inspur.edp.caf.cef.rt.spi.EntitySchemaExtension;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.edp.formserver.vmmanager.commonstructure.VMEntitySchemaExtension")
public class VMEntitySchemaConfiguration {
    @Bean("com.inspur.edp.formserver.vmmanager.commonstructure.VMEntitySchemaExtension.VMEntitySchemaExtension()")
    public EntitySchemaExtension getVMEntitySchemaExtension() {
        return new VMEntitySchemaExtension();
    }
}
