package com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction;


import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.ActionParamInfo;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaCompCodeNames;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaMappedCdpActionGenerator;

public class JavaAfterSaveActionGenerator extends JavaMappedCdpActionGenerator {
    private MappedCdpAction action;

    ///#region 属性
    //private string allInterfaceName;
    //private string beMgrInterfaceName;
    @Override
    protected String getBaseClassName() {
        return "AfterSaveAction";
    }


    ///#endregion


    ///#region 构造函数
    public JavaAfterSaveActionGenerator(ActionParamInfo info, GspViewModel vm, MappedCdpAction vmAction, String nameSpace, String path) {
        super(info, vm, vmAction, nameSpace, path);
        this.action = vmAction;
        //allInterfaceName = ApiHelper.GetBEAllInterfaceClassName(vm);
        //beMgrInterfaceName = ApiHelper.GetBEMgrInterfaceClassName(vm);
    }

    @Override
    public String getNameSpaceSuffix() {
        return JavaCompCodeNames.VOActionNameSpaceSuffix;
    }


    ///#endregion


    ///#region generateConstructor

    @Override
    public void generateConstructor(StringBuilder result) {
        result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(getCompName()).append("(SaveContext context)").append(" ").append("{").append(getNewLine());

        result.append(getIndentationStr()).append(getIndentationStr()).append("super(context)").append(";").append(getNewLine());
        result.append(getIndentationStr()).append("}").append(getNewLine());


        //将部分类合并，加入GenerateExecute方法
        //GenerateExecute(result);
    }

    ///#endregion

    ///#region Using
    @Override
    protected void generateExtendUsing(StringBuilder result) {
        result.append(getUsingStr(JavaCompCodeNames.AfterSaveActionNameSpace));
        result.append(getUsingStr(JavaCompCodeNames.SaveContextNameSpace));
    }

    ///#endregion

}