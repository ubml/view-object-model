package com.inspur.edp.formserver.vmmanager.extendinfo.cache;

import com.inspur.edp.formserver.viewmodel.extendinfo.entity.GspVoExtendInfo;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.caching.api.Cache;
import io.iec.edp.caf.caching.api.CacheManager;
import io.iec.edp.caf.caching.enums.ExpireMode;
import io.iec.edp.caf.caching.serializer.DefaultJsonSerializer;
import io.iec.edp.caf.caching.setting.LayeringCacheSetting;
import io.iec.edp.caf.tenancy.api.ITenantService;
import io.iec.edp.caf.tenancy.api.entity.Tenant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author Kaixuan Shi
 * @since 2024/1/19
 */
public class GspVoExtendInfoCacheManager {
    private static final String GSP_VO_EXTEND_INFO_CACHE_NAME = "gspVoExtendInfo";
    private final Logger logger = LoggerFactory.getLogger(GspVoExtendInfoCacheManager.class);
    private final CacheManager cacheManager;
    private final ITenantService tenantService;
    // 各租户缓存,-1代表公共缓存，上下文中无租户信息时使用
    private final Map<Integer, Cache> voExtendInfoCacheMap = new ConcurrentHashMap<>();
    private final int commonCacheTenantId = -1;

    public GspVoExtendInfoCacheManager(CacheManager cacheManager, ITenantService tenantService) {
        this.cacheManager = cacheManager;
        this.tenantService = tenantService;
    }

    /**
     * 获取当前租户对应缓存实例，如果不存在则创建
     *
     * @return 当前租户对应缓存实例，或者公共缓存（若上下文中无租户信息）
     */
    private Cache getVoExtendInfoCache() {
        int tenantId;
        // 2024/3/26,CAFContext.current.getTenantId()可能产生空指针不可控，因此捕获异常处理，已反馈CAF承诺3月底修复，不抛异常
        try {
            tenantId = CAFContext.current.getTenantId();
        } catch (RuntimeException e) {
            if (logger.isDebugEnabled()) {
                logger.debug("当前上下文无租户，使用公共缓存。线程:" + Thread.currentThread().getName(), e);
            }
            tenantId = commonCacheTenantId;
        }
        return getTenantCache(tenantId);
    }

    private Cache getTenantCache(int tenantId) {
        return voExtendInfoCacheMap.computeIfAbsent(tenantId, this::initCache);
    }

    /**
     * 初始化缓存
     */
    private Cache initCache(int tenantId) {
        LayeringCacheSetting layeringCacheSetting = new LayeringCacheSetting.Builder()
                //开启一级缓存
                .enableFirstCache()
                //一级缓存过期策略
                .firstCacheExpireMode(ExpireMode.ACCESS)
                //一级缓存过期时间
                .firstCacheExpireTime(1)
                //一级缓存过期时间单位
                .firstCacheTimeUnit(TimeUnit.DAYS)
                .firstCacheInitialCapacity(1000)
                .firstCacheMaximumSize(10000)
                //开启二级缓存
                .enableSecondCache()
                //二级缓存过期时间
                .secondCacheExpireTime(2L)
                //二级缓存过期时间单位
                .secondCacheTimeUnit(TimeUnit.DAYS)
                .dataSerializer(new DefaultJsonSerializer())
                .depict("GspVoExtendInfo cache " + tenantId)
                .build();
        logger.info("GspVoExtendInfoCache with tenant {} successfully created.", tenantId);
        return cacheManager.getCache(GSP_VO_EXTEND_INFO_CACHE_NAME + tenantId, layeringCacheSetting);
    }

    /**
     * 清空缓存，应仅由LCM调用，用以更新数据库重启应用时清空旧版缓存。若common层调用，则多负载或分su环境下每台应用重启时都将清空缓存，造成短时间无缓存可用
     */
    public void clear() {
        List<Tenant> tenants = tenantService.getAllTenants("zh-CHS");
        List<Integer> tenantIds = tenants.stream().map(Tenant::getId).collect(Collectors.toList());
        // 添加公共缓存用的租户-1
        tenantIds.add(commonCacheTenantId);
        tenantIds.forEach(tenantId -> {
            getTenantCache(tenantId).clear();
            logger.info("GspVoExtendInfoCache with tenant {} successfully removed.", tenantId);
        });
    }

    public GspVoExtendInfo get(String id) {
        return getVoExtendInfoCache().get(id, GspVoExtendInfo.class);
    }

    public void put(String id, GspVoExtendInfo GspVoExtendInfo) {
        getVoExtendInfoCache().put(id, GspVoExtendInfo);
    }

    public void delete(String id) {
        getVoExtendInfoCache().evict(id);
    }
}
