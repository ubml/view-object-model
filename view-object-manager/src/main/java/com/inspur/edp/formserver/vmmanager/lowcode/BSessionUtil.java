package com.inspur.edp.formserver.vmmanager.lowcode;


import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackage;
import io.iec.edp.caf.tenancy.api.entity.Tenant;

import java.util.function.Consumer;
import java.util.function.Function;

public class BSessionUtil {

    public static void wrapFirstTenantBSession(Consumer<Tenant> consumer) {

    }

    private static void clearSession() {

    }

    public static void handleMdPkg(MetadataPackage metadataPackage,
                                   Function<String, GspMetadata> getMetadata) {
    }
}
