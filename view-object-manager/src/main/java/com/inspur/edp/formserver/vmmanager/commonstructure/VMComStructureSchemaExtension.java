package com.inspur.edp.formserver.vmmanager.commonstructure;

import com.inspur.edp.caf.cef.dt.spi.CommonStructureSchemaExtension;
import com.inspur.edp.caf.cef.schema.common.CommonStructureContext;
import com.inspur.edp.caf.cef.schema.common.CommonStructureInfo;
import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;

public class VMComStructureSchemaExtension implements CommonStructureSchemaExtension {

    @Override
    public String getEntityType() {
        return "GSPViewModel";
    }

    @Override
    public CommonStructure getCommonStructure(IMetadataContent iMetadataContent,
                                              CommonStructureContext commonStructureContext) {
        return (GspViewModel) iMetadataContent;
    }

    @Override
    public CommonStructureInfo getCommonStructureSummary(IMetadataContent iMetadataContent) {
        return null;
    }
}
