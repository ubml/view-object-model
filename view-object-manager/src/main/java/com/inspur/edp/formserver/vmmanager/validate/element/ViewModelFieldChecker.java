package com.inspur.edp.formserver.vmmanager.validate.element;

import com.inspur.edp.das.commonmodel.validate.element.CMFieldChecker;

public class ViewModelFieldChecker extends CMFieldChecker {
    private static ViewModelFieldChecker viewModelField;

    public static ViewModelFieldChecker getInstance() {
        if (viewModelField == null) {
            viewModelField = new ViewModelFieldChecker();
        }
        return viewModelField;
    }
}
