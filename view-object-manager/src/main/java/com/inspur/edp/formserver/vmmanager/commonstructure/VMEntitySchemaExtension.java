package com.inspur.edp.formserver.vmmanager.commonstructure;

import com.inspur.edp.caf.cef.rt.api.CommonStructureInfo;
import com.inspur.edp.caf.cef.rt.spi.EntitySchemaExtension;
import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import io.iec.edp.caf.runtime.config.CefDesignTimeBeanUtil;

public class VMEntitySchemaExtension implements EntitySchemaExtension {
    @Override
    public CommonStructure getEntity(String metaId) {
        MetadataRTService service = CefDesignTimeBeanUtil.getAppCtx().getBean(MetadataRTService.class);
        GspViewModel vo = (GspViewModel) service.getMetadata(metaId).getContent();
        return vo;
    }

    @Override
    public CommonStructureInfo getEntitySummary(String s) {
        return null;
    }

    @Override
    public String getEntityType() {
        return "GSPViewModel";
    }
}
