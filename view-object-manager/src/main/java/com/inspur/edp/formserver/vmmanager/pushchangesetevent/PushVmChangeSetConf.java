package com.inspur.edp.formserver.vmmanager.pushchangesetevent;

import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PushVmChangeSetConf {
    @Bean(name = "PushVmChangeSetEventBroker")
    public PushVmChangeSetEventBroker setEventBroker(EventListenerSettings settings, PushVmChangeSetEventManager eventManager) {
        return new PushVmChangeSetEventBroker(settings, eventManager);
    }

    @Bean(name = "PushVmChangeSetEventManager")
    public PushVmChangeSetEventManager setEventManager() {
        return new PushVmChangeSetEventManager();
    }
}
