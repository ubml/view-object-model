package com.inspur.edp.formserver.vmmanager;


import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.common.LinkBeUtils;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectSourceType;
import com.inspur.edp.formserver.vmmanager.i18nservice.ViewModelI18nService;
import com.inspur.edp.formserver.vmmanager.validate.ViewModelValidator;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventArgs;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventListener;

public class BffMetadataEventListener implements MetadataEventListener {
    /**
     * 业务实体元数据保存前事件
     *
     * @param e
     */
    public final void fireMetadataSavingEvent(MetadataEventArgs e) {
        // 类型判断
        if (!(e.getMetadata().getContent() instanceof GspViewModel)) {
            return;
        }

        GspViewModel vm = (GspViewModel) ((e.getMetadata().getContent() instanceof GspViewModel) ? e.getMetadata().getContent() : null);

        //保存前校验
        ViewModelValidator validator = new ViewModelValidator();
        validator.validate(e.getPath(), vm);
        //国际化抽取
        ViewModelI18nService service = new ViewModelI18nService();
        service.getResourceItem(e.getMetadata());


    }

    /**
     * 视图对象元数据保存后事件
     *
     * @param e
     */
    public final void fireMetadataSavedEvent(MetadataEventArgs e) {

    }

    @Override
    public void fireMetadataDeletingEvent(MetadataEventArgs metadataEventArgs) {

    }

    @Override
    public void fireMetadataDeletedEvent(MetadataEventArgs metadataEventArgs) {

    }

    @Override
    public void fireMetadataAchievedEvent(MetadataEventArgs args) {
        GspMetadata metadata = args.getMetadata();

        if (!(metadata.getContent() instanceof GspViewModel)) {
            return;
        }
        GspViewModel viewModel = (GspViewModel) metadata.getContent();
        if (viewModel.getIsVirtual())
            return;
        if (viewModel.getMainObject().getMapping() != null && viewModel.getMainObject().getMapping().getSourceType() == GspVoObjectSourceType.BeObject) {
            LinkBeUtils utils = new LinkBeUtils(false);
            utils.setMetadata(metadata);
            utils.setIsExtend(metadata.isExtended());
            utils.linkWithBe(viewModel);
        }
    }
}
