/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.linkedconfig;

import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.collection.ValueHelpConfigCollection;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedConfig;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedConfigCollection;
import com.inspur.edp.formserver.viewmodel.linkedconfig.service.LinkedConfigService;
import com.inspur.edp.formserver.vmmanager.exception.VoManagerErrorCodes;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import org.apache.commons.lang3.StringUtils;

import static com.inspur.edp.formserver.viewmodel.common.LinkedHelpConfigUtil.getObjectByCode;
import static com.inspur.edp.formserver.viewmodel.common.LinkedHelpConfigUtil.getSourceFieldId;

public class LinkedConfigImp implements LinkedConfigService {
    @Override
    public void handleLinkedConfig(GspMetadata sourceMetadata) {
        GspViewModel model = (GspViewModel) sourceMetadata.getContent();
        ValueHelpConfigCollection valueHelpConfigs = model.getValueHelpConfigs();
        if (valueHelpConfigs == null || valueHelpConfigs.size() == 0)
            return;
        for (ValueHelpConfig valueHelpConfig : valueHelpConfigs) {
            handleMappingField(valueHelpConfig, sourceMetadata);
        }
    }

    private void handleMappingField(ValueHelpConfig helpConfig, GspMetadata sourceMetadata) {
        if (helpConfig.getLinkedConfigs() == null || helpConfig.getLinkedConfigs().size() == 0) {
            return;
        }
        GspViewModel sourceviewModel = (GspViewModel) sourceMetadata.getContent();
        LinkedConfigCollection linkedConfigs = helpConfig.getLinkedConfigs();
        for (LinkedConfig linkedConfig : linkedConfigs) {
            handleLinkedConfigField(linkedConfig, sourceviewModel);
        }
    }

    private void handleLinkedConfigField(LinkedConfig linkedConfig, GspViewModel sourceviewModel) {
        String sourceField = linkedConfig.getLinkedFieldsMapping().getSourceField();
        if (sourceField == null || "".equals(sourceField))
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0032, null);
        String sourceObjCode = linkedConfig.getLinkedFieldsMapping().getSourceObjCode();
        if (sourceObjCode == null || "".equals(sourceObjCode))
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0033, null);
        GspViewObject sourceObject = getObjectByCode(sourceviewModel, sourceObjCode);
        if (sourceObject == null)
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0034, null, sourceObjCode);

        String sourceFieldId;
        try {
            sourceFieldId = getSourceFieldId(sourceObject, sourceField);
        } catch (Exception e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0035, e, sourceField);
        }
        if (StringUtils.isEmpty(sourceFieldId)) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0035, null, sourceField);
        }
        linkedConfig.getLinkedFieldsMapping().setSourceField(sourceFieldId);
    }

}
