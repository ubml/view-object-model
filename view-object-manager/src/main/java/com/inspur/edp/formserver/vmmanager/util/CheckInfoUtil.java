package com.inspur.edp.formserver.vmmanager.util;

import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.vmmanager.exception.VoManagerErrorCodes;

import java.io.File;
import java.nio.file.Paths;

public class CheckInfoUtil {

    /**
     * 检查必填项是否为空
     *
     * @param propName
     * @param propValue
     */
    public static void checkNessaceryInfo(String propName, Object propValue) {
        if (checkNull(propValue)) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0070, null, propName);
        }
    }


    /**
     * 检查空对象，空字符串
     *
     * @param propValue
     * @return
     */
    public static boolean checkNull(Object propValue) {
        if (propValue == null) {
            return true;
        }
        if (propValue.getClass().isAssignableFrom(String.class)) {
            String stringValue = (String) propValue;
            if (stringValue == null || stringValue.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static String getCombinePath(String path1, String path2) {
        String path = Paths.get(path1).resolve(path2).toString();
        return handlePath(path);
    }

    public static String handlePath(String path) {
        return path.replace("\\", File.separator);
    }
}
