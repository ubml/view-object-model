package com.inspur.edp.formserver.vmmanager.generatacmpcode;

import com.inspur.edp.das.commonmodel.util.generate.ComponentCodeUpdater;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.ActionParamInfo;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.vmmanager.util.I18nResourceUtil;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;

public class JavaMappedCdpActionGenerator extends JavaBaseActionGenerator {
    private final MappedCdpAction action;

    ///#region 属性
    @Override
    protected String getBaseClassName() {
        if (getBelongElement()) {
            return "AbstractHelpAction";
        } else {
            return "AbstractFSAction<" + ReturnTypeName + ">";
        }
    }

    ///#region 构造函数
    public JavaMappedCdpActionGenerator(ActionParamInfo info, GspViewModel vm, MappedCdpAction vmAction, String nameSpace, String path) {
        super(info, vm, vmAction, nameSpace, path);
        this.action = vmAction;
    }

    @Override
    public String getNameSpaceSuffix() {
        return JavaCompCodeNames.VOActionNameSpaceSuffix;
    }


    @Override
    public void generateConstructor(StringBuilder result) {
        result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(getCompName()).append("(");

        if (hasCustomConstructorParams()) {
            generateConstructorParams(result);
            result.append(") ");
        } else {
            result.append(") ");
        }

        result.append("{").append(getNewLine());
        generateConstructorContent(result);
        result.append(getNewLine()).append(getIndentationStr()).append("}").append(getNewLine());

        //generateExecute(result);

    }

    protected final void generateExecute(StringBuilder result) {
        result.append(getIndentationStr()).append("@").append(JavaCompCodeNames.KeywordOverride).append(getNewLine());
        result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(JavaCompCodeNames.KeywordVoid).append(" ").append("execute()").append(" ").append("{").append(getNewLine());
        result.append(getIndentationStr()).append("}").append(getNewLine());
    }

    @Override
    protected void generateExtendMethod(StringBuilder result) {
    }


    @Override
    public String getInitializeCompName() {
        return String.format("%1$s%2$s", this.VMAction, "VO");
    }

    public String update(String originContent) {
        String genFile = this.generateCommon();
        String compName = this.getCompName();
        //获取元数据Header信息
        MetadataHeader metadataHeader=this.metadata.getHeader();
        //获取前缀信息，在IDE控制台输当前修改的元数据动作信息
        String prefix= I18nResourceUtil.getMessage("GSP_VIEWOBJECT_MANAGER_MESSAGE_0001",metadataHeader.getCode(),
                metadataHeader.getName(),this.VMAction.getCode(),this.VMAction.getName());
        return   ComponentCodeUpdater.codeUpdate(originContent, genFile,compName,prefix);
    }


}