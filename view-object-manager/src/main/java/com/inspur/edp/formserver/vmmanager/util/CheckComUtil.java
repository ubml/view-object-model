package com.inspur.edp.formserver.vmmanager.util;

import lombok.Data;

@Data
public class CheckComUtil {

    private String actionName = "";
    private String type = "";
    private String action = "";
}
