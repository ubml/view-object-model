package com.inspur.edp.formserver.vmmanager.config;

import io.iec.edp.caf.commons.event.config.EventListenerData;
import io.iec.edp.caf.commons.event.config.EventManagerData;
import lombok.var;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration(proxyBeanMethods = false)
public class MdPkgDeployConfig {

    @Bean
    public EventManagerData createEventManagerData() {
        EventManagerData data = new EventManagerData();
        data.setName("MdpkgChangedEventManager");
        List<EventListenerData> listeners = new ArrayList<>();
        data.setListeners(listeners);
        var listenerData = new EventListenerData();
        listenerData.setName("VoListener");
        listenerData.setImplClassName("com.inspur.edp.formserver.vmmanager.lowcode.MdPkgDeployListener");
        data.getListeners().add(listenerData);
        return data;
    }
}
