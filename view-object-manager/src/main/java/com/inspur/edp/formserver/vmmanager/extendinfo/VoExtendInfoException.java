package com.inspur.edp.formserver.vmmanager.extendinfo;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

@Deprecated
public class VoExtendInfoException extends CAFRuntimeException {
    public VoExtendInfoException(String message, Exception innerException, ExceptionLevel level) {
        super(VoExdInfoExceptionCode.SERVICE_UNIT_CODE, VoExdInfoExceptionCode.EXCEPTIONCODE, message, innerException, level);
    }
}
