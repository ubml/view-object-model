package com.inspur.edp.formserver.vmmanager.generatacmpcode;

public class ParameterInfo {
    private String privateParamName;

    public final String getParamName() {
        return privateParamName;
    }

    public final void setParamName(String value) {
        privateParamName = value;
    }

    private String privateParamType;

    public final String getParamType() {
        return privateParamType;
    }

    public final void setParamType(String value) {
        privateParamType = value;
    }

    private String privateParameterMode;

    public final String getParameterMode() {
        return privateParameterMode;
    }

    public final void setParameterMode(String value) {
        privateParameterMode = value;
    }
}