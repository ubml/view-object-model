package com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction;


import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.ActionParamInfo;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaCompCodeNames;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaMappedCdpActionGenerator;

public class JavaQueryActionGenerator extends JavaMappedCdpActionGenerator {
    private MappedCdpAction action;

    ///#region 属性
    //private string allInterfaceName;
    //private string beMgrInterfaceName;
    @Override
    protected String getBaseClassName() {
        if (getBelongElement()) {
            return "AbstractQueryAction";
        } else
        //return "AfterQueryAction<" + ReturnTypeName + ">";
        {
            return "AbstractQueryAction";
        }
    }


    ///#endregion

    ///#region import
    @Override
    protected void generateExtendUsing(StringBuilder result) {
        result.append(getUsingStr(JavaCompCodeNames.VMQueryContextNameSpace));
        result.append(getUsingStr(JavaCompCodeNames.VMQueryNameSpace));
        //if (BaseClassName.Contains("Date"))
        //{
        //    result.append(getUsingStrNoStar(JavaCompCodeNames.DateNameSpace));
        //}
        //if (BaseClassName.Contains("BigDecimal"))
        //{
        //    result.append(getUsingStrNoStar(JavaCompCodeNames.BigDecimalNameSpace));
        //}

        //参数列表
        //if (usingList == null || usingList.Count < 1)
        //    return;

        //foreach (var usingName in usingList)
        //{
        //    result.append(getUsingStr(usingName));
        //}
    }

    ///#endregion


    ///#region 构造函数
    public JavaQueryActionGenerator(ActionParamInfo info, GspViewModel vm, MappedCdpAction vmAction, String nameSpace, String path) {
        super(info, vm, vmAction, nameSpace, path);
        this.action = vmAction;
        //allInterfaceName = ApiHelper.GetBEAllInterfaceClassName(vm);
        //beMgrInterfaceName = ApiHelper.GetBEMgrInterfaceClassName(vm);
    }

    @Override
    public String getNameSpaceSuffix() {
        return JavaCompCodeNames.VOActionNameSpaceSuffix;
    }


    ///#endregion


    ///#region generateConstructor

    @Override
    public void generateConstructor(StringBuilder result) {
        result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(getCompName()).append("(").append(JavaCompCodeNames.KeywordQueryContext).append(" ").append(JavaCompCodeNames.Keywordcontext);

        //result.append(getDoubleIndentationStr()).append(getIndentationStr()).append("IBEManagerContext managerContext");

        if (hasCustomConstructorParams()) {
            result.append("\n");
            generateConstructorParams(result);
            result.append(getDoubleIndentationStr()).append(getIndentationStr()).append("{").append(")").append(getNewLine());
        } else {
            result.append(")").append("{").append(getNewLine());
        }

        result.append(getDoubleIndentationStr()).append(JavaCompCodeNames.KeywordSuper).append("(").append(JavaCompCodeNames.Keywordcontext).append(")").append(";").append(getNewLine());

        //result.append(getDoubleIndentationStr()).append(getIndentationStr()).append(") : base(managerContext)");

        //result.append(getDoubleIndentationStr()).append("{");
        generateConstructorContent(result);
        result.append(getIndentationStr()).append("}");
        result.append("\n");

        //GenerateExecute(result);

    }


    @Override
    public String getInitializeCompName() {
        return String.format("%1$s%2$s", VMAction.getCode(), "VO");
    }
}