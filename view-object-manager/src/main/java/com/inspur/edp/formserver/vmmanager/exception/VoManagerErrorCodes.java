/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.exception;

public class VoManagerErrorCodes {
    public final static String GSP_VIEWOBJECT_MANAGER_0001 = "GSP_VIEWOBJECT_MANAGER_0001";
    public final static String GSP_VIEWOBJECT_MANAGER_0002 = "GSP_VIEWOBJECT_MANAGER_0002";
    public final static String GSP_VIEWOBJECT_MANAGER_0003 = "GSP_VIEWOBJECT_MANAGER_0003";
    public final static String GSP_VIEWOBJECT_MANAGER_0004 = "GSP_VIEWOBJECT_MANAGER_0004";
    public final static String GSP_VIEWOBJECT_MANAGER_0005 = "GSP_VIEWOBJECT_MANAGER_0005";
    public final static String GSP_VIEWOBJECT_MANAGER_0006 = "GSP_VIEWOBJECT_MANAGER_0006";
    public final static String GSP_VIEWOBJECT_MANAGER_0007 = "GSP_VIEWOBJECT_MANAGER_0007";
    public final static String GSP_VIEWOBJECT_MANAGER_0008 = "GSP_VIEWOBJECT_MANAGER_0008";
    public final static String GSP_VIEWOBJECT_MANAGER_0009 = "GSP_VIEWOBJECT_MANAGER_0009";
    public final static String GSP_VIEWOBJECT_MANAGER_0010 = "GSP_VIEWOBJECT_MANAGER_0010";
    public final static String GSP_VIEWOBJECT_MANAGER_0011 = "GSP_VIEWOBJECT_MANAGER_0011";
    public final static String GSP_VIEWOBJECT_MANAGER_0012 = "GSP_VIEWOBJECT_MANAGER_0012";
    public final static String GSP_VIEWOBJECT_MANAGER_0013 = "GSP_VIEWOBJECT_MANAGER_0013";
    public final static String GSP_VIEWOBJECT_MANAGER_0014 = "GSP_VIEWOBJECT_MANAGER_0014";
    public final static String GSP_VIEWOBJECT_MANAGER_0015 = "GSP_VIEWOBJECT_MANAGER_0015";
    public final static String GSP_VIEWOBJECT_MANAGER_0016 = "GSP_VIEWOBJECT_MANAGER_0016";
    public final static String GSP_VIEWOBJECT_MANAGER_0017 = "GSP_VIEWOBJECT_MANAGER_0017";
    public final static String GSP_VIEWOBJECT_MANAGER_0018 = "GSP_VIEWOBJECT_MANAGER_0018";
    public final static String GSP_VIEWOBJECT_MANAGER_0019 = "GSP_VIEWOBJECT_MANAGER_0019";
    public final static String GSP_VIEWOBJECT_MANAGER_0020 = "GSP_VIEWOBJECT_MANAGER_0020";
    public final static String GSP_VIEWOBJECT_MANAGER_0021 = "GSP_VIEWOBJECT_MANAGER_0021";
    public final static String GSP_VIEWOBJECT_MANAGER_0022 = "GSP_VIEWOBJECT_MANAGER_0022";
    public final static String GSP_VIEWOBJECT_MANAGER_0023 = "GSP_VIEWOBJECT_MANAGER_0023";
    public final static String GSP_VIEWOBJECT_MANAGER_0024 = "GSP_VIEWOBJECT_MANAGER_0024";
    public final static String GSP_VIEWOBJECT_MANAGER_0025 = "GSP_VIEWOBJECT_MANAGER_0025";
    public final static String GSP_VIEWOBJECT_MANAGER_0026 = "GSP_VIEWOBJECT_MANAGER_0026";
    public final static String GSP_VIEWOBJECT_MANAGER_0027 = "GSP_VIEWOBJECT_MANAGER_0027";
    public final static String GSP_VIEWOBJECT_MANAGER_0028 = "GSP_VIEWOBJECT_MANAGER_0028";
    public final static String GSP_VIEWOBJECT_MANAGER_0029 = "GSP_VIEWOBJECT_MANAGER_0029";
    public final static String GSP_VIEWOBJECT_MANAGER_0030 = "GSP_VIEWOBJECT_MANAGER_0030";
    public final static String GSP_VIEWOBJECT_MANAGER_0031 = "GSP_VIEWOBJECT_MANAGER_0031";
    public final static String GSP_VIEWOBJECT_MANAGER_0032 = "GSP_VIEWOBJECT_MANAGER_0032";
    public final static String GSP_VIEWOBJECT_MANAGER_0033 = "GSP_VIEWOBJECT_MANAGER_0033";
    public final static String GSP_VIEWOBJECT_MANAGER_0034 = "GSP_VIEWOBJECT_MANAGER_0034";
    public final static String GSP_VIEWOBJECT_MANAGER_0035 = "GSP_VIEWOBJECT_MANAGER_0035";
    public final static String GSP_VIEWOBJECT_MANAGER_0036 = "GSP_VIEWOBJECT_MANAGER_0036";
    public final static String GSP_VIEWOBJECT_MANAGER_0037 = "GSP_VIEWOBJECT_MANAGER_0037";
    public final static String GSP_VIEWOBJECT_MANAGER_0038 = "GSP_VIEWOBJECT_MANAGER_0038";
    public final static String GSP_VIEWOBJECT_MANAGER_0039 = "GSP_VIEWOBJECT_MANAGER_0039";
    public final static String GSP_VIEWOBJECT_MANAGER_0040 = "GSP_VIEWOBJECT_MANAGER_0040";
    public final static String GSP_VIEWOBJECT_MANAGER_0041 = "GSP_VIEWOBJECT_MANAGER_0041";
    public final static String GSP_VIEWOBJECT_MANAGER_0042 = "GSP_VIEWOBJECT_MANAGER_0042";
    public final static String GSP_VIEWOBJECT_MANAGER_0043 = "GSP_VIEWOBJECT_MANAGER_0043";
    public final static String GSP_VIEWOBJECT_MANAGER_0044 = "GSP_VIEWOBJECT_MANAGER_0044";
    public final static String GSP_VIEWOBJECT_MANAGER_0045 = "GSP_VIEWOBJECT_MANAGER_0045";
    public final static String GSP_VIEWOBJECT_MANAGER_0046 = "GSP_VIEWOBJECT_MANAGER_0046";
    public final static String GSP_VIEWOBJECT_MANAGER_0047 = "GSP_VIEWOBJECT_MANAGER_0047";
    public final static String GSP_VIEWOBJECT_MANAGER_0048 = "GSP_VIEWOBJECT_MANAGER_0048";
    public final static String GSP_VIEWOBJECT_MANAGER_0049 = "GSP_VIEWOBJECT_MANAGER_0049";
    public final static String GSP_VIEWOBJECT_MANAGER_0050 = "GSP_VIEWOBJECT_MANAGER_0050";
    public final static String GSP_VIEWOBJECT_MANAGER_0051 = "GSP_VIEWOBJECT_MANAGER_0051";
    public final static String GSP_VIEWOBJECT_MANAGER_0052 = "GSP_VIEWOBJECT_MANAGER_0052";
    public final static String GSP_VIEWOBJECT_MANAGER_0053 = "GSP_VIEWOBJECT_MANAGER_0053";
    public final static String GSP_VIEWOBJECT_MANAGER_0056 = "GSP_VIEWOBJECT_MANAGER_0056";
    public final static String GSP_VIEWOBJECT_MANAGER_0057 = "GSP_VIEWOBJECT_MANAGER_0057";
    public final static String GSP_VIEWOBJECT_MANAGER_0058 = "GSP_VIEWOBJECT_MANAGER_0058";
    public final static String GSP_VIEWOBJECT_MANAGER_0059 = "GSP_VIEWOBJECT_MANAGER_0059";
    public final static String GSP_VIEWOBJECT_MANAGER_0060 = "GSP_VIEWOBJECT_MANAGER_0060";
    public final static String GSP_VIEWOBJECT_MANAGER_0061 = "GSP_VIEWOBJECT_MANAGER_0061";
    public final static String GSP_VIEWOBJECT_MANAGER_0062 = "GSP_VIEWOBJECT_MANAGER_0062";
    public final static String GSP_VIEWOBJECT_MANAGER_0063 = "GSP_VIEWOBJECT_MANAGER_0063";
    public final static String GSP_VIEWOBJECT_MANAGER_0065 = "GSP_VIEWOBJECT_MANAGER_0065";
    public final static String GSP_VIEWOBJECT_MANAGER_0066 = "GSP_VIEWOBJECT_MANAGER_0066";
    public final static String GSP_VIEWOBJECT_MANAGER_0067 = "GSP_VIEWOBJECT_MANAGER_0067";
    public final static String GSP_VIEWOBJECT_MANAGER_0068 = "GSP_VIEWOBJECT_MANAGER_0068";
    public final static String GSP_VIEWOBJECT_MANAGER_0069 = "GSP_VIEWOBJECT_MANAGER_0069";
    public final static String GSP_VIEWOBJECT_MANAGER_0070 = "GSP_VIEWOBJECT_MANAGER_0070";
    public final static String GSP_VIEWOBJECT_MANAGER_0071 = "GSP_VIEWOBJECT_MANAGER_0071";
    public final static String GSP_VIEWOBJECT_MANAGER_0072 = "GSP_VIEWOBJECT_MANAGER_0072";
    public final static String GSP_VIEWOBJECT_MANAGER_0073 = "GSP_VIEWOBJECT_MANAGER_0073";
    public final static String GSP_VIEWOBJECT_MANAGER_0074 = "GSP_VIEWOBJECT_MANAGER_0074";
    public final static String GSP_VIEWOBJECT_MANAGER_0075 = "GSP_VIEWOBJECT_MANAGER_0075";
    public final static String GSP_VIEWOBJECT_MANAGER_0076 = "GSP_VIEWOBJECT_MANAGER_0076";
    public final static String GSP_VIEWOBJECT_MANAGER_0087 = "GSP_VIEWOBJECT_MANAGER_0087";
    public final static String GSP_VIEWOBJECT_MANAGER_0092 = "GSP_VIEWOBJECT_MANAGER_0092";
    public final static String GSP_VIEWOBJECT_MANAGER_0093 = "GSP_VIEWOBJECT_MANAGER_0093";
    public final static String GSP_VIEWOBJECT_MANAGER_0094 = "GSP_VIEWOBJECT_MANAGER_0094";
    public final static String GSP_VIEWOBJECT_MANAGER_0095 = "GSP_VIEWOBJECT_MANAGER_0095";
    public final static String GSP_VIEWOBJECT_MANAGER_0096 = "GSP_VIEWOBJECT_MANAGER_0096";
    public final static String GSP_VIEWOBJECT_MANAGER_0097 = "GSP_VIEWOBJECT_MANAGER_0097";
    public final static String GSP_VIEWOBJECT_MANAGER_0100 = "GSP_VIEWOBJECT_MANAGER_0100";
    public final static String GSP_VIEWOBJECT_MANAGER_0101 = "GSP_VIEWOBJECT_MANAGER_0101";
    public final static String GSP_VIEWOBJECT_MANAGER_0102 = "GSP_VIEWOBJECT_MANAGER_0102";
    public final static String GSP_VIEWOBJECT_MANAGER_0103 = "GSP_VIEWOBJECT_MANAGER_0103";
    /**
     * 对象[{0}]在业务实体上无映射对象，业务实体对象ID为[{1}],请检查是否已删除。
     */
    public final static String GSP_VIEWOBJECT_MANAGER_0104 = "GSP_VIEWOBJECT_MANAGER_0104";
    /**
     * 获取映射的业务实体[{0}]失败
     */
    public static final String GSP_VIEWOBJECT_MANAGER_0105 = "GSP_VIEWOBJECT_MANAGER_0105";
    /**
     * 以下子对象的关联信息不完整，请修改后再保存：[{0}]
     */
    public static final String GSP_VIEWOBJECT_MANAGER_0106 = "GSP_VIEWOBJECT_MANAGER_0106";
    /**
     * 对象[{0}]上的字段[{1}]在业务实体上无映射字段，业务实体字段的ID=[{2}],请检查是否已删除。
     */
    public static final String GSP_VIEWOBJECT_MANAGER_0107 = "GSP_VIEWOBJECT_MANAGER_0107";
    /**
     * 对象[{0}]上的字段[{1}]的[长度]不能大于业务实体上字段长度[{2}]。
     */
    public static final String GSP_VIEWOBJECT_MANAGER_0108 = "GSP_VIEWOBJECT_MANAGER_0108";
    /**
     * 对象[{0}]上的字段[{1}]的[精度]不能大于业务实体上字段精度[{2}]。
     */
    public static final String GSP_VIEWOBJECT_MANAGER_0109 = "GSP_VIEWOBJECT_MANAGER_0109";
    /**
     * 操作[{0}]在业务实体上无映射操作，业务实体自定义操作的ID=[{1}],请检查是否已删除。
     */
    public static final String GSP_VIEWOBJECT_MANAGER_0110 = "GSP_VIEWOBJECT_MANAGER_0110";
}
