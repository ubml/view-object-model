package com.inspur.edp.formserver.vmmanager.vodtconsistencychecklistener;


import com.inspur.edp.bef.bizentity.bizentitydtevent.BizEntityActionDTEventListener;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.AbstractMgrActionEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.DeletingActionEventArgs;
import com.inspur.edp.cef.designtime.api.dtconsistencycheck.ConsistencyCheckEventMessage;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.List;

public class BizEntityActionDTConsistencyCheckListener extends BizEntityActionDTEventListener {

    @Override
    public DeletingActionEventArgs deletingAction(DeletingActionEventArgs args) {
        return (DeletingActionEventArgs) bizEntityConsistencyCheck(args);
    }

    /**
     * 对关联信息进行检查
     *
     * @param args
     * @return
     */
    protected AbstractMgrActionEventArgs bizEntityConsistencyCheck(AbstractMgrActionEventArgs args) {
        String returnMessage = getDependentInfos(args.getMetadataPath(), args.getBeId(), args.getActionId());
        if (returnMessage == null || returnMessage.length() == 0) {
            return args;
        }
        ConsistencyCheckEventMessage message = new ConsistencyCheckEventMessage(false, returnMessage);
        args.addEventMessage(message);
        return args;
    }

    /**
     * 获取的关联信息
     *
     * @param metadataPath 元数据路径
     * @param beId         元数据ID
     * @param actionId     自定义动作Id
     * @return 关联当前BE的关联信息
     */
    protected String getDependentInfos(String metadataPath, String beId, String actionId) {
        MetadataService metadataService = SpringBeanUtils.getBean(com.inspur.edp.lcm.metadata.api.service.MetadataService.class);
        List<GspMetadata> gspMetadataList = metadataService.getMetadataListByRefedMetadataId(metadataPath, beId);
        StringBuilder strBuilder = new StringBuilder();
        gspMetadataList.forEach(gspMetadata -> {
            if (!gspMetadata.getHeader().getType().equals("GSPViewModel"))
                return;
            GspViewModel viewModel = (GspViewModel) metadataService
                    .loadMetadata(gspMetadata.getHeader().getFileName(), gspMetadata.getRelativePath())
                    .getContent();
            viewModel.getActions().forEach(vmAction -> {

                if (vmAction.getMapping() == null || !vmAction.getMapping().getTargetObjId().equals(actionId))
                    return;
                strBuilder.append(returnMessage(getProjectName(gspMetadata.getRelativePath()), viewModel.getCode(), vmAction.getCode()));
            });
        });
        return strBuilder.toString();
    }

    protected String returnMessage(String projectName, String viewModelCode, String actionCode) {
        return VMI8nResourceUtil.getMessage(VoResourceKeyNames.VO_ACTION_CONSISTENCY_CHECK_PREFIX,
                projectName, viewModelCode, actionCode);
    }

    /**
     * 获取元数据包名
     *
     * @param metadataPath 元数据路径
     * @return 元数据包名
     */
    protected String getProjectName(String metadataPath) {
        MetadataProjectService projectService = SpringBeanUtils
                .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataProjectService.class);
        return projectService.getMetadataProjInfo(metadataPath).getName();
    }
}
