package com.inspur.edp.formserver.vmmanager.vmdtconsistencycheckevent;

public enum VmDtConsistencyCheckType {
    changingViewObjectCode,
    removingViewObject,
    removingVoField,
    changingVoFieldDataType,
    changingVoFieldObjectType,
    changingVoFieldLabelId,
    changingVoActionCode,
    deletingVoAction,
    changingVoActionReturn,
    changingVoActionParams,
    changingVoActionCollectType;
}
