package com.inspur.edp.formserver.vmmanager.increment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.increment.ViewModelIncrement;
import com.inspur.edp.formserver.vmmanager.exception.VoManagerErrorCodes;
import com.inspur.edp.metadata.rtcustomization.api.ICustomizedContent;
import com.inspur.edp.metadata.rtcustomization.spi.CustomizationSerializer;

public class ViewModelCustomizationSerializer implements CustomizationSerializer {

    private ObjectMapper mapper;

    private ObjectMapper getMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
//            SimpleModule module = new SimpleModule();
//            module.addSerializer(ViewModelIncrement.class, new ViewModelIncrementSerializer());
//            module.addDeserializer(ViewModelIncrement.class, new ViewModelIncrementDeserializer());
//            mapper.registerModule(module);
        }
        return mapper;
    }

    @Override
    public String serialize(ICustomizedContent increament) {
        try {
            return getMapper().writeValueAsString(increament);
        } catch (JsonProcessingException e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0027, e);
        }
    }

    @Override
    public ICustomizedContent deSerialize(String increamentStr) {
        try {
            return getMapper().readValue(increamentStr, ViewModelIncrement.class);
        } catch (JsonProcessingException e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0028, e);
        }
    }
}
