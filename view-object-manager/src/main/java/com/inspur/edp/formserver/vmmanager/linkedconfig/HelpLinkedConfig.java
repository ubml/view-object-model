package com.inspur.edp.formserver.vmmanager.linkedconfig;

import com.inspur.edp.formserver.viewmodel.linkedconfig.service.LinkedConfigService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HelpLinkedConfig {
    @Bean("com.inspur.edp.formserver.vmmanager.linkedconfig.HelpLinkedConfig.LinkedConfigService")
    public LinkedConfigService getLinkedConfigService() {
        return new LinkedConfigImp();
    }
}
