package com.inspur.edp.formserver.vmmanager.lowcode;

import com.inspur.edp.metadata.rtcustomization.spi.event.ExtMdSavedArgs;
import com.inspur.edp.metadata.rtcustomization.spi.event.IMetadataDeployEventListener;
import com.inspur.edp.metadata.rtcustomization.spi.event.MdPkgDeployedEventArgs;

public class MdPkgToolsDeployListener implements IMetadataDeployEventListener {


    public MdPkgToolsDeployListener() {
    }

    @Override
    public void fireMdPkgDeployedEvent(MdPkgDeployedEventArgs mdPkgDeployedEventArgs) {
    }

    @Override
    public void fireExtMdSavedEvent(ExtMdSavedArgs extMdSavedArgs) {

    }
}
