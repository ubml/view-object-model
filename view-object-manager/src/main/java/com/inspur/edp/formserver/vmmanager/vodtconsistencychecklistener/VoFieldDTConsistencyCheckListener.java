package com.inspur.edp.formserver.vmmanager.vodtconsistencychecklistener;

import com.inspur.edp.bef.bizentity.bizentitydtevent.BizEntityFieldDTEventListener;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.AbstractBeFieldEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.RemovingFieldEventArgs;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.dtconsistencycheck.ConsistencyCheckEventMessage;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.collection.VMElementCollection;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.ArrayList;
import java.util.List;

public class VoFieldDTConsistencyCheckListener extends BizEntityFieldDTEventListener {

    /**
     * 删除节点监听事件
     *
     * @param args
     * @return
     */
    @Override
    public RemovingFieldEventArgs removingField(RemovingFieldEventArgs args) {
        return (RemovingFieldEventArgs) fieldConsistencyCheck(args);
    }


    /**
     * 进行依赖性检查
     *
     * @param args
     * @return
     */
    protected AbstractBeFieldEventArgs fieldConsistencyCheck(AbstractBeFieldEventArgs args) {
        String returnMessage = getFieldAssoAndDependencyInfos(args.getMetadataPath(),
                args.getBeId(), args.getFieldId());
        if (returnMessage == null || returnMessage.length() == 0) {
            return args;
        }
        ConsistencyCheckEventMessage message = new ConsistencyCheckEventMessage(true, returnMessage);
        args.addEventMessage(message);
        return args;
    }

    /**
     * 获取关联信息
     *
     * @param
     * @return
     */

    protected String getFieldAssoAndDependencyInfos(String metadataPath,
                                                    String beId, String fieldId) {
        MetadataService metadataService = SpringBeanUtils
                .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataService.class);
        List<GspMetadata> gspMetadataList = metadataService
                .getMetadataListByRefedMetadataId(metadataPath, beId);
        StringBuilder strBuilder = new StringBuilder();
        for (GspMetadata gspMetadata : gspMetadataList) {
            if (!gspMetadata.getHeader().getType().equals("GSPViewModel")) {
                continue;
            }
            String projectName = null;
            GspViewModel viewModel = (GspViewModel) metadataService
                    .loadMetadata(gspMetadata.getHeader().getFileName(), gspMetadata.getRelativePath())
                    .getContent();
            ArrayList<IGspCommonObject> commonObjects = viewModel.getAllObjectList();
            for (IGspCommonObject viewObject : commonObjects) {
                //检查是否依赖的节点
                VMElementCollection vmElements = ((GspViewObject) viewObject).getContainElements();
                for (IGspCommonField vmElement : vmElements) {
                    //检查VO上的字段是依赖的
                    if (getIsDependence((GspViewModelElement) vmElement, fieldId)) {
                        if (projectName == null) {
                            projectName = getProjectName(gspMetadata.getRelativePath());
                        }
                        strBuilder.append(
                                returnMessage(projectName, gspMetadata.getHeader().getCode(), viewObject.getCode(),
                                        vmElement.getCode()));
                    }
                }
            }
        }
        if (strBuilder.toString() == null || strBuilder.toString().length() == 0) {
            return null;
        }
        return strBuilder.toString();
    }

    /**
     * 获取工程名称
     *
     * @param metadataPath 元数据路径
     * @return 元数据包名
     */
    protected String getProjectName(String metadataPath) {
        MetadataProjectService projectService = SpringBeanUtils
                .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataProjectService.class);
        return projectService.getMetadataProjInfo(metadataPath).getName();
    }

    /**
     * 拼接返回值信息
     */
    protected String returnMessage(String projectName, String voCode, String nodeCode,
                                   String fieldCode) {
        return VMI8nResourceUtil.getMessage(VoResourceKeyNames.VO_ELEMENT_RELY_ELEMENT_CONSISTENCY_CHECK_PREFIX,
                projectName, voCode, nodeCode, fieldCode);
    }

    /**
     * 判断当前字段是否是关联或依赖
     */
    protected boolean getIsDependence(GspViewModelElement vmElement, String beElementId) {
        //依赖
        if (vmElement.getMapping() != null && vmElement.getMapping().getTargetElementId().equals(beElementId)) {
            return true;
        }
        //虚拟字段关联
        if (vmElement.getObjectType() != GspElementObjectType.Association || !vmElement
                .getIsVirtual()) {
            return false;
        }
        if (vmElement.getChildAssociations().size() == 0) {
            return false;
        }
        GspFieldCollection fieldCollection = vmElement.getChildAssociations().get(0).getRefElementCollection();
        for (IGspCommonField field : fieldCollection) {
            if (field.getRefElementId().equals(beElementId))
                return true;
        }
        return false;
    }
}

