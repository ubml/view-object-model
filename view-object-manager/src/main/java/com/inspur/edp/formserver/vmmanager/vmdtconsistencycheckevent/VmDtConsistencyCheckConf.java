package com.inspur.edp.formserver.vmmanager.vmdtconsistencycheckevent;

import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.edp.formserver.vmmanager.vmdtconsistencycheckevent")
public class VmDtConsistencyCheckConf {
    @Bean(name = "VmDtConsistencyCheckEventBroker")
    public VmDtConsistencyCheckEventBroker setEventBroker(EventListenerSettings settings) {
        return new VmDtConsistencyCheckEventBroker(settings);
    }

    @Bean(name = "VmDtConsistencyCheckEventManager")
    public VmDtConsistencyCheckEventManager setEventManager() {
        return new VmDtConsistencyCheckEventManager();
    }
}
