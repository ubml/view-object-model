package com.inspur.edp.formserver.vmmanager.metaelement.service;

public class BffMetaElementConst {
    public static final String VO_ENTITY = "VoEntity";
    public static final String VO_ENTITY_CODE = "VoEntity-Code";
    public static final String VO_ELEMENT = "VoElement";
    public static final String VO_ACTION = "VoAction";
}
