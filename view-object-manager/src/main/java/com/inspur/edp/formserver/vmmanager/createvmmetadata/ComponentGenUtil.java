package com.inspur.edp.formserver.vmmanager.createvmmetadata;

import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import com.inspur.edp.formserver.vmmanager.exception.VoManagerErrorCodes;
import com.inspur.edp.lcm.fs.api.IFsService;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public final class ComponentGenUtil {
    public static final String ComponentDir = "component";

    public static String getComponentAssemblyName(String path) {
        MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);
        MetadataProject projSvr = service.getMetadataProjInfo(path);

        if (projSvr == null) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0008, null);
        }
        return projSvr.getNameSpace();
    }

    public static String prepareComponentDir(String vmPath) {
        String path = ViewModelUtils.getCombinePath(vmPath, ComponentDir);
        IFsService fsSvr = SpringBeanUtils.getBean(IFsService.class);
        if (!fsSvr.existsAsDir(path)) {
            fsSvr.createDir(path);
        }
        return path;
    }
}