package com.inspur.edp.formserver.vmmanager.validate.object;

import com.inspur.edp.cef.designtime.api.validate.element.FieldChecker;
import com.inspur.edp.das.commonmodel.validate.object.CMObjectChecker;
import com.inspur.edp.formserver.vmmanager.validate.element.ViewModelFieldChecker;

public class ViewObjInspaction extends CMObjectChecker {
    private static ViewObjInspaction viewObj;

    public static ViewObjInspaction getInstance() {
        if (viewObj == null) {
            viewObj = new ViewObjInspaction();
        }
        return viewObj;
    }

    @Override
    protected FieldChecker getFieldChecker() {
        return ViewModelFieldChecker.getInstance();
    }
}
