package com.inspur.edp.formserver.vmmanager.accessory;


public enum ApprovalLogType {
    /**
     * 流程实例
     */
    ProcessInstant(0),

    /**
     * 单据内码
     */
    BIllCode(1);

    private int intValue;
    private static java.util.HashMap<Integer, ApprovalLogType> mappings;

    private synchronized static java.util.HashMap<Integer, ApprovalLogType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ApprovalLogType>();
        }
        return mappings;
    }

    ApprovalLogType(int value) {
        intValue = value;
        ApprovalLogType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static ApprovalLogType forValue(int value) {
        return getMappings().get(value);
    }
}
