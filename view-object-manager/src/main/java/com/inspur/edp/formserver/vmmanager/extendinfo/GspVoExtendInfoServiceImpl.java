package com.inspur.edp.formserver.vmmanager.extendinfo;

import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.extendinfo.api.GspVoExtendInfoService;
import com.inspur.edp.formserver.viewmodel.extendinfo.entity.GspVoExtendInfo;
import com.inspur.edp.formserver.vmmanager.exception.VoManagerErrorCodes;
import com.inspur.edp.formserver.vmmanager.extendinfo.cache.GspVoExtendInfoCacheManager;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import io.iec.edp.caf.rpc.api.support.Type;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

import java.util.LinkedHashMap;
import java.util.List;

public class GspVoExtendInfoServiceImpl implements GspVoExtendInfoService {

    private static final String Lcm_SU = "Lcm";
    private static final String VO_EXTEND_INFO_SERVICE = "com.inspur.edp.formserver.voextendinfo.server.api.GspVoExtendInfoRpcService";
    private final GspVoExtendInfoCacheManager gspVoExtendInfoCacheManager;
    private final RpcClient rpcClient;

    public GspVoExtendInfoServiceImpl(GspVoExtendInfoCacheManager gspVoExtendInfoCacheManager,
                                      RpcClient rpcClient) {
        this.gspVoExtendInfoCacheManager = gspVoExtendInfoCacheManager;
        this.rpcClient = rpcClient;
    }

    // 使用be Id的取消缓存
    @Override
    public GspVoExtendInfo getVoExtendInfo(String id) {
        GspVoExtendInfo voExtendInfoFromCache = this.gspVoExtendInfoCacheManager.get(id);
        if (voExtendInfoFromCache != null) {
            return voExtendInfoFromCache;
        }
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("id", id);
        try {
            return rpcClient.invoke(GspVoExtendInfo.class,
                    getMethodString("getVoExtendInfo"), Lcm_SU, params, null);
        } catch (Exception e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0010, e, id);
        }
    }

    @Override
    public GspVoExtendInfo getVoExtendInfoByConfigId(String configId) {
        GspVoExtendInfo voExtendInfoFromCache = this.gspVoExtendInfoCacheManager.get(configId);
        if (voExtendInfoFromCache != null) {
            return voExtendInfoFromCache;
        }
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("configId", configId);
        try {
            return rpcClient.invoke(GspVoExtendInfo.class,
                    getMethodString("getVoExtendInfoByConfigId"), Lcm_SU, params, null);
        } catch (IncorrectResultSizeDataAccessException e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0011, e, configId);
        } catch (Exception e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0012, e, configId);
        }
    }

    @Override
    public List<GspVoExtendInfo> getVoExtendInfos() {
        try {
            LinkedHashMap<String, Object> params = new LinkedHashMap<>();
            Type<?> type = getViewModelListType();
            return (List<GspVoExtendInfo>) rpcClient.invoke(type,
                    getMethodString("getVoExtendInfos"), Lcm_SU, params, null);
        } catch (Exception e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0013, e);
        }
    }

    @Override
    public void saveGspVoExtendInfos(List<GspVoExtendInfo> infos) {
        infos.forEach(item -> {
            if (item.getId() == null || "".equals(item.getId()))
                throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0017, null);
        });
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("infos", infos);
        try {
            rpcClient.invoke(Void.class,
                    getMethodString("saveGspVoExtendInfos"), Lcm_SU, params, null);
        } catch (Exception e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0014, e);

        }
    }

    @Override
    public void deleteVoExtendInfo(String id) {
        if (id == null || "".equals(id)) {
            return;
        }
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("id", id);
        try {
            rpcClient.invoke(Void.class,
                    getMethodString("deleteVoExtendInfo"), Lcm_SU, params, null);
        } catch (Exception e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0015, e, id);
        }
    }

    @Override
    public List<GspVoExtendInfo> getVoId(String beId) {
        if (beId == null || "".equals(beId)) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0018, null);
        }
        try {
            LinkedHashMap<String, Object> params = new LinkedHashMap<>();
            params.put("beId", beId);
            Type type = getViewModelListType();
            List<GspVoExtendInfo> voExtendInfos = (List<GspVoExtendInfo>) rpcClient.invoke(type,
                    getMethodString("getVoId"), Lcm_SU, params, null);
            return voExtendInfos;
        } catch (Exception e) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0016, e, beId);
        }
    }

    private String getMethodString(String methodName) {
        return VO_EXTEND_INFO_SERVICE + "." + methodName;
    }

    private Type<List> getViewModelListType() {
        Type<List> type = new Type<>(List.class, GspVoExtendInfo.class);
        return type;
    }
}
