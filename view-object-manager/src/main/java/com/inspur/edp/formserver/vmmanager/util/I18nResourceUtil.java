package com.inspur.edp.formserver.vmmanager.util;

import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.commons.utils.StringUtils;
import io.iec.edp.caf.i18n.api.ResourceLocalizer;

import java.text.MessageFormat;

public class I18nResourceUtil {
    private static final String I18N_RESOURCE_FIle ="viewobject_model_designtime.properties";
    private static final String SU="pfcommon";
    private static  final ResourceLocalizer resourceLocalizer= SpringBeanUtils.getBean(ResourceLocalizer.class);

    /**
     * 获取国际化资源信息
     * @param msgCode 消息编码
     * @param messageParams 消息参数
     * @return 国际化消息
     */
    public static String getMessage(String msgCode, Object... messageParams) {
        // 获取资源文件中对应消息编码的文本内容
        String message = getMessage(msgCode);
        // 如果消息参数为空，则直接返回消息编码
        if (messageParams == null || messageParams.length < 1) {
            return message;
        }
        // 根据消息参数构造消息
        return MessageFormat.format(message, messageParams);
    }

    /**
     * 获取国际化资源信息
     * @param msgCode 消息编码
     * @return 国际化消息
     */
    private static String getMessage(String msgCode) {
        // 从国际化资源文件中获取对应编码的消息
        String messageFormat = resourceLocalizer.getString(msgCode, I18N_RESOURCE_FIle, SU, CAFContext.current.getLanguage());
        if (StringUtils.isEmpty(messageFormat)) {
            messageFormat = msgCode;
        }
        return messageFormat;
    }
}
