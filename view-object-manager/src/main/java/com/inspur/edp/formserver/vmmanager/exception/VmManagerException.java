package com.inspur.edp.formserver.vmmanager.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;

@Deprecated
public class VmManagerException extends CAFRuntimeException {

    public VmManagerException(String serviceUnitCode, String exceptionCode, String exceptionMessage, RuntimeException innerException, io.iec.edp.caf.commons.exception.ExceptionLevel level, boolean isBizException) {
        super(serviceUnitCode, exceptionCode, exceptionMessage, innerException, level, isBizException);
    }
}
