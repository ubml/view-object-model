package com.inspur.edp.formserver.vmmanager.metaelement.service.config;

import com.inspur.edp.formserver.vmmanager.metaelement.service.BffMetadataElementServiceImpl;
import com.inspur.edp.lcm.metadata.spi.MetadataElementService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BffMetaElementServiceConfig {
    @Bean(name = "com.inspur.edp.formserver.vmmanager.metaelement.service.config.BffMetaElementService")
    public MetadataElementService getBffMetaElementService() {
        return new BffMetadataElementServiceImpl();
    }
}
