package com.inspur.edp.formserver.vmmanager.pushchangesetevent;

import com.inspur.edp.formserver.viewmodel.pushchangesetargs.VmPushChangeSetArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

public class PushVmChangeSetEventBroker extends EventBroker {
    private PushVmChangeSetEventManager eventManager;

    public PushVmChangeSetEventBroker(EventListenerSettings settings, PushVmChangeSetEventManager eventManager) {
        super(settings);
        this.eventManager = eventManager;
        this.init();
    }

    @Override
    protected void onInit() {
        this.getEventManagerCollection().add(this.eventManager);
    }

    public void firePushChangeSet(VmPushChangeSetArgs args) {
        this.eventManager.firePushChangeSet(args);
    }
}
