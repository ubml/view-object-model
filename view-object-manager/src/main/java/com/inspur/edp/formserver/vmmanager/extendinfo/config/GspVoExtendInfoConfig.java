package com.inspur.edp.formserver.vmmanager.extendinfo.config;

import com.inspur.edp.formserver.viewmodel.extendinfo.api.GspVoExtendInfoService;
import com.inspur.edp.formserver.vmmanager.extendinfo.GspVoExtendInfoServiceImpl;
import com.inspur.edp.formserver.vmmanager.extendinfo.cache.GspVoExtendInfoCacheManager;
import io.iec.edp.caf.caching.api.CacheManager;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import io.iec.edp.caf.tenancy.api.ITenantService;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan({"com.inspur.edp.formserver.viewmodel.extendinfo.entity"})
@EnableJpaRepositories({"com.inspur.edp.formserver.vmmanager.extendinfo.repository"})
public class GspVoExtendInfoConfig {
    @Bean
    public GspVoExtendInfoService getVoExtendInfoService(GspVoExtendInfoCacheManager gspVoExtendInfoCacheManager,
                                                         RpcClient rpcClient) {
        return new GspVoExtendInfoServiceImpl(gspVoExtendInfoCacheManager, rpcClient);
    }

    @Bean("com.inspur.edp.formserver.vmmanager.extendinfo.config.GspVoExtendInfoConfig.getVoExtendInfoCacheManager")
    public GspVoExtendInfoCacheManager getVoExtendInfoCacheManager(CacheManager cacheManager, ITenantService tenantService) {
        return new GspVoExtendInfoCacheManager(cacheManager, tenantService);
    }

}

