package com.inspur.edp.formserver.vmmanager.validate.model;

import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.validate.model.CommonModelChecker;
import com.inspur.edp.das.commonmodel.validate.object.CMObjectChecker;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.vmmanager.validate.object.ViewObjInspaction;
import com.inspur.edp.formserver.vmmanager.validate.operation.ActionChecker;

public class ViewModelChecker extends CommonModelChecker {
    private static ViewModelChecker viewModelChecker;

    public static ViewModelChecker getInstance() {
        if (viewModelChecker == null) {
            viewModelChecker = new ViewModelChecker();
        }
        return viewModelChecker;
    }

    public final void check(GspViewModel viewModel) {
        checkCM(viewModel);
    }

    @Override
    protected void checkExtension(GspCommonModel viewModel) {
        ActionChecker.getInstance().checkVMAction((GspViewModel) viewModel);
    }

    @Override
    protected CMObjectChecker getCMObjectChecker() {
        return ViewObjInspaction.getInstance();
    }
}
