/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.vmmanager.generatacmpcode;


import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaCommonDeterminationGenerator;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaIBaseCompCodeGen;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.ActionParamInfo;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionInfo;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionType;
import com.inspur.edp.formserver.viewmodel.common.InternalExtendActionUtil;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.common.ViewModelActionUtil;
import com.inspur.edp.formserver.viewmodel.dataextendinfo.VoDataExtendInfo;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import com.inspur.edp.formserver.vmmanager.createvmmetadata.ComponentGenUtil;
import com.inspur.edp.formserver.vmmanager.exception.VoManagerErrorCodes;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAbstractDeleteActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAbstractModifyActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAbstractMultiDeleteActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAbstractRetrieveDefaultActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAfterDeleteActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAfterModifyActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAfterMultiDeleteActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAfterQueryActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAfterRetrieveActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAfterRetrieveDefaultActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaAfterSaveActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaBeforeDeleteActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaBeforeModifyActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaBeforeMultiDeleteActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaBeforeQueryActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaBeforeRetrieveActionsGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaBeforeRetrieveDefaultActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaBeforeSaveActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaChangeMappingActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaChangeReversalMappingActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaDataMappingActionsGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaDataReversalMappingActionsGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaQueryActionGenerator;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.voextendedaction.JavaRetrieveActionGenerator;
import com.inspur.edp.formserver.vmmanager.service.VmManagerService;
import com.inspur.edp.lcm.fs.api.IFsService;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.io.IOException;
import java.util.ArrayList;


public class JavaCodeFileGenerator {

    private String relativePath;
    private GspViewModel viewModel;
    private GspMetadata sourceMeta;
    private String compAssemblyName;
    private FileService fsService;
    IFsService iFsService;
    private static final String codeFileExtension = ".java";


    public JavaCodeFileGenerator(GspMetadata metadata) {
        this.sourceMeta = metadata;
        relativePath = metadata.getRelativePath();

        GspProject metadataProj = SpringBeanUtils.getBean(MetadataService.class)
                .getGspProjectInfo(relativePath);
        viewModel = (GspViewModel) metadata.getContent();
        //根据元数据相对路径获取工程的namespace（dotnet中对应程序集名称）
        compAssemblyName = metadataProj.getProjectNameSpace();
        fsService = SpringBeanUtils.getBean(FileService.class);
        iFsService = SpringBeanUtils.getBean(IFsService.class);
    }

    public final void generate(java.util.ArrayList actionList) {
        generateVMActions(actionList);
        generateVariableDtms(actionList);
    }


    private boolean isGenCompCode(ArrayList<String> actionList, String actionCode) {
        return actionList.contains(actionCode);
    }

    private void generateVMActions(java.util.ArrayList actionList) {
        String path = null;
        ActionParamInfo info = new ActionParamInfo();
        info.setMetadata(this.sourceMeta);
        for (ViewModelAction action : viewModel.getActions()) {
            if (action.getType() == ViewModelActionType.VMAction && isGenCompCode(actionList,
                    action.getCode())) {
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                VMCodeGenerate(info, viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), path, false);
            }
        }

        for (ValueHelpConfig valueHelpConfig : viewModel.getValueHelpConfigs()) {
            if (valueHelpConfig.getHelpExtend() != null && valueHelpConfig.getHelpExtend().getBeforeHelp() != null) {

                for (ViewModelAction action : valueHelpConfig.getHelpExtend().getBeforeHelp()) {
                    if (isGenCompCode(actionList, action.getCode())) {
                        if (path == null) {
                            ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                            path = prepareDir(actionInfo);
                        }
                        VMCodeGenerate(info, viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), path, true);
                    }

                }
            }
        }
        //扩展操作
        VoDataExtendInfo extendInfo = viewModel.getDataExtendInfo();
        //创建BeforeCreateActions

        for (ViewModelAction action : extendInfo.getBeforeCreateActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaBeforeRetrieveDefaultActionGenerator gen = new JavaBeforeRetrieveDefaultActionGenerator(info,
                        viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                        compAssemblyName, relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }
        //创建AfterCreateActions

        for (ViewModelAction action : extendInfo.getAfterCreateActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaAfterRetrieveDefaultActionGenerator gen = new JavaAfterRetrieveDefaultActionGenerator(info,
                        viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                        compAssemblyName, relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }
        //创建CreateActions

        for (ViewModelAction action : extendInfo.getCreateActions()) {

            //if (!action.ID.Equals("52479451-8e22-4751-8684-80489ce5786b"))
            if (!action.getID().equals(InternalExtendActionUtil.CreateActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaAbstractRetrieveDefaultActionGenerator gen = new JavaAbstractRetrieveDefaultActionGenerator(info,
                            viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }
            }
        }
        //创建BeforeModifyActions

        for (ViewModelAction action : extendInfo.getBeforeModifyActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaBeforeModifyActionGenerator gen = new JavaBeforeModifyActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }
        //创建AfterModifyActions

        for (ViewModelAction action : extendInfo.getAfterModifyActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaAfterModifyActionGenerator gen = new JavaAfterModifyActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }
        //创建ModifyActions

        for (ViewModelAction action : extendInfo.getModifyActions()) {
            //if (!action.ID.Equals("47dd3752-72a3-4c56-81c0-ae8ccfe5eb98"))
            if (!action.getID().equals(InternalExtendActionUtil.ModifyActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaAbstractModifyActionGenerator gen = new JavaAbstractModifyActionGenerator(info, viewModel,
                            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }

            }
        }
        //创建ChangesetMappingActions

        for (ViewModelAction action : extendInfo.getChangesetMappingActions()) {
            //if (!action.ID.Equals("5798f884-c222-47f4-8bbe-685c7013dee4"))
            if (!action.getID().equals(InternalExtendActionUtil.ChangesetMappingActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaChangeMappingActionGenerator gen = new JavaChangeMappingActionGenerator(info, viewModel,
                            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }

            }
        }
        //创建ChangesetReversalMappingActions

        for (ViewModelAction action : extendInfo.getChangesetReversalMappingActions()) {
            if (!action.getID().equals(InternalExtendActionUtil.ChangesetReversalMappingActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaChangeReversalMappingActionGenerator gen = new JavaChangeReversalMappingActionGenerator(info,
                            viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }

            }
        }
        //创建AfterQueryActions

        for (ViewModelAction action : extendInfo.getAfterQueryActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaAfterQueryActionGenerator gen = new JavaAfterQueryActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }
        //创建BeforeQueryActions

        for (ViewModelAction action : extendInfo.getBeforeQueryActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaBeforeQueryActionGenerator gen = new JavaBeforeQueryActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }

        for (ViewModelAction action : extendInfo.getQueryActions()) {

            if (!action.getID().equals(InternalExtendActionUtil.QueryActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaQueryActionGenerator gen = new JavaQueryActionGenerator(info, viewModel,
                            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }

            }
        }

        for (ViewModelAction action : extendInfo.getDataMappingActions()) {
            if (!action.getID().equals(InternalExtendActionUtil.DataMappingActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaDataMappingActionsGenerator gen = new JavaDataMappingActionsGenerator(info, viewModel,
                            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }

            }
        }

        for (ViewModelAction action : extendInfo.getDataReversalMappingActions()) {
            if (!action.getID().equals(InternalExtendActionUtil.DataReversalMappingActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaDataReversalMappingActionsGenerator gen = new JavaDataReversalMappingActionsGenerator(info,
                            viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }

            }
        }

        for (ViewModelAction action : extendInfo.getBeforeRetrieveActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaBeforeRetrieveActionsGenerator gen = new JavaBeforeRetrieveActionsGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }

        for (ViewModelAction action : extendInfo.getAfterRetrieveActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaAfterRetrieveActionGenerator gen = new JavaAfterRetrieveActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }

        for (ViewModelAction action : extendInfo.getRetrieveActions()) {
            if (!action.getID().equals(InternalExtendActionUtil.RetrieveActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaRetrieveActionGenerator gen = new JavaRetrieveActionGenerator(info, viewModel,
                            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }

            }
        }

        for (ViewModelAction action : extendInfo.getBeforeMultiDeleteActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaBeforeMultiDeleteActionGenerator gen = new JavaBeforeMultiDeleteActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }

        for (ViewModelAction action : extendInfo.getMultiDeleteActions()) {
            if (!action.getID().equals(InternalExtendActionUtil.MultiDeleteActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaAbstractMultiDeleteActionGenerator gen = new JavaAbstractMultiDeleteActionGenerator(info,
                            viewModel, (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }

            }
        }

        for (ViewModelAction action : extendInfo.getAfterMultiDeleteActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaAfterMultiDeleteActionGenerator gen = new JavaAfterMultiDeleteActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }

        for (ViewModelAction action : extendInfo.getBeforeDeleteActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaBeforeDeleteActionGenerator gen = new JavaBeforeDeleteActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }

        for (ViewModelAction action : extendInfo.getDeleteActions()) {
            if (!action.getID().equals(InternalExtendActionUtil.DeleteActionId)) {
                if (isGenCompCode(actionList, action.getCode())) {
                    JavaAbstractDeleteActionGenerator gen = new JavaAbstractDeleteActionGenerator(info, viewModel,
                            (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null),
                            compAssemblyName, relativePath);
                    gen.setBelongElement(false);
                    if (path == null) {
                        ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                        path = prepareDir(actionInfo);
                    }
                    extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
                }

            }
        }

        for (ViewModelAction action : extendInfo.getAfterDeleteActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaAfterDeleteActionGenerator gen = new JavaAfterDeleteActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }

        for (ViewModelAction action : extendInfo.getBeforeSaveActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaBeforeSaveActionGenerator gen = new JavaBeforeSaveActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }

        for (ViewModelAction action : extendInfo.getAfterSaveActions()) {
            if (isGenCompCode(actionList, action.getCode())) {
                JavaAfterSaveActionGenerator gen = new JavaAfterSaveActionGenerator(info, viewModel,
                        (MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), compAssemblyName,
                        relativePath);
                gen.setBelongElement(false);
                if (path == null) {
                    ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo((MappedCdpAction) action, viewModel, this.sourceMeta);
                    path = prepareDir(actionInfo);
                }
                extendActionCodeGen((MappedCdpAction) ((action instanceof MappedCdpAction) ? action : null), gen, path, false);
            }

        }
    }

    /**
     * 变量联动计算构件代码生成
     */
    private void generateVariableDtms(java.util.ArrayList actionList) {
        if (viewModel.getVariables() == null) {
            return;
        }
        javaGenerateVariableDtms(viewModel.getVariables().getDtmAfterCreate(), actionList);
        javaGenerateVariableDtms(viewModel.getVariables().getDtmAfterModify(), actionList);
        javaGenerateVariableDtms(viewModel.getVariables().getDtmBeforeSave(), actionList);
    }

    private void javaGenerateVariableDtms(CommonDtmCollection dtms, java.util.ArrayList actionList) {
        String dirPath = null;

        for (CommonDetermination dtm : dtms) {
            if (dirPath == null) {
                ViewModelActionInfo actionInfo = ViewModelActionUtil.getActionInfo(dtm, viewModel, this.sourceMeta);
                dirPath = prepareDir(actionInfo);
            }
            if (!dtm.getIsRef() && dtm.getIsGenerateComponent() && actionList.contains(dtm.getCode())) {
                javaGenerateVariableDtm(dtm, dirPath);
            }
        }
    }

    private void javaGenerateVariableDtm(CommonDetermination dtm, String dirPath) {
        JavaCommonDeterminationGenerator gen = new JavaCommonDeterminationGenerator(viewModel, dtm,
                compAssemblyName, relativePath);
        generateSplitFile(gen, dirPath, false);
    }

    private String prepareDir(ViewModelActionInfo actionInfo) {
        MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);
        //获取Java工程路径下java文件夹路径
        String compModulePath = service.getJavaCompProjectPath(relativePath);
        String className = VmManagerService.getComponentJavaClassName(actionInfo);
        //构件中获取javaClassName,需要截取掉一级ClassName拼接相对路径
        String path = String.format("%1$s%2$s%3$s", compModulePath, ViewModelUtils.getSeparator(),
                (className.substring(0, className.lastIndexOf(".")).replace(".", ViewModelUtils.getSeparator())));
        if (!fsService.isDirectoryExist(path)) {
            fsService.createDirectory(path);
        }
        return path;
    }


    private void extendActionCodeGen(MappedCdpAction vmAction, JavaIBaseCompCodeGen codeGen,
                                     String dirPath, boolean belongElement) {
        if (vmAction.getIsGenerateComponent()) {
            generateSplitFile(codeGen, dirPath, false);
        }
    }

    private void VMCodeGenerate(ActionParamInfo info, GspViewModel viewModel, MappedCdpAction vmAction, String dirPath,
                                boolean belongElement) {
        if (vmAction.getIsGenerateComponent()) {
            JavaMappedCdpActionGenerator gen = new JavaMappedCdpActionGenerator(info, viewModel, vmAction,
                    compAssemblyName, relativePath);
            gen.setBelongElement(belongElement);
            generateSplitFile(gen, dirPath, belongElement);
        }
    }

    // belongElement 默认为false,
    private void generateSplitFile(JavaIBaseCompCodeGen codeGen, String dirPath,
                                   boolean belongElement) {
        belongElement = true; //已存在文件时，true:不覆盖；false:覆盖
        String compName = codeGen.getCompName();

        //一个相对路径，目前未在使用的路径
        String originalFilePath = ViewModelUtils
                .getCombinePath(relativePath, ComponentGenUtil.ComponentDir, compName + codeFileExtension);
        //构件代码实际路径
        String filePathCommon = fsService.getCombinePath(dirPath, compName + codeFileExtension);
        String originalFile=null;
        if (fsService.isFileExist(filePathCommon) && belongElement) {
            try {
                originalFile = fsService.fileRead(filePathCommon);
            } catch (IOException e) {
               throw  new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0103,e,e.getMessage());
            }
            String  updatedFile=codeGen.update(originalFile );
            if (updatedFile==null ||"".equals(updatedFile))
                return;
            fsService.fileUpdate(filePathCommon,updatedFile,false);
            return;
        }
//        if (fsService.isFileExist(originalFilePath)) {
//            return;
//        }

        if (codeGen.getIsCommonGenerate()) {
            iFsService.createFile(filePathCommon, codeGen.generateCommon());
        } else {
            iFsService.createFile(filePathCommon, codeGen.generateExecute());
        }
    }
}