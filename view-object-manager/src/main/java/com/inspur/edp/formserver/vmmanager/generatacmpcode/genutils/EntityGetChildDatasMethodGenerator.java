package com.inspur.edp.formserver.vmmanager.generatacmpcode.genutils;

import com.inspur.edp.cef.designtime.core.utilsgenerator.*;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;

public class EntityGetChildDatasMethodGenerator {
    private final String childNodeCode;
    private final JavaClassInfo classInfo;
    private MethodInfo methodInfo;

    public EntityGetChildDatasMethodGenerator(String code, JavaClassInfo classInfo) {

        this.childNodeCode = code;
        this.classInfo = classInfo;
        this.methodInfo = new MethodInfo();
    }

    public void generate() {
        methodInfo.setMethodName("get" + childNodeCode + "s");
        methodInfo.setReturnType(new TypeRefInfo(IEntityDataCollection.class));
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Static);
        methodInfo.getParameters().add(new ParameterInfo("data", new TypeRefInfo(IEntityData.class)));
        generateMethodBodies();
        classInfo.addMethodInfo(methodInfo);
    }

    private void generateMethodBodies() {
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.cef.entity.entity.EntityDataUtils");
        methodInfo.getMethodBodies().add("return EntityDataUtils.getChildDatas(data,\"" + childNodeCode + "\");");
    }
}
