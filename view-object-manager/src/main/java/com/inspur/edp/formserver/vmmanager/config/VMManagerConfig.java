package com.inspur.edp.formserver.vmmanager.config;

import com.inspur.edp.caf.cef.dt.spi.CommonStructureSchemaExtension;
import com.inspur.edp.formserver.vmmanager.commonstructure.VMComStructureSchemaExtension;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.edp.formserver.vmmanager.config.VMManagerConfig")
public class VMManagerConfig {
    @Bean("com.inspur.edp.formserver.vmmanager.config.VMManagerConfig.VMComStructureSchemaExtension()")
    public CommonStructureSchemaExtension getVMComStructureSchemaExtension() {
        return new VMComStructureSchemaExtension();
    }

}

