package com.inspur.edp.formserver.vmmanager.service;

import com.inspur.edp.bef.component.detailcmpentity.udtdetermination.UDTDtmComponent;
import com.inspur.edp.bef.component.detailcmpentity.vm.VMComponent;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionInfo;
import com.inspur.edp.formserver.viewmodel.common.ViewModelActionUtil;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import com.inspur.edp.formserver.vmmanager.createvmmetadata.ComponentGenerator;
import com.inspur.edp.formserver.vmmanager.exception.VoManagerErrorCodes;
import com.inspur.edp.formserver.vmmanager.generatacmpcode.JavaCodeFileGenerator;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.lcm.metadata.api.entity.MetadataReference;
import com.inspur.edp.lcm.metadata.api.entity.uri.MetadataURI;
import com.inspur.edp.lcm.metadata.api.exception.MetadataNotFoundException;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;

public class VmManagerService {
    public static void saveMetadata(GspMetadata metadata, String metadataPath) {
        SpringBeanUtils.getBean(MetadataService.class).saveMetadata(metadata, ViewModelUtils.getCombinePath(metadataPath, metadata.getHeader().getFileName()));
    }



    public static ArrayList generateComponent(GspMetadata metadata, String metadataPath, boolean isSave) {
        GspViewModel vm = (GspViewModel) ((metadata.getContent() instanceof GspViewModel) ? metadata.getContent() : null);
        String bizObjectID = metadata.getHeader().getBizobjectID();
        return ComponentGenerator.getInstance().GenerateComponent(vm, metadataPath, bizObjectID);
        //if (isSave)
        //    saveMetadata(metadata, metadataPath);
    }

    public static String getComponentJavaClassName(String componentEntityId) {
        RefCommonService lcmDtService = SpringBeanUtils.getBean(RefCommonService.class);
        GspMetadata metadata = lcmDtService.getRefMetadata(componentEntityId);
        String javaClassName = null;
        if (metadata.getContent() instanceof VMComponent) {
            javaClassName = ((VMComponent) metadata.getContent()).getVmMethod().getClassName();
        }
        if (metadata.getContent() instanceof UDTDtmComponent) {
            javaClassName = ((UDTDtmComponent) metadata.getContent()).getUdtDtmMethod().getClassName();
        }
        if (javaClassName == null || !javaClassName.contains(".")) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0046, null);
        }
        return javaClassName;
    }

    public static String getComponentJavaClassName(ViewModelActionInfo actionInfo) {
        RefCommonService lcmDtService = SpringBeanUtils.getBean(RefCommonService.class);
        GspMetadata metadata = getMetadata(actionInfo, lcmDtService);
//    GspMetadata metadata = lcmDtService.getRefMetadata(componentEntityId);
        String javaClassName = null;
        if (metadata.getContent() instanceof VMComponent) {
            javaClassName = ((VMComponent) metadata.getContent()).getVmMethod().getClassName();
        }
        if (metadata.getContent() instanceof UDTDtmComponent) {
            javaClassName = ((UDTDtmComponent) metadata.getContent()).getUdtDtmMethod().getClassName();
        }
        if (javaClassName == null || !javaClassName.contains(".")) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0046, null);
        }
        return javaClassName;
    }

    public static GspMetadata getMetadata(ViewModelActionInfo actionInfo, RefCommonService service) {
        MetadataURI metadataURI = new MetadataURI();
        metadataURI.setId(actionInfo.getCompId());
        GspMetadata metadata = null;
        try {
            metadata = service.getRefMetadata(metadataURI, null, "");
            return metadata;
        } catch (MetadataNotFoundException e) {
            String vmNameSpace = getVmNameSpace(actionInfo.getSourceMeta());
            MetadataHeader metadataHeader = getgetDependentMetadataInfo(actionInfo.getSourceMeta(), actionInfo.getCompId());
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0100, e, actionInfo.getViewModel().getCode(), actionInfo.getViewModel().getName(), vmNameSpace, actionInfo.getName(),
                    actionInfo.getCompId(), metadataHeader.getCode(), metadataHeader.getName(), metadataHeader.getNameSpace());
        }
    }

    private static MetadataHeader getgetDependentMetadataInfo(GspMetadata metadata, String id) {
        MetadataHeader metadataHeader = new MetadataHeader() {{
            setName("");
            setCode("");
            setNameSpace("");
        }};
        if (metadata == null || metadata.getRefs() == null || metadata.getRefs().size() == 0) {
            return metadataHeader;
        }
        MetadataReference ref = ViewModelActionUtil.getReference(metadata.getRefs(), id);
        if (ref == null || ref.getDependentMetadata() == null) {
            return metadataHeader;
        }
        return ref.getDependentMetadata();
    }

    private static String getVmNameSpace(GspMetadata sourceMeta) {
        if (sourceMeta == null || sourceMeta.getHeader() == null)
            return "";
        return sourceMeta.getHeader().getNameSpace();
    }

    //        *
//         * actionList 生成构件的动作编号集合（初次生成构件才生成构件代码）
//
    public static void generateComponentCode(GspMetadata metadata, String metadataPath, java.util.ArrayList actionList) {

        String ProjectPath = metadata.getRelativePath();

        new JavaCodeFileGenerator(metadata).generate(actionList);
//    new VoCommonUtilsGenerator((GspViewModel) metadata.getContent(),metadata).generateCommonUtils();
    }


    /**
     * 判断当前是否创建Java模板，创建返回值为true,不能创建返回flase;
     *
     * @param ProjectPath
     * @return
     */
    public static boolean creatJavaModule(String ProjectPath) {
        boolean moduleCreated = true;
        return moduleCreated;
    }


    /**
     判断当前是否创建donet模板，创建返回值为true,不能创建返回flase;

     @param
     @return
     */

}
