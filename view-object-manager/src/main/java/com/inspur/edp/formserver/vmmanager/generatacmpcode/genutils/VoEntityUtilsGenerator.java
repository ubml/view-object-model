package com.inspur.edp.formserver.vmmanager.generatacmpcode.genutils;

import com.inspur.edp.cef.designtime.core.utilsgenerator.DataTypeUtilsGenerator;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.formserver.viewmodel.GspViewObject;

public class VoEntityUtilsGenerator extends DataTypeUtilsGenerator {
    private final GspViewObject viewObject;
    private final String packageName;

    public VoEntityUtilsGenerator(GspViewObject viewObject, String packageName, String basePath) {
        super(viewObject, basePath);
        this.viewObject = viewObject;
        this.packageName = packageName;
    }

    @Override
    protected String getClassName() {
        return viewObject.getCode() + "Utils";
    }

    @Override
    protected String getClassPackage() {
        return packageName;
    }

    @Override
    protected void generateExtendInfos() {
        super.generateExtendInfos();
        generateGetChildObjectDatas();
    }

    private void generateGetChildObjectDatas() {
        if (viewObject.getContainChildObjects() == null || viewObject.getContainChildObjects().size() == 0)
            return;
        for (IGspCommonObject childObject : viewObject.getContainChildObjects()) {
            new EntityGetChildDatasMethodGenerator(childObject.getCode(), getClassInfo()).generate();
        }
    }
}
