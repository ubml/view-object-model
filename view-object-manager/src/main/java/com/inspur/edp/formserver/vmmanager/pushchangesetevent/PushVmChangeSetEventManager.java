package com.inspur.edp.formserver.vmmanager.pushchangesetevent;

import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.pushchangesetargs.VmPushChangeSetArgs;
import com.inspur.edp.formserver.viewmodel.pushchangesetlistener.IVmPushChangeSetListener;
import com.inspur.edp.formserver.vmmanager.exception.VoManagerErrorCodes;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class PushVmChangeSetEventManager extends EventManager {

    @Override
    public String getEventManagerName() {
        return "PushVmChangeSetEventManager";
    }

    @Override
    public boolean isHandlerListener(IEventListener listener) {
        return listener instanceof IVmPushChangeSetListener;
    }

    /**
     * 注册事件
     *
     * @param listener 监听者
     */
    @Override
    public void addListener(IEventListener listener) {
        if (!(listener instanceof IVmPushChangeSetListener)) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0038, null);
        }
        IVmPushChangeSetListener pushChangeSetListener = (IVmPushChangeSetListener) listener;
        this.addEventHandler(PushVmChangeSetType.vmPushChangeSet, pushChangeSetListener, "vmPushChangeSet");
    }

    /***
     * 注销事件
     * @param listener  监听者
     */
    @Override
    public void removeListener(IEventListener listener) {
        if (!(listener instanceof IVmPushChangeSetListener)) {
            throw new ViewModelException(VoManagerErrorCodes.GSP_VIEWOBJECT_MANAGER_0038, null);
        }
        IVmPushChangeSetListener pushChangeSetListener = (IVmPushChangeSetListener) listener;
        this.removeEventHandler(PushVmChangeSetType.vmPushChangeSet, pushChangeSetListener, "vmPushChangeSet");
    }

    // fire
    public final void firePushChangeSet(VmPushChangeSetArgs args) {
        this.fire(PushVmChangeSetType.vmPushChangeSet, args);
    }
}
