package com.inspur.edp.formserver.vmmanager.accessory;

import com.inspur.edp.formserver.viewmodel.accessory.AccessoryService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
@ConditionalOnClass(name = {"com.inspur.edp.task.entity.CommentFieldInfo"})
public class Configuration {

    @Bean
    public AccessoryService createAccessoryService() {
        return new AccessoryServiceImpl();
    }
}
