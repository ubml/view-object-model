package com.inspur.edp.formserver.viewmodel.rtwebapi;

import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;

public class BffMetadataRtWebApiServiceAutoConfig {
    @Bean
    public VMMetadataController getRtBffMetadataRtWebApi() {
        return new VMMetadataController();
    }

    @Bean
    public RESTEndpoint getBffRtMetadataRtWebApiEndpoint(VMMetadataController genService) {
        return new RESTEndpoint(
                "/runtime/lcm/v1.0/bff/metadata",
                genService
        );
    }
}
