/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.rtwebapi;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.json.element.BizElementSerializer;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.json.element.GspAssociationSerializer;
import com.inspur.edp.cef.designtime.api.util.MetadataUtil;
import com.inspur.edp.das.commonmodel.entity.element.GspCommonAssociation;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoElementMapping;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.rtwebapi.exception.VoRtApiErrorCodes;
import com.inspur.edp.formserver.viewmodel.util.ViewModelUtils;
import com.inspur.edp.formserver.vmmanager.service.UpdateVirtualVoElementWithUdtService;
import com.inspur.edp.formserver.vmmanager.service.WebControllerService;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataDto;
import com.inspur.edp.lcm.metadata.common.MetadataDtoConverter;
import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public class VMMetadataController {
    private final String EXCEPTIONCODE = "vmMetadataRtWebApi";


    @Path("getbizObject/{beid}/{objid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getBizObject(@PathParam("beid") String beid, @PathParam("objid") String objid, @QueryParam("path") String path, @QueryParam("bePkgName") String bePkgName) {
        GspBusinessEntity be = getBizEntity(path, bePkgName, beid);
        return WebControllerService.getInstance().getBizObject(be, objid);
    }

    /**
     * 保存运行时元数据
     *
     * @param metadataDto
     * @return
     */
    @Path("saveMetadata")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public boolean saveMetadata(MetadataDto metadataDto) {
        if (metadataDto == null) {
            throw new ViewModelException(VoRtApiErrorCodes.GSP_VIEWOBJECT_RTWEBAPI_1006, null);
        }
        GspMetadata metadata = MetadataDtoConverter.asMetadata(metadataDto);
        SpringBeanUtils.getBean(CustomizationService.class).save(metadata);
        return true;
    }

    @Path("checkvo")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public void checkKeywords(String jsonObject) {
        WebControllerService.getInstance().checkKeywords(jsonObject);
    }

    @Path("getVirtualVoAsso")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String getVirtualVoAsso(String info) {
        try {
            return WebControllerService.getInstance().getVirtualVoAsso(info, (String path, String bePkgName, String beId) -> getBizEntity(path, bePkgName, beId));
        } catch (Exception e) {
            throw new ViewModelException(VoRtApiErrorCodes.GSP_VIEWOBJECT_RTWEBAPI_1010, e);
        }
    }

    @Path("virtualVoChooseUdt")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String updateVirtualVoUdtElementWhenChooseUdt(String info) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(info);
            String refUdtId = node.get("refUdtId").textValue();
            String path = node.get("path").textValue();
            String beElementJson = node.get("udtElementJson").textValue();
            return UpdateVirtualVoElementWithUdtService
                    .getInstance().UpdateVariableWithRefUdtRT(refUdtId, path, beElementJson, true);
        } catch (Exception e) {
            throw new ViewModelException(e);
        }

    }

    /**
     * @param path
     * @param bePkgName
     * @param beId
     * @return
     */
    private GspBusinessEntity getBizEntity(String path, String bePkgName, String beId) {

        if (ViewModelUtils.checkNull(beId)) {
            throw new ViewModelException(VoRtApiErrorCodes.GSP_VIEWOBJECT_RTWEBAPI_1007, null);
        }
        GspMetadata metadata = MetadataUtil.getCustomMetadata(beId);
        if (!(metadata.getContent() instanceof GspBusinessEntity)) {
            throw new ViewModelException(VoRtApiErrorCodes.GSP_VIEWOBJECT_RTWEBAPI_1008, null, beId);
        }
        return (GspBusinessEntity) metadata.getContent();
    }

    /**
     * 转换BE字段
     *
     * @param convertEleInfo
     * @return
     */
    @Path("convertBeElements")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String convertBeElementIdsToVmElements(String convertEleInfo) {
        return WebControllerService.getInstance().convertBeElementIdsToVmElements(convertEleInfo,
                (String path, String bePkgName, String beId) -> getBizEntity(path, bePkgName, beId));
    }

    @Path("addObject")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String addBizObject(String addObjInfo) {
        return WebControllerService.getInstance().addBizObject(addObjInfo,
                (String path, String bePkgName, String beId) -> getBizEntity(path, bePkgName, beId));
    }


    @Path("getBeAsso")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public String getBizAssoById(String info) {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        BizElementSerializer bizElementSerializer = new BizElementSerializer();
        GspAssociationSerializer associationDeserializer = new GspAssociationSerializer(bizElementSerializer);
        module.addSerializer(GspAssociation.class, associationDeserializer);
        mapper.registerModule(module);
        try {
            JsonNode node = mapper.readTree(info);
            String path = node.get("path").textValue();
            String voEleMappingJson = node.get("voEleMapping").textValue();
            String beAssoId = node.get("assoId").textValue();
            GspVoElementMapping voEleMapping = WebControllerService.getInstance().readVoEleMapping(voEleMappingJson);
            GspCommonAssociation bizAsso = WebControllerService.getInstance().getBizAsso(path, voEleMapping, beAssoId, this::getBizEntity);

            String bizAssoJson = mapper.writeValueAsString(bizAsso);
            return bizAssoJson;
        } catch (Exception e) {
            throw new ViewModelException(VoRtApiErrorCodes.GSP_VIEWOBJECT_RTWEBAPI_1009, e);
        }
    }
}
