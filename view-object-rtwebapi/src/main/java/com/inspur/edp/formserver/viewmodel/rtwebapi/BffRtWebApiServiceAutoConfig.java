package com.inspur.edp.formserver.viewmodel.rtwebapi;

import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BffRtWebApiServiceAutoConfig {
    @Bean
    public VMCodeGenController getRtBffWebApi() {
        return new VMCodeGenController();
    }

    @Bean
    public RESTEndpoint getBffRtWebApiEndpoint(VMCodeGenController genService) {
        return new RESTEndpoint(
                "/runtime/lcm/v1.0/bff",
                genService
        );
    }
}
