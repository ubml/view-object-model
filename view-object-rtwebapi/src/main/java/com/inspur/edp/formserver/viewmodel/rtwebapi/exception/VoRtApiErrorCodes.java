/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.rtwebapi.exception;

public class VoRtApiErrorCodes {
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1001 = "GSP_VIEWOBJECT_RTWEBAPI_1001";
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1002 = "GSP_VIEWOBJECT_RTWEBAPI_1002";
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1003 = "GSP_VIEWOBJECT_RTWEBAPI_1003";
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1004 = "GSP_VIEWOBJECT_RTWEBAPI_1004";
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1005 = "GSP_VIEWOBJECT_RTWEBAPI_1005";
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1006 = "GSP_VIEWOBJECT_RTWEBAPI_1006";
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1007 = "GSP_VIEWOBJECT_RTWEBAPI_1007";
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1008 = "GSP_VIEWOBJECT_RTWEBAPI_1008";
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1009 = "GSP_VIEWOBJECT_RTWEBAPI_1009";
    public final static String GSP_VIEWOBJECT_RTWEBAPI_1010 = "GSP_VIEWOBJECT_RTWEBAPI_1010";
}
