@echo off

set EXPORT_PATH=".\out\"

set LCM_SU_PATH="server\platform\runtime\lcm\libs\"
set PFCOMMON_SU_PATH="server\platform\common\libs\"
set DEV_SU_PATH="server\platform\dev\main\libs\"


set METADATA_TOOL_PATH="tools\deploy\metadata\runtime\libs\"


for /f "tokens=*" %%i in ('mvn help:evaluate "-Dexpression=project.version" "-DforceStdout=true" -q') do set version=%%i

call mvn clean verify


rmdir /q /s %EXPORT_PATH%

md %EXPORT_PATH%%LCM_SU_PATH%
md %EXPORT_PATH%%PFCOMMON_SU_PATH%
md %EXPORT_PATH%%METADATA_TOOL_PATH%
md %EXPORT_PATH%%DEV_SU_PATH%

goto view-object-api

:view-object-api
copy .\view-object-api\target\view-object-api-%version%.jar %EXPORT_PATH%%DEV_SU_PATH%view-object-api.jar
goto view-object-manager

:view-object-manager
copy .\view-object-manager\target\view-object-manager-%version%.jar %EXPORT_PATH%%PFCOMMON_SU_PATH%com.inspur.edp.formserver.vmmanager.jar
copy .\view-object-manager\target\view-object-manager-%version%.jar %EXPORT_PATH%%METADATA_TOOL_PATH%com.inspur.edp.formserver.vmmanager.jar
goto view-object-model

:view-object-model
copy .\view-object-model\target\view-object-model-%version%.jar %EXPORT_PATH%%PFCOMMON_SU_PATH%com.inspur.edp.formserver.viewmodel.jar
copy .\view-object-model\target\view-object-model-%version%.jar %EXPORT_PATH%%METADATA_TOOL_PATH%com.inspur.edp.formserver.viewmodel.jar
goto view-object-rtwebapi

:view-object-rtwebapi
copy .\view-object-rtwebapi\target\view-object-rtwebapi-%version%.jar %EXPORT_PATH%%LCM_SU_PATH%formserver-viewmodel-rtwebapi.jar
goto view-object-voextendinfo-server-api

:view-object-voextendinfo-server-api
copy .\view-object-voextendinfo-server-api\target\view-object-voextendinfo-server-api-%version%.jar %EXPORT_PATH%%LCM_SU_PATH%formserver-voextendinfo-server-api.jar
copy .\view-object-voextendinfo-server-api\target\view-object-voextendinfo-server-api-%version%.jar %EXPORT_PATH%%METADATA_TOOL_PATH%formserver-voextendinfo-server-api.jar
goto view-object-voextendinfo-server-core

:view-object-voextendinfo-server-core
copy .\view-object-voextendinfo-server-core\target\view-object-voextendinfo-server-core-%version%.jar %EXPORT_PATH%%LCM_SU_PATH%formserver-voextendinfo-server-core.jar
copy .\view-object-voextendinfo-server-core\target\view-object-voextendinfo-server-core-%version%.jar %EXPORT_PATH%%METADATA_TOOL_PATH%formserver-voextendinfo-server-core.jar
goto view-object-webapi

:view-object-webapi
copy .\view-object-webapi\target\view-object-webapi-%version%.jar %EXPORT_PATH%%DEV_SU_PATH%com.inspur.edp.formserver.viewmodel.webapi.jar
goto resource_file

:resource_file
md %EXPORT_PATH%server\platform\common\resources
xcopy /E .\resources  %EXPORT_PATH%server\platform\common\resources

:end