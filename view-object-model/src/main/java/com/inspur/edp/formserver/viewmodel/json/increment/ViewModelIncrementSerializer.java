package com.inspur.edp.formserver.viewmodel.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.json.increment.CommonModelIncrementSerializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementSerializer;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;
import com.inspur.edp.formserver.viewmodel.increment.ViewModelIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import lombok.var;

import java.io.IOException;
import java.util.HashMap;


/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelIncrementSerializer extends CommonModelIncrementSerializer {
    @Override
    protected CommonObjectIncrementSerializer getObjectIncrementSerializer() {
        return new ViewObjectIncrementSerializer();
    }

    @Override
    protected void writeExtendInfo(CommonModelIncrement value, JsonGenerator gen) {
        ViewModelIncrement increment = (ViewModelIncrement) value;
        writeHelpConfigs(increment.getValueHelpConfigs(), gen);
        writeVoActions(increment.getActions(), gen);
    }

    private void writeHelpConfigs(HashMap<String, HelpConfigIncrement> helpConfigs, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, ViewModelJsonConst.HelpIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : helpConfigs.entrySet()) {
            try {
                SerializerUtils.writeStartObject(gen);
                SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

                SerializerUtils.writePropertyName(gen, ViewModelJsonConst.HelpIncrement);
                HelpConfigIncrementSerializer serializer = new HelpConfigIncrementSerializer();
                serializer.serialize(item.getValue(), gen, null);
                SerializerUtils.writeEndObject(gen);
            } catch (IOException e) {
                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1032, e);
            }
        }
        SerializerUtils.WriteEndArray(gen);
    }

    private void writeVoActions(HashMap<String, VoActionIncrement> actions, JsonGenerator gen) {
        if (actions == null)
            return;
        SerializerUtils.writePropertyName(gen, ViewModelJsonConst.Actions);
        SerializerUtils.WriteStartArray(gen);
        for (var item : actions.entrySet()) {
            try {
                SerializerUtils.writeStartObject(gen);
                SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

                SerializerUtils.writePropertyName(gen, ViewModelJsonConst.Action);
                VoActionIncrementSerializer serializer = new VoActionIncrementSerializer();
                serializer.serialize(item.getValue(), gen, null);
                SerializerUtils.writeEndObject(gen);
            } catch (IOException e) {
                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1033, e);
            }
        }
        SerializerUtils.WriteEndArray(gen);
    }
}
