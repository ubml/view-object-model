package com.inspur.edp.formserver.viewmodel.common.copy;

import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;

/**
 * The Context While Copying
 *
 * @ClassName: CopyContext
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CopyContext {
    /**
     * 源Vo
     */
    private GspViewModel privateOriginVo;

    public final GspViewModel getOriginVo() {
        return privateOriginVo;
    }

    public final void setOriginVo(GspViewModel value) {

        privateOriginVo = value;
    }

    /**
     * 新Vo的元数据头节点
     */
//	private MetadataHeader privateMetadataHeader;
    private CopyContext privateMetadataHeader;

    public final CopyContext getMetadataHeader() {

        return privateMetadataHeader;
    }

    public final void setMetadataHeader(CopyContext value) {
        privateMetadataHeader = value;
    }

    private boolean includeVoActions = false;

    /**
     * 是否拷贝Vo操作
     */
    boolean getIncludeVoActions() {
        return this.includeVoActions;
    }

    public final void setIncludeVoActions(boolean value) {
        if (value) {
            throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1001, null);
        }
        includeVoActions = value;
    }
}