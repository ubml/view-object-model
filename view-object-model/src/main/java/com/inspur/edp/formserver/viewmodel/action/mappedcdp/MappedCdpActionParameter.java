package com.inspur.edp.formserver.viewmodel.action.mappedcdp;

import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParameter;

/**
 * The Definition Of Mapped Component Action Parameter
 *
 * @ClassName: MappedCdpActionParameter
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpActionParameter extends ViewModelParameter {
    public MappedCdpActionParameter() {
    }
}