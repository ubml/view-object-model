package com.inspur.edp.formserver.viewmodel.common;

public class ViewModelErrorCodes {
    public static final String GSP_BFF_SER_0001 = "GSP_BFF_SER_0001";
    public static final String GSP_BFF_VARDTNACTION_0003 = "GSP_BFF_VARDTNACTION_0001";
    public static final String GSP_BFF_ACTION_0002 = "GSP_BFF_ACTION_0002";
    public static final String GSP_BFF_ENTITY_0001 = "GSP_BFF_ENTITY_0001";
    public static final String GSP_BFF_MODEL_0001 = "GSP_BFF_MODEL_0001";
    public static final String GSP_BFF_ELEMENT_0001 = "GSP_BFF_MELEMENT_0001";
}
