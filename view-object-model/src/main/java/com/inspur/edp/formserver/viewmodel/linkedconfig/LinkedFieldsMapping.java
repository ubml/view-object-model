package com.inspur.edp.formserver.viewmodel.linkedconfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

public class LinkedFieldsMapping {
    /**
     * 当前表单VO上的字段标签
     */
    private String sourceField;
    /**
     * 当前表单VO上字段所在的实体节点编号
     */
    private String sourceObjCode;
    /**
     * 当前选择帮助VO上的字段标签
     */
    private String targetField;

    /**
     * 当前选择帮助VO上字段所在的的实体节点编号
     */

    @JsonProperty(ViewModelJsonConst.SourceField)
    public String getSourceField() {
        return sourceField;
    }

    public void setSourceField(String sourceField) {
        this.sourceField = sourceField;
    }

    @JsonProperty(ViewModelJsonConst.TargetField)
    public String getTargetField() {
        return targetField;
    }

    public void setTargetField(String targetField) {
        this.targetField = targetField;
    }

    private String targetObjCode;

    @JsonProperty(ViewModelJsonConst.SourceObjCode)
    public String getSourceObjCode() {
        return sourceObjCode;
    }

    public void setSourceObjCode(String sourceObjCode) {
        this.sourceObjCode = sourceObjCode;
    }

    @JsonProperty(ViewModelJsonConst.TargetObjCode)
    public String getTargetObjCode() {
        return targetObjCode;
    }

    public void setTargetObjCode(String targetObjCode) {
        this.targetObjCode = targetObjCode;
    }

}
