package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs;

import com.inspur.edp.cef.designtime.api.dtconsistencycheck.AbstractDtEventArgs;

public abstract class AbstractVoEntityArgs extends AbstractDtEventArgs {

    public String viewModelId;
    public String viewObjectId;

    public AbstractVoEntityArgs() {
    }

    public AbstractVoEntityArgs(String viewModelId, String viewObjectId) {
        this.viewModelId = viewModelId;
        this.viewObjectId = viewObjectId;
    }

    public String getViewModelId() {
        return viewModelId;
    }

    public void setViewModelId(String viewModelId) {
        this.viewModelId = viewModelId;
    }

    public String getViewObjectId() {
        return viewObjectId;
    }

    public void setViewObjectId(String viewObjectId) {
        this.viewObjectId = viewObjectId;
    }
}
