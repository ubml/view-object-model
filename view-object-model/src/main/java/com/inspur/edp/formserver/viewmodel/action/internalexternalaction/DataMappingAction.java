package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

/**
 * The Definition Of Data Mapping Action
 *
 * @ClassName: DataMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DataMappingAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String id = "42221ca3-9ee4-40af-89d2-ff4c8b466ac3";
    public static final String code = "DataMapping";

    public DataMappingAction() {
        setID(id);
        setCode(code);
        setName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.ACTION_DATAMAPPING_NAME));
    }
}