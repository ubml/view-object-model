package com.inspur.edp.formserver.viewmodel.extendinfo.api;

import com.inspur.edp.formserver.viewmodel.extendinfo.entity.GspVoExtendInfo;

import java.util.List;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface GspVoExtendInfoService {
    /**
     * 根据ID获取某条Vo扩展信息
     *
     * @param id
     * @return
     */
    GspVoExtendInfo getVoExtendInfo(String id);

    /**
     * 根据configId获取某条Vo扩展信息
     *
     * @param configId
     * @return
     */
    GspVoExtendInfo getVoExtendInfoByConfigId(String configId);

    /**
     * 获取所有BE扩展信息
     *
     * @return
     */
    List<GspVoExtendInfo> getVoExtendInfos();

    /**
     * 保存
     *
     * @param infos
     */
    void saveGspVoExtendInfos(List<GspVoExtendInfo> infos);

    void deleteVoExtendInfo(String id);

    /**
     * 根据BEId获取某条VoID
     *
     * @param Id
     * @return
     */
    List<GspVoExtendInfo> getVoId(String Id);

}
