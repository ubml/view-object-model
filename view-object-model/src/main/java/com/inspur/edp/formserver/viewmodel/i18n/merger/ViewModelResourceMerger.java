package com.inspur.edp.formserver.viewmodel.i18n.merger;

import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonModelResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonobjectResourceMerger;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewObject;

public class ViewModelResourceMerger extends CommonModelResourceMerger {

    public ViewModelResourceMerger(GspViewModel model,
                                   ICefResourceMergeContext context) {
        super(model, context);
    }

    @Override
    protected String getModelDescriptionName() {
        return null;
    }

    @Override
    protected CommonobjectResourceMerger getObjectResourceMerger(
            ICefResourceMergeContext context, IGspCommonObject obj) {
        return new ViewObjResourceMerger((GspViewObject) obj, context);
    }

    @Override
    protected void extractExtendProperties(IGspCommonModel model) {

    }
}
