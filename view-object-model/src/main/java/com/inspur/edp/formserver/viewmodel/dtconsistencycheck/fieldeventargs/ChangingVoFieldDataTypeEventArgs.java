package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs;

public class ChangingVoFieldDataTypeEventArgs extends AbstractVofieldEventArgs {

    protected String newDateType;
    protected String originalDataType;

    public ChangingVoFieldDataTypeEventArgs() {
    }

    public ChangingVoFieldDataTypeEventArgs(String newDateType, String originalDataType) {
        this.newDateType = newDateType;
        this.originalDataType = originalDataType;
    }

    public String getNewDateType() {
        return newDateType;
    }

    public void setNewDateType(String newDateType) {
        this.newDateType = newDateType;
    }

    public String getOriginalDataType() {
        return originalDataType;
    }

    public void setOriginalDataType(String originalDataType) {
        this.originalDataType = originalDataType;
    }
}
