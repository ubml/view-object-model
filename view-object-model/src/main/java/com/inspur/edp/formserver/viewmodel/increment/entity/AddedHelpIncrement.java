package com.inspur.edp.formserver.viewmodel.increment.entity;

import com.inspur.edp.cef.designtime.api.increment.AddedIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;

public class AddedHelpIncrement extends HelpConfigIncrement implements AddedIncrement {
    private ValueHelpConfig helpConfig;

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Added;
    }

    public ValueHelpConfig getHelpConfig() {
        return helpConfig;
    }

    public void setHelpConfig(ValueHelpConfig helpConfig) {
        this.helpConfig = helpConfig;
    }
}
