package com.inspur.edp.formserver.viewmodel.dataextendinfo;

import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;

/**
 * The Definition Of View Object Data Exntend Info
 *
 * @ClassName: VoDataExtendInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoDataExtendInfo {
    ///#region 私有字段

    private VMActionCollection dataMappingActions;
    private VMActionCollection beforeQueryActions;
    private VMActionCollection queryActions;
    private VMActionCollection afterQueryActions;
    private VMActionCollection retrieveActions;
    private VMActionCollection beforeRetrieveActions;
    private VMActionCollection afterRetrieveActions;
    private VMActionCollection beforeModifyActions;
    private VMActionCollection modifyActions;
    private VMActionCollection afterModifyActions;
    private VMActionCollection changesetMappingActions;
    private VMActionCollection beforeCreateActions;
    private VMActionCollection createActions;
    private VMActionCollection beforeDeleteActions;
    private VMActionCollection deleteActions;
    private VMActionCollection afterSaveActions;
    private VMActionCollection beforeSaveActions;
    private VMActionCollection afterCreateActions;
    private VMActionCollection dataReversalMappingActions;
    private VMActionCollection changesetReversalMappingActions;
    private VMActionCollection afterDeleteActions;
    private VMActionCollection beforeMultiDeleteActions;
    private VMActionCollection multiDeleteActions;
    private VMActionCollection afterMultiDeleteActions;
    private VMActionCollection saveActions;
    private VMActionCollection createChildActions;
    private VMActionCollection deleteChildActions;
    private VMActionCollection cancelActions;

    ///#endregion
    public final VMActionCollection getBeforeMultiDeleteActions() {
        if (beforeMultiDeleteActions == null) {
            beforeMultiDeleteActions = new VMActionCollection();
        }

        return beforeMultiDeleteActions;
    }

    public void setBeforeMultiDeleteActions(VMActionCollection value) {
        this.beforeMultiDeleteActions = value;
    }

    public final VMActionCollection getMultiDeleteActions() {
        if (multiDeleteActions == null) {
            multiDeleteActions = new VMActionCollection();
        }

        return multiDeleteActions;
    }

    public void setMultiDeleteActions(VMActionCollection value) {
        this.multiDeleteActions = value;
    }

    public final VMActionCollection getAfterMultiDeleteActions() {
        if (afterMultiDeleteActions == null) {
            afterMultiDeleteActions = new VMActionCollection();
        }

        return afterMultiDeleteActions;
    }

    public void setAfterMultiDeleteActions(VMActionCollection value) {
        this.afterMultiDeleteActions = value;
    }

    /**
     * 数据Mapping
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getDataMappingActions() {
        if (dataMappingActions == null) {
            dataMappingActions = new VMActionCollection();
        }

        return dataMappingActions;
    }

    public void setDataMappingActions(VMActionCollection value) {
        this.dataMappingActions = value;
    }

    /**
     * 查询数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeQueryActions() {
        if (beforeQueryActions == null) {
            beforeQueryActions = new VMActionCollection();
        }

        return beforeQueryActions;
    }

    public void setBeforeQueryActions(VMActionCollection value) {
        this.beforeQueryActions = value;
    }

    /**
     * 查询数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getQueryActions() {
        if (queryActions == null) {
            queryActions = new VMActionCollection();
        }

        return queryActions;
    }

    public void setQueryActions(VMActionCollection value) {
        this.queryActions = value;
    }

    /**
     * 查询数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterQueryActions() {
        if (afterQueryActions == null) {
            afterQueryActions = new VMActionCollection();
        }

        return afterQueryActions;
    }

    public void setAfterQueryActions(VMActionCollection value) {
        this.afterQueryActions = value;
    }

    /**
     * 检索数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeRetrieveActions() {
        if (beforeRetrieveActions == null) {
            beforeRetrieveActions = new VMActionCollection();
        }

        return beforeRetrieveActions;
    }

    public void setBeforeRetrieveActions(VMActionCollection value) {
        this.beforeRetrieveActions = value;
    }

    /**
     * 检索数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getRetrieveActions() {
        if (retrieveActions == null) {
            retrieveActions = new VMActionCollection();
        }

        return retrieveActions;
    }

    //	public void setRetrieveActions(VMActionCollection value)
    public void setRetrieveActions(VMActionCollection value) {
        this.retrieveActions = value;
    }

    /**
     * 检索数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterRetrieveActions() {
        if (afterRetrieveActions == null) {
            afterRetrieveActions = new VMActionCollection();
        }

        return afterRetrieveActions;
    }

    public void setAfterRetrieveActions(VMActionCollection value) {
        this.afterRetrieveActions = value;
    }

    /**
     * 修改数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeModifyActions() {
        if (beforeModifyActions == null) {
            beforeModifyActions = new VMActionCollection();
        }

        return beforeModifyActions;
    }

    public void setBeforeModifyActions(VMActionCollection value) {
        this.beforeModifyActions = value;
    }

    /**
     * 修改数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getModifyActions() {
        if (modifyActions == null) {
            modifyActions = new VMActionCollection();
        }

        return modifyActions;
    }

    public void setModifyActions(VMActionCollection value) {
        this.modifyActions = value;
    }

    /**
     * 修改数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterModifyActions() {
        if (afterModifyActions == null) {
            afterModifyActions = new VMActionCollection();
        }

        return afterModifyActions;
    }

    public void setAfterModifyActions(VMActionCollection value) {
        this.afterModifyActions = value;
    }

    /**
     * 变更集Mapping
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getChangesetMappingActions() {
        if (changesetMappingActions == null) {
            changesetMappingActions = new VMActionCollection();
        }

        return changesetMappingActions;
    }

    //	public void setChangesetMappingActions(VMActionCollection value)
    public void setChangesetMappingActions(VMActionCollection value) {
        this.changesetMappingActions = value;
    }

    /**
     * 新增数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeCreateActions() {
        if (beforeCreateActions == null) {
            beforeCreateActions = new VMActionCollection();
        }

        return beforeCreateActions;
    }

    public void setBeforeCreateActions(VMActionCollection value) {
        this.beforeCreateActions = value;
    }


    /**
     * 新增数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getCreateActions() {
        if (createActions == null) {
            createActions = new VMActionCollection();
        }

        return createActions;
    }

    public void setCreateActions(VMActionCollection value) {
        this.createActions = value;
    }

    /**
     * 新增数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterCreateActions() {
        if (afterCreateActions == null) {
            afterCreateActions = new VMActionCollection();
        }

        return afterCreateActions;
    }

    //	public void setAfterCreateActions(VMActionCollection value)
    public void setAfterCreateActions(VMActionCollection value) {
        this.afterCreateActions = value;
    }

    public final VMActionCollection getCreateChildActions() {
        if (createChildActions == null) {
            createChildActions = new VMActionCollection();
        }
        return createChildActions;
    }

    public void setCreateChildActions(VMActionCollection value) {
        this.createChildActions = value;
    }

    /**
     * 删除数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeDeleteActions() {
        if (beforeDeleteActions == null) {
            beforeDeleteActions = new VMActionCollection();
        }

        return beforeDeleteActions;
    }

    //	public void setBeforeDeleteActions(VMActionCollection value)
    public void setBeforeDeleteActions(VMActionCollection value) {
        this.beforeDeleteActions = value;
    }


    /**
     * 删除数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getDeleteActions() {
        if (deleteActions == null) {
            deleteActions = new VMActionCollection();
        }

        return deleteActions;
    }

    public void setDeleteActions(VMActionCollection value) {
        this.deleteActions = value;
    }

    public final VMActionCollection getDeleteChildActions() {
        if (deleteChildActions == null) {
            deleteChildActions = new VMActionCollection();
        }
        return deleteChildActions;
    }

    public void setDeleteChildActions(VMActionCollection value) {
        this.deleteChildActions = value;
    }

    /**
     * 删除数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterDeleteActions() {
        if (afterDeleteActions == null) {
            afterDeleteActions = new VMActionCollection();
        }

        return afterDeleteActions;
    }

    public void setAfterDeleteActions(VMActionCollection value) {
        this.afterDeleteActions = value;
    }


    /**
     * 保存数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeSaveActions() {
        if (beforeSaveActions == null) {
            beforeSaveActions = new VMActionCollection();
        }

        return beforeSaveActions;
    }

    public void setBeforeSaveActions(VMActionCollection value) {
        this.beforeSaveActions = value;
    }

    /**
     * 数据反向Mapping
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getDataReversalMappingActions() {
        if (dataReversalMappingActions == null) {
            dataReversalMappingActions = new VMActionCollection();
        }

        return dataReversalMappingActions;
    }

    public void setDataReversalMappingActions(VMActionCollection value) {
        this.dataReversalMappingActions = value;
    }

    /**
     * 保存数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterSaveActions() {
        if (afterSaveActions == null) {
            afterSaveActions = new VMActionCollection();
        }

        return afterSaveActions;
    }

    public void setAfterSaveActions(VMActionCollection value) {
        this.afterSaveActions = value;
    }


    /**
     * 变更集反向Mapping
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getChangesetReversalMappingActions() {
        if (changesetReversalMappingActions == null) {
            changesetReversalMappingActions = new VMActionCollection();
        }

        return changesetReversalMappingActions;
    }

    public void setChangesetReversalMappingActions(VMActionCollection value) {
        this.changesetReversalMappingActions = value;
    }

    /**
     * 保存
     **/
    public final VMActionCollection getSaveActions() {
        if (saveActions == null) {
            saveActions = new VMActionCollection();
        }
        return saveActions;
    }

    public void setSaveActions(VMActionCollection value) {
        this.saveActions = value;
    }

    /**
     * 取消扩展操作
     *
     * @return
     */
    public final VMActionCollection getCancelActions() {
        if (cancelActions == null) {
            cancelActions = new VMActionCollection();
        }
        return cancelActions;
    }

    public void setCancelActions(VMActionCollection value) {
        this.cancelActions = value;
    }

    public boolean isAllNull() {
        if (dataMappingActions == null || dataMappingActions.size() == 0)
            if (beforeQueryActions == null || beforeQueryActions.size() == 0)
                if (queryActions == null || queryActions.size() == 0)
                    if (afterQueryActions == null || afterQueryActions.size() == 0)
                        if (retrieveActions == null || retrieveActions.size() == 0)
                            if (beforeRetrieveActions == null || beforeRetrieveActions.size() == 0)
                                if (afterRetrieveActions == null || afterRetrieveActions.size() == 0)
                                    if (beforeModifyActions == null || beforeModifyActions.size() == 0)
                                        if (modifyActions == null || modifyActions.size() == 0)
                                            if (afterModifyActions == null || afterModifyActions.size() == 0)
                                                if (changesetMappingActions == null || changesetMappingActions.size() == 0)
                                                    if (beforeCreateActions == null || beforeCreateActions.size() == 0)
                                                        if (createActions == null || createActions.size() == 0)
                                                            if (beforeDeleteActions == null || beforeDeleteActions.size() == 0)
                                                                if (deleteActions == null || deleteActions.size() == 0)
                                                                    if (afterSaveActions == null || afterSaveActions.size() == 0)
                                                                        if (beforeSaveActions == null || beforeSaveActions.size() == 0)
                                                                            if (saveActions == null || saveActions.size() == 0)
                                                                                if (afterCreateActions == null || afterCreateActions.size() == 0)
                                                                                    if (dataReversalMappingActions == null || dataReversalMappingActions.size() == 0)
                                                                                        if (changesetReversalMappingActions == null || changesetReversalMappingActions.size() == 0)
                                                                                            if (afterDeleteActions == null || afterDeleteActions.size() == 0)
                                                                                                if (beforeMultiDeleteActions == null || beforeMultiDeleteActions.size() == 0)
                                                                                                    if (multiDeleteActions == null || multiDeleteActions.size() == 0)
                                                                                                        if (afterMultiDeleteActions == null || afterMultiDeleteActions.size() == 0)
                                                                                                            if (createChildActions == null || createChildActions.size() == 0)
                                                                                                                if (deleteChildActions == null || deleteChildActions.size() == 0)
                                                                                                                    if (cancelActions == null || cancelActions.size() == 0)
                                                                                                                        return true;

        return false;
    }

    public VoDataExtendInfo initVoDataExendInfo(VoDataExtendInfo voDataExtendInfo) {
        voDataExtendInfo.dataMappingActions = new VMActionCollection();
        voDataExtendInfo.beforeQueryActions = new VMActionCollection();
        voDataExtendInfo.queryActions = new VMActionCollection();
        voDataExtendInfo.afterQueryActions = new VMActionCollection();
        voDataExtendInfo.retrieveActions = new VMActionCollection();
        voDataExtendInfo.beforeRetrieveActions = new VMActionCollection();
        voDataExtendInfo.afterRetrieveActions = new VMActionCollection();
        voDataExtendInfo.beforeModifyActions = new VMActionCollection();
        voDataExtendInfo.modifyActions = new VMActionCollection();
        voDataExtendInfo.afterModifyActions = new VMActionCollection();
        voDataExtendInfo.changesetMappingActions = new VMActionCollection();
        voDataExtendInfo.beforeCreateActions = new VMActionCollection();
        voDataExtendInfo.createActions = new VMActionCollection();
        voDataExtendInfo.beforeDeleteActions = new VMActionCollection();
        voDataExtendInfo.deleteActions = new VMActionCollection();
        voDataExtendInfo.afterSaveActions = new VMActionCollection();
        voDataExtendInfo.beforeSaveActions = new VMActionCollection();
        voDataExtendInfo.saveActions = new VMActionCollection();
        voDataExtendInfo.afterCreateActions = new VMActionCollection();
        voDataExtendInfo.dataReversalMappingActions = new VMActionCollection();
        voDataExtendInfo.changesetReversalMappingActions = new VMActionCollection();
        voDataExtendInfo.afterDeleteActions = new VMActionCollection();
        voDataExtendInfo.beforeMultiDeleteActions = new VMActionCollection();
        voDataExtendInfo.multiDeleteActions = new VMActionCollection();
        voDataExtendInfo.afterMultiDeleteActions = new VMActionCollection();
        voDataExtendInfo.deleteChildActions = new VMActionCollection();
        voDataExtendInfo.createChildActions = new VMActionCollection();
        voDataExtendInfo.cancelActions = new VMActionCollection();
        return voDataExtendInfo;
    }
}
