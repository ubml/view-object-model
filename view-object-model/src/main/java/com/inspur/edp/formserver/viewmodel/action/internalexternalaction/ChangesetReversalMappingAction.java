package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

/**
 * The Definition Of Changeset Reversal Mapping Action
 *
 * @ClassName: ChangesetReversalMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ChangesetReversalMappingAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String id = "301c5991-a32d-4221-88bf-8c9d07bdd884";
    public static final String code = "ChangesetReversalMapping";

    public ChangesetReversalMappingAction() {
        setID(id);
        setCode(code);
        setName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.ACTION_CHANGESETREVERSALMAPPING_NAME));
    }
}