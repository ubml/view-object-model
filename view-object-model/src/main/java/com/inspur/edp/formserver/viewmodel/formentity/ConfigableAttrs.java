package com.inspur.edp.formserver.viewmodel.formentity;

import lombok.Getter;

@Getter
public class ConfigableAttrs {
    private String code;
    private String name;

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }
}
