package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs;

public class ChangingVoFieldObjectTypeEventArgs extends AbstractVofieldEventArgs {
    protected String newObjectType;
    protected String originalObjectType;

    public ChangingVoFieldObjectTypeEventArgs() {
    }

    public ChangingVoFieldObjectTypeEventArgs(String newObjectType, String originalObjectType) {
        this.newObjectType = newObjectType;
        this.originalObjectType = originalObjectType;
    }

    public String getNewObjectType() {
        return newObjectType;
    }

    public void setNewObjectType(String newObjectType) {
        this.newObjectType = newObjectType;
    }

    public String getOriginalObjectType() {
        return originalObjectType;
    }

    public void setOriginalObjectType(String originalObjectType) {
        this.originalObjectType = originalObjectType;
    }
}
