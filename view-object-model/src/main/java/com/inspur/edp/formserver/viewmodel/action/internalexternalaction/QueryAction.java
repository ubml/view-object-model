package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

/**
 * The Definition Of Query Action
 *
 * @ClassName: QueryAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class QueryAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String id = "6fe68bfa-7c1b-4d6b-a7ef-14654168ae75";
    public static final String code = "Query";

    public QueryAction() {
        setID(id);
        setCode(code);
        setName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.ACTION_QUERY_NAME));
    }

}