package com.inspur.edp.formserver.viewmodel.controlrule.rule;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.das.commonmodel.controlrule.CmFieldControlRule;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.parser.VoFieldRuleParser;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.serializer.VoFieldControlRuleSerializer;

@JsonSerialize(using = VoFieldControlRuleSerializer.class)
@JsonDeserialize(using = VoFieldRuleParser.class)
public class VoFeildControlRule extends CmFieldControlRule {
}
