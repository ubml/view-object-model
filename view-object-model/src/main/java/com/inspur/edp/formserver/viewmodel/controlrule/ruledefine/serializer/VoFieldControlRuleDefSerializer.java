package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmFieldControlRuleDefSerializer;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoFieldControlRuleDef;

public class VoFieldControlRuleDefSerializer<T extends VoFieldControlRuleDef> extends CmFieldControlRuleDefSerializer<T> {

    protected void writeCmFieldRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

        super.writeCmFieldRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
    }
}
