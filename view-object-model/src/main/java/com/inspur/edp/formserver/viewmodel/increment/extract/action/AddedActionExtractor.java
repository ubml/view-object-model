package com.inspur.edp.formserver.viewmodel.increment.extract.action;

import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.AddedVoActionIncrement;

public class AddedActionExtractor {

    public AddedActionExtractor() {
    }

    public AddedVoActionIncrement extract(ViewModelAction voAction) {
        AddedVoActionIncrement addedEntityIncrement = this.createIncrement();
        addedEntityIncrement.setAction(voAction);
        return addedEntityIncrement;
    }

    protected AddedVoActionIncrement createIncrement() {
        return new AddedVoActionIncrement();
    }
}
