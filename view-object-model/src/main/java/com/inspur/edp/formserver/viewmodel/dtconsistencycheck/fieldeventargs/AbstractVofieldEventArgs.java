package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs;

import com.inspur.edp.cef.designtime.api.dtconsistencycheck.AbstractDtEventArgs;

public abstract class AbstractVofieldEventArgs extends AbstractDtEventArgs {

    public String viewModelId;
    public String viewObjectId;
    public String viewFieldId;

    public AbstractVofieldEventArgs() {
    }

    public String getViewModelId() {
        return viewModelId;
    }

    public void setViewModelId(String viewModelId) {
        this.viewModelId = viewModelId;
    }

    public String getViewObjectId() {
        return viewObjectId;
    }

    public void setViewObjectId(String viewObjectId) {
        this.viewObjectId = viewObjectId;
    }

    public String getViewFieldId() {
        return viewFieldId;
    }

    public void setViewFieldId(String viewFieldId) {
        this.viewFieldId = viewFieldId;
    }
}
