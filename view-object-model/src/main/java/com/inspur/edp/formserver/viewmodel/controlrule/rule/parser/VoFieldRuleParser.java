package com.inspur.edp.formserver.viewmodel.controlrule.rule.parser;

import com.inspur.edp.das.commonmodel.controlrule.CmFieldControlRule;
import com.inspur.edp.das.commonmodel.controlrule.parser.CmFieldRuleParser;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.VoFeildControlRule;

public class VoFieldRuleParser extends CmFieldRuleParser {

    protected CmFieldControlRule createCmFieldRuleDefinition() {
        return new VoFeildControlRule();
    }

}
