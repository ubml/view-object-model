package com.inspur.edp.formserver.viewmodel.action.viewmodelbase;


import com.inspur.edp.formserver.viewmodel.common.VMParameterType;

/**
 * The Definition Of  Void View Model Return Value
 *
 * @ClassName: ViewModelVoidReturnValue
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelVoidReturnValue extends ViewModelReturnValue {
    public static final String assembly = "mscorlib.dll";
    public static final String className = "Void";
    public static final String javaClassName = "void";

    /**
     * 参数类对应程序集类
     */
    @Override
    public String getAssembly() {
        return assembly;
    }

    @Override
    public void setAssembly(String value) {
    }

    /**
     * 参数类名
     */
    @Override
    public String getClassName() {
        return javaClassName;
    }

    @Override
    public void setClassName(String value) {
    }

    public String getDotnetClassName() {
        return className;
    }

    public void setDotnetClassName(String dotnetClassName) {
    }


    /**
     * 参数类型，与Assembly和ClassName关联
     */
    @Override
    public VMParameterType getParameterType() {
        return VMParameterType.Custom;

    }

    @Override
    public void setParameterType(VMParameterType value) {
    }

    /**
     * 新版判断动作返回值为空属性
     *
     * @return
     */
    public boolean isVoidReturnType() {
        return true;
    }

    public void setVoidReturnType(boolean voidReturnType) {

    }
}