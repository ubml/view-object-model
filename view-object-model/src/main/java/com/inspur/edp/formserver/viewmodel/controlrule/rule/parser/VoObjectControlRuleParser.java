package com.inspur.edp.formserver.viewmodel.controlrule.rule.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;
import com.inspur.edp.das.commonmodel.controlrule.parser.CmEntityControlRuleParser;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.VoObjControlRule;

public class VoObjectControlRuleParser extends CmEntityControlRuleParser {

    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        switch (childTypeName) {
            case CommonModelNames.ChildObject:
                return new VoObjectControlRuleParser();
            case CommonModelNames.Element:
                return new VoFieldRuleParser();
        }
        return null;
    }

    @Override
    protected CmEntityControlRule createCmEntityRule() {
        return new VoObjControlRule();
    }

}
