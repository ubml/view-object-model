package com.inspur.edp.formserver.viewmodel.json.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoElementMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoElementSourceType;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

/**
 * The Josn Serializer Of View Model Element Mapping
 *
 * @ClassName: GspVoElementMappingSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoElementMappingSerializer extends ViewModelMappingSerializer {

    public GspVoElementMappingSerializer() {
    }

    public GspVoElementMappingSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected final void writeExtendMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
        GspVoElementMapping voMapping = (GspVoElementMapping) mapping;
        if (isFull || voMapping.getSourceType() != GspVoElementSourceType.BeElement)
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.SourceType, voMapping.getSourceType().toString());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetObjectId, voMapping.getTargetObjectId());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetElementId, voMapping.getTargetElementId());
    }
}
