package com.inspur.edp.formserver.viewmodel.increment.entity;

import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.increment.ModifyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;

import java.util.HashMap;

public class ModifyHelpIncrement extends HelpConfigIncrement implements ModifyIncrement {

    private HashMap<String, PropertyIncrement> changeProperties;

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Modify;
    }

    @Override
    public HashMap<String, PropertyIncrement> getChangeProperties() {
        return null;
    }

    private ValueHelpConfig helpConfig;

    public ValueHelpConfig getHelpConfig() {
        return helpConfig;
    }

    public void setHelpConfig(ValueHelpConfig helpConfig) {
        this.helpConfig = helpConfig;
        setElementId(helpConfig.getElementId());
    }
}
