package com.inspur.edp.formserver.viewmodel.i18n.merger;

import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonElementResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonobjectResourceMerger;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.formserver.viewmodel.GspViewObject;

public class ViewObjResourceMerger extends CommonobjectResourceMerger {

    public ViewObjResourceMerger(GspViewObject viewObject,
                                 ICefResourceMergeContext context) {
        super(viewObject, context);
    }

    @Override
    protected void extractExtendObjProperties(IGspCommonObject iGspCommonObject) {

    }

    @Override
    protected CommonElementResourceMerger getCommonEleResourceMerger(
            ICefResourceMergeContext context, IGspCommonElement element) {
        return new ViewEleResourceMerger((GspViewModelElement) element, context);
    }

    @Override
    protected CommonobjectResourceMerger getObjectResourceMerger(
            ICefResourceMergeContext context, IGspCommonObject obj) {
        return new ViewObjResourceMerger((GspViewObject) obj, context);
    }
}
