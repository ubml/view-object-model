package com.inspur.edp.formserver.viewmodel.viewmodeldtevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs.ChangingViewObjectCodeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs.RemovingViewObjectEventArgs;
/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoEntityDTEventListener implements IVoEntityDTEventListener {

    @Override
    public ChangingViewObjectCodeEventArgs changingViewObjectCode(
            ChangingViewObjectCodeEventArgs args) {
        return null;
    }

    @Override
    public RemovingViewObjectEventArgs removingViewObject(RemovingViewObjectEventArgs args) {
        return null;
    }
}
