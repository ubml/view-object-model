package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

/**
 * The Json Serializer Of Mapped Component Action Definition
 *
 * @ClassName: MappedCdpActionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpActionSerializer extends VmActionSerializer<MappedCdpAction> {

    public MappedCdpActionSerializer() {
    }

    public MappedCdpActionSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected VmParameterSerializer getParaConvertor() {
        return new MappedCdpParaSerializer(isFull);
    }

    @Override
    protected void writeExtendOperationProperty(JsonGenerator writer, ViewModelAction op) {
        MappedCdpAction action = (MappedCdpAction) op;
        if (isFull || (action.getComponentEntityId() != null && !"".equals(action.getComponentEntityId()))) {
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ComponentEntityId, action.getComponentEntityId());
        }
        if (isFull || (action.getComponentPkgName() != null && !"".equals(action.getComponentPkgName()))) {
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ComponentPkgName, action.getComponentPkgName());
        }
        if (isFull || !action.getIsGenerateComponent())
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsGenerateComponent, action.getIsGenerateComponent());
    }
}
