package com.inspur.edp.formserver.viewmodel.json.increment;

import com.inspur.edp.das.commonmodel.json.element.CmElementSerializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonElementIncrementSerializer;
import com.inspur.edp.formserver.viewmodel.json.element.ViewElementSerializer;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewElementIncrementSerializer extends CommonElementIncrementSerializer {
    @Override
    protected CmElementSerializer getCmElementSerializer() {
        return new ViewElementSerializer();
    }
}
