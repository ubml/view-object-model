package com.inspur.edp.formserver.viewmodel.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class ViewModelException extends CAFRuntimeException {
    public final static String SU = "pfcommon";
    public final static String RESOURCE_FILE = "viewobject_model_designtime.properties";

    public ViewModelException(String exceptionCode, Throwable innerException, String... messageParams) {
        super(SU, RESOURCE_FILE, exceptionCode, messageParams, innerException);
    }

    public ViewModelException(String exceptionCode, Throwable innerException, ExceptionLevel level, String... messageParams) {
        super(SU, RESOURCE_FILE, exceptionCode, messageParams, innerException, level);
    }

    public ViewModelException(String exceptionCode, Throwable innerException, ExceptionLevel level, boolean bizException, String... messageParams) {
        super(SU, RESOURCE_FILE, exceptionCode, messageParams, innerException, level, bizException);
    }

    public ViewModelException(String exceptionCode, Throwable innerException, boolean bizException, String... messageParams) {
        super(SU, RESOURCE_FILE, exceptionCode, messageParams, innerException, ExceptionLevel.Error, bizException);
    }

    public ViewModelException(String serviceUnitCode, String exceptionCode, String message, Throwable innerException, ExceptionLevel level) {
        super(SU, exceptionCode, message, innerException, level);
    }

    public ViewModelException(String serviceUnitCode, String exceptionCode, String message, Throwable innerException, ExceptionLevel level, boolean bizException) {
        super(SU, exceptionCode, message, innerException, level, bizException);
    }

    public ViewModelException(String exceptionCode, String message, Throwable innerException, ExceptionLevel level) {
        super(SU, exceptionCode, message, innerException, level);
    }

    public ViewModelException(Throwable innerException) {
        super(SU, "", "", innerException, ExceptionLevel.Error);
    }

    public ViewModelException(String exceptionCode, String message, Throwable innerException, ExceptionLevel level, boolean bizException) {
        super(SU, exceptionCode, message, innerException, level, bizException);
    }

    public ViewModelException(Throwable innerException, boolean bizException) {
        super(SU, "", "", innerException, ExceptionLevel.Error, bizException);
    }
}
