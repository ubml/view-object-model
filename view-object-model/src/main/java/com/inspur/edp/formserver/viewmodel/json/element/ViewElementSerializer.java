package com.inspur.edp.formserver.viewmodel.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.json.element.CmElementSerializer;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.mapping.GspVoElementMappingSerializer;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionCollectionSerializer;
import lombok.var;
/**
 * The  Josn Serializer Of View Model Elemnet
 *
 * @ClassName: ViewElementSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewElementSerializer extends CmElementSerializer {

    public ViewElementSerializer() {
    }

    public ViewElementSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected void writeExtendElementBaseProperty(JsonGenerator jsonGenerator, IGspCommonElement iGspCommonElement) {

    }

    @Override
    protected void writeExtendElementSelfProperty(JsonGenerator writer, IGspCommonElement commonElement) {
        GspViewModelElement bizElement = (GspViewModelElement) commonElement;
        if (isFull || bizElement.getIsBeckendOnly())
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsBeckendOnly, bizElement.getIsBeckendOnly());
        if (isFull || bizElement.getImmediateSubmission())
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ImmediateSubmission, bizElement.getImmediateSubmission());
        if (isFull || bizElement.getIsVirtualViewElement())
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsVirtualViewElement, bizElement.getIsVirtualViewElement());
        if (isFull || bizElement.isEnableMultiLanguageInput())
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.EnableMultiLanguageInput, bizElement.isEnableMultiLanguageInput());
        if (isFull || !bizElement.getShowInFilter())
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ShowInFilter, bizElement.getShowInFilter());
        if (isFull || !bizElement.getShowInSort())
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ShowInSort, bizElement.getShowInSort());
        writeMapping(writer, bizElement);
        writeVMActionCollection(writer, bizElement);
        writeVMHelpConfig(writer, bizElement);
        writeExtendProperties(writer, bizElement);
    }

    private void writeMapping(JsonGenerator writer, GspViewModelElement ve) {
        if (ve.getMapping() == null)
            return;
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.Mapping);
        GspVoElementMappingSerializer convertor = new GspVoElementMappingSerializer(isFull);
        convertor.serialize(ve.getMapping(), writer, null);
    }

    private void writeVMActionCollection(JsonGenerator writer, GspViewModelElement ve) {
        if (ve.getHelpActions() == null)
            return;
        if (!isFull && ve.getHelpActions().size() == 0)
            return;
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.HelpActions);
        VmActionCollectionSerializer convertor = new VmActionCollectionSerializer(isFull);
        convertor.serialize(ve.getHelpActions(), writer, null);
    }

    private void writeVMHelpConfig(JsonGenerator writer, GspViewModelElement ve) {
        if (ve.getVMHelpConfig() == null)
            return;
        if (!isFull && "".equals(ve.getVMHelpConfig().getHelpId()))
            return;
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.VMHelpConfig);
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.HelpId, ve.getVMHelpConfig().getHelpId());
        SerializerUtils.writeEndObject(writer);
    }

    private void writeExtendProperties(JsonGenerator writer, GspViewModelElement vm) {
        var dic = vm.getExtendProperties();
        if (!isFull && (dic == null || dic.size() == 0))
            return;
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ExtendProperties);
        SerializerUtils.writeStartObject(writer);
        if (dic != null && dic.size() > 0) {
            for (var item : dic.entrySet()) {
                SerializerUtils.writePropertyValue(writer, item.getKey(), item.getValue());
            }
        }
        SerializerUtils.writeEndObject(writer);
    }
}
