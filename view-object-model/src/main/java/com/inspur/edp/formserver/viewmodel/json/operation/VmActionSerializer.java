package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParameter;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.mapping.ViewModelMappingSerializer;
import lombok.var;

/**
 * The Josn Serializer Of View Model Action
 *
 * @ClassName: VmActionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class VmActionSerializer<T extends ViewModelAction> extends JsonSerializer<T> {
    protected boolean isFull = true;

    public VmActionSerializer() {
    }

    public VmActionSerializer(boolean full) {
        isFull = full;
    }

    @Override
    public void serialize(T value, JsonGenerator writer, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, CommonModelNames.ID, value.getID());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.Code, value.getCode());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.Name, value.getName());
        if (isFull || (value.getComponentName() != null && !"".equals(value.getComponentName()))) {
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ComponentName, value.getComponentName());
        }
        SerializerUtils.writePropertyValue(writer, CommonModelNames.Type, value.getType().toString());
        if (isFull || value.getIsAutoSave())
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsAutoSave, value.getIsAutoSave());
        if (isFull || value.getCustomizationInfo().isCustomized()) {
            SerializerUtils.writePropertyValue(writer, CefNames.CustomizationInfo, value.getCustomizationInfo());
        }
        if (isFull || (value.getExtendInfo() != null && value.getExtendInfo().size() > 0)) {
            SerializerUtils.writePropertyValue(writer, CefNames.ExtendInfo, value.getExtendInfo());
        }
        writeParameters(writer, value);
        writeReturnValue(writer, value);
        writeMapping(writer, value);
        writeExtendProperties(writer, value);

        //扩展模型属性
        writeExtendOperationProperty(writer, value);

        SerializerUtils.writeEndObject(writer);
    }

    private void writeParameters(JsonGenerator writer, ViewModelAction action) {
        if (isFull || (action.getParameterCollection() != null && action.getParameterCollection().getCount() > 0)) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ParameterCollection);
            //[
            SerializerUtils.WriteStartArray(writer);
            if (action.getParameterCollection().getCount() > 0) {
                for (var item : action.getParameterCollection()) {
                    getParaConvertor().serialize((ViewModelParameter) item, writer, null);
                }
            }
            //]
            SerializerUtils.WriteEndArray(writer);
        }
    }

    private void writeReturnValue(JsonGenerator writer, ViewModelAction action) {
        if (action == null || action.getReturnValue() == null)
            return;

        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ReturnValue);
        VmParameterSerializer convertor = new VmParameterSerializer(isFull);
        convertor.serialize(action.getReturnValue(), writer, null);

    }

    private void writeMapping(JsonGenerator writer, ViewModelAction action) {
        if (action.getMapping() == null) {
            return;
        }
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.Mapping);
        ViewModelMappingSerializer convertor = new ViewModelMappingSerializer(isFull);
        convertor.serialize(action.getMapping(), writer, null);
    }

    private void writeExtendProperties(JsonGenerator writer, ViewModelAction vm) {
        var dic = vm.getExtendProperties();
        if (isFull || (dic != null && dic.size() > 0)) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ExtendProperties);
            SerializerUtils.writeStartObject(writer);
            if (dic != null && dic.size() > 0) {
                for (var item : dic.entrySet()) {
                    SerializerUtils.writePropertyValue(writer, item.getKey(), item.getValue());
                }
            }
            SerializerUtils.writeEndObject(writer);
        }
    }


    protected abstract VmParameterSerializer getParaConvertor();

    /**
     * 序列化子类扩展信息
     *
     * @param writer
     * @param op
     */
    protected abstract void writeExtendOperationProperty(JsonGenerator writer, ViewModelAction op);
}
