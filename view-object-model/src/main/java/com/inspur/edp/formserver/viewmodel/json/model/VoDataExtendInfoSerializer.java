package com.inspur.edp.formserver.viewmodel.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;
import com.inspur.edp.formserver.viewmodel.dataextendinfo.VoDataExtendInfo;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionCollectionSerializer;

/**
 * The  Josn Serializer Of View Model Data Extendion Info
 *
 * @ClassName: VoDataExtendInfoSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoDataExtendInfoSerializer extends JsonSerializer<VoDataExtendInfo> {
    protected boolean isFull = true;

    public VoDataExtendInfoSerializer() {
    }

    public VoDataExtendInfoSerializer(boolean full) {
        isFull = full;
    }

    @Override
    public void serialize(VoDataExtendInfo info, JsonGenerator writer, SerializerProvider serializers) {
        if (info == null) {
            SerializerUtils.writeStartObject(writer);
            SerializerUtils.writeEndObject(writer);
            return;
        }
        //{
        SerializerUtils.writeStartObject(writer);
        if (isFull || info.getDataMappingActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.DataMappingActions);
            writeVmActions(writer, info.getDataMappingActions());
        }
        if (isFull || info.getBeforeQueryActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeQueryActions);
            writeVmActions(writer, info.getBeforeQueryActions());
        }
        if (isFull || info.getQueryActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.QueryActions);
            writeVmActions(writer, info.getQueryActions());
        }
        if (isFull || info.getAfterQueryActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterQueryActions);
            writeVmActions(writer, info.getAfterQueryActions());
        }
        if (isFull || info.getBeforeRetrieveActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeRetrieveActions);
            writeVmActions(writer, info.getBeforeRetrieveActions());
        }
        if (isFull || info.getRetrieveActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.RetrieveActions);
            writeVmActions(writer, info.getRetrieveActions());
        }
        if (isFull || info.getAfterRetrieveActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterRetrieveActions);
            writeVmActions(writer, info.getAfterRetrieveActions());
        }
        if (isFull || info.getBeforeModifyActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeModifyActions);
            writeVmActions(writer, info.getBeforeModifyActions());
        }
        if (isFull || info.getModifyActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ModifyActions);
            writeVmActions(writer, info.getModifyActions());
        }
        if (isFull || info.getAfterModifyActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterModifyActions);
            writeVmActions(writer, info.getAfterModifyActions());
        }
        if (isFull || info.getChangesetMappingActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ChangesetMappingActions);
            writeVmActions(writer, info.getChangesetMappingActions());
        }
        if (isFull || info.getBeforeCreateActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeCreateActions);
            writeVmActions(writer, info.getBeforeCreateActions());
        }
        if (isFull || info.getCreateActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.CreateActions);
            writeVmActions(writer, info.getCreateActions());
        }
        if (isFull || info.getAfterCreateActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterCreateActions);
            writeVmActions(writer, info.getAfterCreateActions());
        }
        if (isFull || !info.getCreateChildActions().isEmpty()) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.CreateChildActions);
            writeVmActions(writer, info.getCreateChildActions());
        }
        if (isFull || info.getBeforeDeleteActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeDeleteActions);
            writeVmActions(writer, info.getBeforeDeleteActions());
        }
        if (isFull || info.getDeleteActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.DeleteActions);
            writeVmActions(writer, info.getDeleteActions());
        }
        if (isFull || info.getAfterDeleteActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterDeleteActions);
            writeVmActions(writer, info.getAfterDeleteActions());
        }
        if (isFull || !info.getDeleteChildActions().isEmpty()) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.DeleteChildActions);
            writeVmActions(writer, info.getDeleteChildActions());
        }
        if (isFull || info.getBeforeSaveActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeSaveActions);
            writeVmActions(writer, info.getBeforeSaveActions());
        }
        if (isFull || !info.getSaveActions().isEmpty()) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.SaveActions);
            writeVmActions(writer, info.getSaveActions());
        }
        if (isFull || info.getDataReversalMappingActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.DataReversalMappingActions);
            writeVmActions(writer, info.getDataReversalMappingActions());
        }
        if (isFull || info.getAfterSaveActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterSaveActions);
            writeVmActions(writer, info.getAfterSaveActions());
        }
        if (isFull || info.getChangesetMappingActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ChangesetReversalMappingActions);
            writeVmActions(writer, info.getChangesetReversalMappingActions());
        }
        if (isFull || info.getBeforeMultiDeleteActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeMultiDeleteActions);
            writeVmActions(writer, info.getBeforeMultiDeleteActions());
        }
        if (isFull || info.getMultiDeleteActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.MultiDeleteActions);
            writeVmActions(writer, info.getMultiDeleteActions());
        }
        if (isFull || info.getAfterMultiDeleteActions() != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.AfterMultiDeleteActions);
            writeVmActions(writer, info.getAfterMultiDeleteActions());
        }
        if (isFull || isVmCollectionNotEmpty(info.getCancelActions())) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.CancelActions);
            writeVmActions(writer, info.getCancelActions());
        }
        //}
        SerializerUtils.writeEndObject(writer);
    }


    private boolean isVmCollectionNotEmpty(VMActionCollection vmActionCollection) {
        if (vmActionCollection == null || vmActionCollection.size() == 0) {
            return false;
        }
        return true;
    }

    private void writeVmActions(JsonGenerator writer, VMActionCollection actions) {
        getVMActionCollectionConvertor().serialize(actions, writer, null);
    }

    private VmActionCollectionSerializer getVMActionCollectionConvertor() {
        return new VmActionCollectionSerializer(isFull);
    }
}
