package com.inspur.edp.formserver.viewmodel.viewmodeldtevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldDataTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldLabelIdEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldObjectTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.RemovingVoFieldEventArgs;
import io.iec.edp.caf.commons.event.IEventListener;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IVoFieldDTEventListener extends IEventListener {

    public ChangingVoFieldDataTypeEventArgs changingVoFieldDataType(ChangingVoFieldDataTypeEventArgs args);

    public ChangingVoFieldLabelIdEventArgs changingVoFieldLabelId(ChangingVoFieldLabelIdEventArgs args);

    public ChangingVoFieldObjectTypeEventArgs changingVoFieldObjectType(
            ChangingVoFieldObjectTypeEventArgs args);

    public RemovingVoFieldEventArgs removingVoField(RemovingVoFieldEventArgs args);
}
