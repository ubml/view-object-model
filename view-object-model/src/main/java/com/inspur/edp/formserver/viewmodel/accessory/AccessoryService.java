package com.inspur.edp.formserver.viewmodel.accessory;

import com.inspur.edp.formserver.viewmodel.GspViewModel;

/**
 * The Approval Log Services,Using By VO Creating Wizard
 *
 * @ClassName: AccessoryService
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface AccessoryService {

    /*
    nodeCode : 父节点编号
    processInstLabelId: 父节点上流程实例字段标签
     */
    void addApprovalComments(GspViewModel model, String nodeCode, String processInstLabelId, boolean includeBacklog);

    /**
     * @param model
     * @param nodeCode
     * @param processInstLabelId
     * @param includeBacklog
     */
    void addApprovalWorkItenLogs(GspViewModel model, String nodeCode, String processInstLabelId, boolean includeBacklog);

}
