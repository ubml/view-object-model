package com.inspur.edp.formserver.viewmodel.extendinfo.entity;

import com.inspur.edp.cef.entity.config.CefConfig;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;

/**
 * 生成代码中configCollection的getConfigs中信息
 *
 * @author haoxiaofei
 */
public class VoConfigCollectionInfo {
    private CefConfig config = new CefConfig();
    private ProcessMode projectType;

    public CefConfig getConfig() {
        return config;
    }

    public void setConfig(CefConfig config) {
        this.config = config;
    }

    public ProcessMode getProjectType() {
        return projectType;
    }

    public void setProjectType(ProcessMode projectType) {
        this.projectType = projectType;
    }
}
