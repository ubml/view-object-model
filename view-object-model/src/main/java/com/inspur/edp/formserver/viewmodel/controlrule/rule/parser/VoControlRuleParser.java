package com.inspur.edp.formserver.viewmodel.controlrule.rule.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlrule.parser.CmControlRuleParser;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.VoControlRule;

public class VoControlRuleParser extends CmControlRuleParser {

    @Override
    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return new VoObjectControlRuleParser();
    }

    @Override
    protected CmControlRule createCmRule() {
        return new VoControlRule();
    }

}
