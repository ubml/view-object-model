package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

/**
 * The Definition Of Changeset Mapping Action
 *
 * @ClassName: ChangesetMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ChangesetMappingAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String id = "5798f884-c222-47f4-8bbe-685c7013dee4";
    public static final String code = "ChangesetMapping";

    public ChangesetMappingAction() {
        setID(id);
        setCode(code);
        setName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.ACTION_CHANGESETMAPPING_NAME));
    }
}