package com.inspur.edp.formserver.viewmodel.increment.extract;

import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;

public class HelpIncrementExtractor extends AbstractIncrementExtractor {
    public HelpIncrementExtractor() {
    }

    public HelpConfigIncrement extractorIncrement(ValueHelpConfig oldHelpConfig, ValueHelpConfig newHelpConfig, CmControlRule rule, CmControlRuleDef def) {
        if (oldHelpConfig == null && newHelpConfig == null) {
            return null;
        } else if (oldHelpConfig == null && newHelpConfig != null) {
            return this.getAddedHelpExtractor().extract(newHelpConfig);
        } else {
            return null;
//            return (CommonEntityIncrement)(oldHelpConfig != null && newHelpConfig == null ? this.getDeletedEntityExtractor().extract(oldHelpConfig) : this.getModifyEntityExtractor().extract(oldHelpConfig, newHelpConfig, rule, def));
        }
    }

    protected AddedHelpExtractor getAddedHelpExtractor() {
        return new AddedHelpExtractor();
    }

//    protected DeletedEntityExtractor getDeletedEntityExtractor() {
//        return new DeletedEntityExtractor();
//    }

//    protected ModifyEntityExtractor getModifyEntityExtractor() {
//        return new ModifyEntityExtractor();
//    }
}
