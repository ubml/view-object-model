package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

/**
 * The Definition Of Delete Action
 *
 * @ClassName: DeleteAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DeleteAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String id = "9a17e935-7366-489d-b110-0ae103e5648e";
    public static final String code = "Delete";

    public DeleteAction() {
        setID(id);
        setCode(code);
        setName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.ACTION_DELETE_NAME));
    }
}