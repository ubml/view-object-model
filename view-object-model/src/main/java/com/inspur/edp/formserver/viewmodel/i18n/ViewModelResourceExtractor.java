package com.inspur.edp.formserver.viewmodel.i18n;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.CommonModelResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.CommonObjectResourceExtractor;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

public class ViewModelResourceExtractor extends CommonModelResourceExtractor {

    public ViewModelResourceExtractor(GspViewModel model, ICefResourceExtractContext context) {
        super(model, context);
    }

    @Override
    protected final String getModelDescriptionName() {
        return VMI8nResourceUtil.getMessage(VoResourceKeyNames.VIEWMODEL);
    }

    @Override
    protected final CommonObjectResourceExtractor getObjectResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo modelPrefixInfo,
            IGspCommonObject obj) {
        return new ViewObjResourceExtractor((GspViewObject) obj, context, modelPrefixInfo);
    }

    protected final void extractExtendProperties(IGspCommonModel model) {

    }
}
