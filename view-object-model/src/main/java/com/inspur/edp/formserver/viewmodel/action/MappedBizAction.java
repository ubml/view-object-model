package com.inspur.edp.formserver.viewmodel.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.formserver.viewmodel.action.mappedbiz.MappedBizActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedBizActionDeserializer;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedBizActionSerializer;

import java.io.Serializable;

/**
 * The Definition Of The Mapped Biz  Action
 *
 * @ClassName: MappedBizAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = MappedBizActionSerializer.class)
@JsonDeserialize(using = MappedBizActionDeserializer.class)
public class MappedBizAction extends ViewModelAction implements Cloneable, Serializable {

    ///#region 属性
    private MappedBizActionParameterCollection mappedBizActionParams;
    /**
     * 类型
     */
    //@Override
    public ViewModelActionType Type = ViewModelActionType.BEAction;
//	public ViewModelActionType Type => ViewModelActionType.BEAction;

    ///#endregion
    public MappedBizAction() {
        mappedBizActionParams = new MappedBizActionParameterCollection();
    }
    ///#region 方法

    /**
     * 克隆
     *
     * @return VM节点映射
     */
    @Override
    public final MappedBizAction clone() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(this), MappedBizAction.class);
        } catch (JsonProcessingException e) {
            throw new ViewModelException(e);
        }
    }

    @Override
    protected IViewModelParameterCollection getParameters() {
        return mappedBizActionParams;
    }
    ///#endregion
    public void setParameters(MappedBizActionParameterCollection params){
        mappedBizActionParams = params;
    }

}