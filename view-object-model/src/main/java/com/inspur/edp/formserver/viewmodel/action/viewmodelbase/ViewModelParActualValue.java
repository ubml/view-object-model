package com.inspur.edp.formserver.viewmodel.action.viewmodelbase;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Definition Of View Model Parameter Action Value.In A View Model Action,The Parameter Value From The Component Can Be Transfer From Outer Invoke Or Fixed
 * When The Value Is Fixed, The Actual Value Is Required.
 *
 * @ClassName: ViewModelParActualValue
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelParActualValue {

    private String value = "";

    @JsonProperty("Value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private boolean hasValue = false;

    @JsonProperty("HasValue")
    @Deprecated
    public boolean getHasValue() {
        if (hasValue && "".equals(value))
            hasValue = false;
        return hasValue;
    }

    @Deprecated
    public void setHasValue(boolean value) {
        hasValue = value;
    }

    private boolean enable = false;

    @JsonProperty("Enable")
    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    private ViewModelParActualValueType valueType = ViewModelParActualValueType.Constant;

    @JsonProperty("ValueType")
    public ViewModelParActualValueType getValueType() {
        return valueType;
    }

    public void setValueType(ViewModelParActualValueType value) {
        this.valueType = value;
    }


}