package com.inspur.edp.formserver.viewmodel.common;

/**
 * The Definition Of View MOdel Parameter Type
 *
 * @ClassName: VMParameterType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum VMParameterType {
    /**
     * 字符型
     */
    String,
    /**
     * 布尔型
     */
    Boolean,
    /**
     * 整数
     */
    Int32,
    /**
     * 浮点数字
     */
    Decimal,
    /**
     * 双浮点
     */
    Double,
    /**
     * 时间
     */
    DateTime,
    //对象类型
    Object,
    /**
     * 自定义
     */
    Custom;

    public int getValue() {
        return this.ordinal();
    }

    public static VMParameterType forValue(int value) {
        return values()[value];
    }
}