package com.inspur.edp.formserver.viewmodel.increment.extract.action;

import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.ModifyVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;

public class ActionIncrementExtractor extends AbstractIncrementExtractor {

    public VoActionIncrement extractorIncrement(ViewModelAction oldAction, ViewModelAction newAction, CmControlRule rule, CmControlRuleDef def) {

        if (oldAction == null && newAction == null) {
            return null;
        } else if (oldAction == null) {
            return this.getAddedActionExtractor().extract(newAction);
        } else if (newAction == null) {
            return null;
//            return (CommonEntityIncrement)(oldHelpConfig != null && newAction == null ? this.getDeletedEntityExtractor().extract(oldHelpConfig) : this.getModifyEntityExtractor().extract(oldHelpConfig, newAction, rule, def));
        } else {
            ModifyVoActionIncrement increment = new ModifyVoActionIncrement();
            increment.setActionId(newAction.getID());
            increment.setAction(newAction);
            return increment;
        }
    }

    protected AddedActionExtractor getAddedActionExtractor() {
        return new AddedActionExtractor();
    }

}
