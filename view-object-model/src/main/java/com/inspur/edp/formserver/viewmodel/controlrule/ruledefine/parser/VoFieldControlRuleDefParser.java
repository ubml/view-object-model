package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser;

import com.inspur.edp.das.commonmodel.controlruledef.entity.CmFieldControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmFieldRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoFieldControlRuleDef;

public class VoFieldControlRuleDefParser extends CmFieldRuleDefParser<VoFieldControlRuleDef> {
    @Override
    protected final CmFieldControlRuleDef createCmFieldRuleDefinition() {
        return new VoFieldControlRuleDef(null);
    }

}
