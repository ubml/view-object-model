package com.inspur.edp.formserver.viewmodel.json.operation;

import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameter;

/**
 * The Json Serializer Of Mapped Component Action Parameter Definition
 *
 * @ClassName: MappedCdpParaSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpParaSerializer extends VmParameterSerializer<MappedCdpActionParameter> {
    public MappedCdpParaSerializer() {
    }

    public MappedCdpParaSerializer(boolean full) {
        super(full);
        isFull = full;
    }
}
