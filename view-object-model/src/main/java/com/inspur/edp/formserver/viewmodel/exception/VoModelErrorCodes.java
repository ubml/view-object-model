/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.exception;

public class VoModelErrorCodes {
    public final static String GSP_VIEWOBJECT_MODEL_1001 = "GSP_VIEWOBJECT_MODEL_1001";
    public final static String GSP_VIEWOBJECT_MODEL_1002 = "GSP_VIEWOBJECT_MODEL_1002";
    public final static String GSP_VIEWOBJECT_MODEL_1003 = "GSP_VIEWOBJECT_MODEL_1003";
    public final static String GSP_VIEWOBJECT_MODEL_1004 = "GSP_VIEWOBJECT_MODEL_1004";
    public final static String GSP_VIEWOBJECT_MODEL_1005 = "GSP_VIEWOBJECT_MODEL_1005";
    public final static String GSP_VIEWOBJECT_MODEL_1006 = "GSP_VIEWOBJECT_MODEL_1006";
    public final static String GSP_VIEWOBJECT_MODEL_1007 = "GSP_VIEWOBJECT_MODEL_1007";
    public final static String GSP_VIEWOBJECT_MODEL_1008 = "GSP_VIEWOBJECT_MODEL_1008";
    public final static String GSP_VIEWOBJECT_MODEL_1009 = "GSP_VIEWOBJECT_MODEL_1009";
    public final static String GSP_VIEWOBJECT_MODEL_1010 = "GSP_VIEWOBJECT_MODEL_1010";
    public final static String GSP_VIEWOBJECT_MODEL_1011 = "GSP_VIEWOBJECT_MODEL_1011";
    public final static String GSP_VIEWOBJECT_MODEL_1012 = "GSP_VIEWOBJECT_MODEL_1012";
    public final static String GSP_VIEWOBJECT_MODEL_1013 = "GSP_VIEWOBJECT_MODEL_1013";
    public final static String GSP_VIEWOBJECT_MODEL_1014 = "GSP_VIEWOBJECT_MODEL_1014";
    public final static String GSP_VIEWOBJECT_MODEL_1015 = "GSP_VIEWOBJECT_MODEL_1015";
    public final static String GSP_VIEWOBJECT_MODEL_1016 = "GSP_VIEWOBJECT_MODEL_1016";
    public final static String GSP_VIEWOBJECT_MODEL_1017 = "GSP_VIEWOBJECT_MODEL_1017";
    public final static String GSP_VIEWOBJECT_MODEL_1018 = "GSP_VIEWOBJECT_MODEL_1018";
    public final static String GSP_VIEWOBJECT_MODEL_1019 = "GSP_VIEWOBJECT_MODEL_1019";
    public final static String GSP_VIEWOBJECT_MODEL_1020 = "GSP_VIEWOBJECT_MODEL_1020";
    public final static String GSP_VIEWOBJECT_MODEL_1021 = "GSP_VIEWOBJECT_MODEL_1021";
    public final static String GSP_VIEWOBJECT_MODEL_1022 = "GSP_VIEWOBJECT_MODEL_1022";
    public final static String GSP_VIEWOBJECT_MODEL_1023 = "GSP_VIEWOBJECT_MODEL_1023";
    public final static String GSP_VIEWOBJECT_MODEL_1024 = "GSP_VIEWOBJECT_MODEL_1024";
    public final static String GSP_VIEWOBJECT_MODEL_1025 = "GSP_VIEWOBJECT_MODEL_1025";
    public final static String GSP_VIEWOBJECT_MODEL_1026 = "GSP_VIEWOBJECT_MODEL_1026";
    public final static String GSP_VIEWOBJECT_MODEL_1027 = "GSP_VIEWOBJECT_MODEL_1027";
    public final static String GSP_VIEWOBJECT_MODEL_1028 = "GSP_VIEWOBJECT_MODEL_1028";
    public final static String GSP_VIEWOBJECT_MODEL_1029 = "GSP_VIEWOBJECT_MODEL_1029";
    public final static String GSP_VIEWOBJECT_MODEL_1030 = "GSP_VIEWOBJECT_MODEL_1030";
    public final static String GSP_VIEWOBJECT_MODEL_1031 = "GSP_VIEWOBJECT_MODEL_1031";
    public final static String GSP_VIEWOBJECT_MODEL_1032 = "GSP_VIEWOBJECT_MODEL_1032";
    public final static String GSP_VIEWOBJECT_MODEL_1033 = "GSP_VIEWOBJECT_MODEL_1033";
    public final static String GSP_VIEWOBJECT_MODEL_1034 = "GSP_VIEWOBJECT_MODEL_1034";
    public final static String GSP_VIEWOBJECT_MODEL_1035 = "GSP_VIEWOBJECT_MODEL_1035";
    public final static String GSP_VIEWOBJECT_MODEL_1036 = "GSP_VIEWOBJECT_MODEL_1036";
    public final static String GSP_VIEWOBJECT_MODEL_1037 = "GSP_VIEWOBJECT_MODEL_1037";
    public final static String GSP_VIEWOBJECT_MODEL_1038 = "GSP_VIEWOBJECT_MODEL_1038";
    public final static String GSP_VIEWOBJECT_MODEL_1039 = "GSP_VIEWOBJECT_MODEL_1039";
    public final static String GSP_VIEWOBJECT_MODEL_1040 = "GSP_VIEWOBJECT_MODEL_1040";
    public final static String GSP_VIEWOBJECT_MODEL_1041 = "GSP_VIEWOBJECT_MODEL_1041";
    public final static String GSP_VIEWOBJECT_MODEL_1042 = "GSP_VIEWOBJECT_MODEL_1042";
    public final static String GSP_VIEWOBJECT_MODEL_1043 = "GSP_VIEWOBJECT_MODEL_1043";
    public final static String GSP_VIEWOBJECT_MODEL_1044 = "GSP_VIEWOBJECT_MODEL_1044";
    public final static String GSP_VIEWOBJECT_MODEL_1045 = "GSP_VIEWOBJECT_MODEL_1045";
    public final static String GSP_VIEWOBJECT_MODEL_1046 = "GSP_VIEWOBJECT_MODEL_1046";
    public final static String GSP_VIEWOBJECT_MODEL_1048 = "GSP_VIEWOBJECT_MODEL_1048";
    public final static String GSP_VIEWOBJECT_MODEL_1049 = "GSP_VIEWOBJECT_MODEL_1049";
    public final static String GSP_VIEWOBJECT_MODEL_1050 = "GSP_VIEWOBJECT_MODEL_1050";
    public final static String GSP_VIEWOBJECT_MODEL_1051 = "GSP_VIEWOBJECT_MODEL_1051";
    public final static String GSP_VIEWOBJECT_MODEL_1052 = "GSP_VIEWOBJECT_MODEL_1052";
    public final static String GSP_VIEWOBJECT_MODEL_1053 = "GSP_VIEWOBJECT_MODEL_1053";
    public final static String GSP_VIEWOBJECT_MODEL_1054 = "GSP_VIEWOBJECT_MODEL_1054";
    public final static String GSP_VIEWOBJECT_MODEL_1055 = "GSP_VIEWOBJECT_MODEL_1055";
    public final static String GSP_VIEWOBJECT_MODEL_1056 = "GSP_VIEWOBJECT_MODEL_1056";
    public final static String GSP_VIEWOBJECT_MODEL_1057 = "GSP_VIEWOBJECT_MODEL_1057";


    public final static String GSP_VIEWOBJECT_EXTENDINFO_1001 = "GSP_VIEWOBJECT_EXTENDINFO_1001";
    public final static String GSP_VIEWOBJECT_EXTENDINFO_1002 = "GSP_VIEWOBJECT_EXTENDINFO_1002";


}
