package com.inspur.edp.formserver.viewmodel.i18n.merger;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.GspAssoRefEleResourceMerger;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;

public class ViewAssoRefEleResourceMerger extends GspAssoRefEleResourceMerger {

    public ViewAssoRefEleResourceMerger(
            GspViewModelElement element,
            ICefResourceMergeContext context) {
        super(element, context);
    }

    @Override
    protected AssoResourceMerger getAssoResourceMerger(
            ICefResourceMergeContext context, GspAssociation asso) {
        return null;
    }
}
