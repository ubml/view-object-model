package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionType;
import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;
/**
 * The Josn Serializer Of View Model Action Collection
 *
 * @ClassName: VmActionCollectionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VmActionCollectionSerializer extends JsonSerializer<VMActionCollection> {

    protected boolean isFull = true;

    public VmActionCollectionSerializer() {
    }

    public VmActionCollectionSerializer(boolean full) {
        isFull = full;
    }

    @Override
    public void serialize(VMActionCollection value, JsonGenerator writer, SerializerProvider serializers) {

        if (value.size() == 0) {
            SerializerUtils.WriteStartArray(writer);
            SerializerUtils.WriteEndArray(writer);
            return;
        }
        SerializerUtils.WriteStartArray(writer);
        for (int i = 0; i < value.size(); i++) {
            ViewModelAction action = value.get(i);
            getActionConvertor(action.getType()).serialize(value.get(i), writer, null);
        }
        SerializerUtils.WriteEndArray(writer);
    }

    private VmActionSerializer getActionConvertor(ViewModelActionType type) {
        switch (type) {
            case BEAction:
                return new MappedBizActionSerializer(isFull);
            case VMAction:
                return new MappedCdpActionSerializer(isFull);
            case Custom:
            default:
                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1037, null, type.toString());
        }
    }
}
