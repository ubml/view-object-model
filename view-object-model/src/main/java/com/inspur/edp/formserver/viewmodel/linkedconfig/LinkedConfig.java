package com.inspur.edp.formserver.viewmodel.linkedconfig;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.formserver.viewmodel.json.model.LinkedConfigDeserializer;
import com.inspur.edp.formserver.viewmodel.json.model.LinkedConfigSerializer;

import java.io.Serializable;

@JsonDeserialize(using = LinkedConfigDeserializer.class)
@JsonSerialize(using = LinkedConfigSerializer.class)
public class LinkedConfig implements Cloneable, Serializable {
    /**
     * 帮助字段映射
     */
    private LinkedFieldsMapping linkedFieldsMapping;

    /**
     * 字段是主实体还是子实体上的标识
     */
    private boolean sourceFieldInChild;

    /**
     * 所选帮助类型
     */
    private LinkedType linkedType = LinkedType.Normal;

    /**
     * 是否拼接帮助联动的条件
     */
    private boolean enableFilterWhenConditionEmpty;

    public LinkedFieldsMapping getLinkedFieldsMapping() {
        return linkedFieldsMapping;
    }

    public void setLinkedFieldsMapping(LinkedFieldsMapping linkedFieldsMapping) {
        this.linkedFieldsMapping = linkedFieldsMapping;
    }

    public boolean getSourceFieldInChild() {
        return sourceFieldInChild;
    }

    public void setSourceFieldInChild(boolean sourceFieldInChild) {
        this.sourceFieldInChild = sourceFieldInChild;
    }

    public LinkedType getLinkedType() {
        return linkedType;
    }

    public void setLinkedType(LinkedType linkedType) {
        this.linkedType = linkedType;
    }

    public boolean getEnableFilterWhenConditionEmpty() {
        return enableFilterWhenConditionEmpty;
    }

    public void setEnableFilterWhenConditionEmpty(boolean enableFilterWhenConditionEmpty) {
        this.enableFilterWhenConditionEmpty = enableFilterWhenConditionEmpty;
    }

}
