package com.inspur.edp.formserver.viewmodel.i18n;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.GspAssoRefEleResourceExtractor;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;

class ViewAssoRefEleResourceExtractor extends GspAssoRefEleResourceExtractor {

    public ViewAssoRefEleResourceExtractor(GspViewModelElement field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(field, context, parentResourceInfo);
    }

    @Override
    protected final AssoResourceExtractor getAssoResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo fieldPrefixInfo,
            GspAssociation asso) {
        return new ViewAssoResourceExtractor(asso, getContext(), fieldPrefixInfo);
    }
}
