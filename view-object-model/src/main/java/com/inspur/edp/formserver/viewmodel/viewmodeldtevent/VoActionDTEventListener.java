package com.inspur.edp.formserver.viewmodel.viewmodeldtevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs.*;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoActionDTEventListener implements IVoActionDTEventListener {

    @Override
    public ChangingVoActionCodeEventArgs changingVoActionCode(ChangingVoActionCodeEventArgs args) {
        return null;
    }

    @Override
    public ChangingVoActionParamsEventArgs changingVoActionParams(
            ChangingVoActionParamsEventArgs args) {
        return null;
    }

    @Override
    public ChangingVoActionReturnEventArgs changingVoActionReturn(
            ChangingVoActionReturnEventArgs args) {
        return null;
    }

    @Override
    public DeletingVoActionEventArgs deletingVoAction(DeletingVoActionEventArgs args) {
        return null;
    }

    @Override
    public ChangingVoActionCollectTypeEventArgs changingVoActionCollectType(
            ChangingVoActionCollectTypeEventArgs args) {
        return null;
    }
}
