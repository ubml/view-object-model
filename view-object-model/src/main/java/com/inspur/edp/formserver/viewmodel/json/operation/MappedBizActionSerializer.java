package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.formserver.viewmodel.action.MappedBizAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;

/**
 * The Json Serializer Of The View Model Action Of  Mapped Be Action
 *
 * @ClassName: MappedBizActionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionSerializer extends VmActionSerializer<MappedBizAction> {
    public MappedBizActionSerializer() {
    }

    public MappedBizActionSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected VmParameterSerializer getParaConvertor() {
        return new MappedBizActionParaSerializer(isFull);
    }

    @Override
    protected void writeExtendOperationProperty(JsonGenerator writer, ViewModelAction op) {

    }
}
