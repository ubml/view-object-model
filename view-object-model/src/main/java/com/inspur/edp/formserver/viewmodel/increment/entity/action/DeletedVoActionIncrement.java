package com.inspur.edp.formserver.viewmodel.increment.entity.action;

import com.inspur.edp.cef.designtime.api.increment.IncrementType;

public class DeletedVoActionIncrement extends VoActionIncrement {

    public DeletedVoActionIncrement(String actionId) {
        setActionId(actionId);
    }

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Deleted;
    }
}
