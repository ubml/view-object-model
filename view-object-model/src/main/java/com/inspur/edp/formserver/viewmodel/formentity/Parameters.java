package com.inspur.edp.formserver.viewmodel.formentity;

import lombok.Getter;

@Getter
public class Parameters {
    private String code;
    private String name;
    private String valueType;
    private String value;

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
