package com.inspur.edp.formserver.viewmodel.increment.entity.action;

import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;

public class AddedVoActionIncrement extends VoActionIncrement {

    private ViewModelAction action;

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Added;
    }

    public ViewModelAction getAction() {
        return action;
    }

    public void setAction(ViewModelAction action) {
        this.action = action;
    }
}
