package com.inspur.edp.formserver.viewmodel.exception;

public class BffException extends RuntimeException {

    public BffException() {
        super();
    }

    public BffException(String message) {
        super(message);
    }

    public BffException(Exception cause) {
        super(cause);
    }

    public BffException(String message, Exception cause) {
        super(cause);
    }
}
