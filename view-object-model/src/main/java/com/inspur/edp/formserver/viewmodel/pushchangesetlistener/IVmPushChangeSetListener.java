package com.inspur.edp.formserver.viewmodel.pushchangesetlistener;

import com.inspur.edp.formserver.viewmodel.pushchangesetargs.VmPushChangeSetArgs;
import io.iec.edp.caf.commons.event.IEventListener;

public interface IVmPushChangeSetListener extends IEventListener {
    void vmPushChangeSet(VmPushChangeSetArgs args);
}
