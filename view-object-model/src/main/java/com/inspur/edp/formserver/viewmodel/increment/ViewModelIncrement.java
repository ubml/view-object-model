package com.inspur.edp.formserver.viewmodel.increment;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;
import com.inspur.edp.formserver.viewmodel.json.increment.ViewModelIncrementDeserializer;
import com.inspur.edp.formserver.viewmodel.json.increment.ViewModelIncrementSerializer;

import java.util.HashMap;

@JsonSerialize(using = ViewModelIncrementSerializer.class)
@JsonDeserialize(using = ViewModelIncrementDeserializer.class)
public class ViewModelIncrement extends CommonModelIncrement {

    private HashMap<String, HelpConfigIncrement> valueHelpConfigs = new HashMap<>();

    public HashMap<String, HelpConfigIncrement> getValueHelpConfigs() {
        return this.valueHelpConfigs;
    }

    private HashMap<String, VoActionIncrement> actions = new HashMap<>();

    public HashMap<String, VoActionIncrement> getActions() {
        return actions;
    }
}
