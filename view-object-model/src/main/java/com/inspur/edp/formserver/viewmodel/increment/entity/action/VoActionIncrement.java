package com.inspur.edp.formserver.viewmodel.increment.entity.action;

import com.inspur.edp.cef.designtime.api.increment.AbstractIncrement;

public abstract class VoActionIncrement extends AbstractIncrement {

    private String actionId;

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }
}
