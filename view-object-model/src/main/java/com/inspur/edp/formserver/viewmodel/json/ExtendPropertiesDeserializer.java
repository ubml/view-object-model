package com.inspur.edp.formserver.viewmodel.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.util.HashMap;

/**
 * The Josn Deserializer Of View Model Extend Properties
 *
 * @ClassName: ExtendPropertiesDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ExtendPropertiesDeserializer extends JsonDeserializer<HashMap<String, String>> {
    @Override
    public HashMap<String, String> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        HashMap<String, String> map = new HashMap<>();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String key = SerializerUtils.readPropertyName(jsonParser);
            String value = SerializerUtils.readPropertyValue_String(jsonParser);
            map.put(key, value);
        }
        SerializerUtils.readEndObject(jsonParser);
        return map;
    }
}
