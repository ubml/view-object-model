package com.inspur.edp.formserver.viewmodel.json.mapping;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectSourceType;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

/**
 * The  Josn Deserializer Of View Model Object Mapping
 *
 * @ClassName: GspVoObjectMappingDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoObjectMappingDeserializer extends ViewModelMappingDeserializer {
    @Override
    protected GspVoObjectMapping createVmMapping() {
        return new GspVoObjectMapping();
    }

    @Override
    protected boolean readExtendMappingProperty(JsonParser reader, ViewModelMapping mapping, String propertyName) {
        boolean hasProperty = true;
        GspVoObjectMapping voMapping = (GspVoObjectMapping) mapping;
        switch (propertyName) {
            case ViewModelJsonConst.SourceType:
                voMapping.setSourceType(SerializerUtils.readPropertyValue_Enum(reader, GspVoObjectSourceType.class, GspVoObjectSourceType.values(), GspVoObjectSourceType.VoObject));
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }
}
