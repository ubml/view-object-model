package com.inspur.edp.formserver.viewmodel.increment.entity;

import com.inspur.edp.cef.designtime.api.increment.AbstractIncrement;

public abstract class HelpConfigIncrement extends AbstractIncrement {

    private String elementId;

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }
}
