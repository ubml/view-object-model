package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmEntityControlRuleDefSerializer;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoObjectControlRuleDef;

public class VoObjectControlRuleDefSerializer<T extends VoObjectControlRuleDef> extends CmEntityControlRuleDefSerializer<T> {

    @Override
    protected final void writeCmEntityRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeCmEntityRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
    }

}
