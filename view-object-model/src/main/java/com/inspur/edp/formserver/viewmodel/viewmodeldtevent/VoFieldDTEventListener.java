package com.inspur.edp.formserver.viewmodel.viewmodeldtevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldDataTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldLabelIdEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.ChangingVoFieldObjectTypeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs.RemovingVoFieldEventArgs;
/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoFieldDTEventListener implements IVoFieldDTEventListener {


    @Override
    public ChangingVoFieldDataTypeEventArgs changingVoFieldDataType(
            ChangingVoFieldDataTypeEventArgs args) {
        return null;
    }

    @Override
    public ChangingVoFieldLabelIdEventArgs changingVoFieldLabelId(
            ChangingVoFieldLabelIdEventArgs args) {
        return null;
    }

    @Override
    public ChangingVoFieldObjectTypeEventArgs changingVoFieldObjectType(
            ChangingVoFieldObjectTypeEventArgs args) {
        return null;
    }

    @Override
    public RemovingVoFieldEventArgs removingVoField(RemovingVoFieldEventArgs args) {
        return null;
    }
}
