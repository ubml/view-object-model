/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.i18n.names;

import lombok.Getter;

public enum VoResourceKeyNames {
    /**
     * 视图对象
     */
    VIEWMODEL,
    /**
     * 名称
     */
    RULE_FIELD_NAME,
    /**
     * 字段名称
     */
    RULE_FIELD_NAME_DESCRIPTION,
    /**
     * 长度
     */
    RULE_FIELD_LENGTH,
    /**
     * 字段长度
     */
    RULE_FIELD_LENGTH_DESCRIPTION,
    /**
     * 精度
     */
    RULE_FIELD_PRECISION,
    /**
     * 字段精度
     */
    RULE_FIELD_PRECISION_DESCRIPTION,
    /**
     * 默认值
     */
    RULE_FIELD_DEFAULTVALUE,
    /**
     * 字段默认值
     */
    RULE_FIELD_DEFAULTVALUE_DESCRIPTION,
    /**
     * 是否多语
     */
    RULE_FIELD_MULTILANGUAGE,
    /**
     * 字段多语设置
     */
    RULE_FIELD_MULTILANGUAGE_DESCRIPTION,
    /**
     * 是否只读
     */
    RULE_FIELD_READONLY,
    /**
     * 字段只读设置
     */
    RULE_FIELD_READONLY_DESCRIPTION,
    /**
     * 是否必填
     */
    RULE_FIELD_REQUIRED,
    /**
     * 名称
     */
    RULE_VIEWMODEL_NAME,
    /**
     * 实体名称
     */
    RULE_VIEWMODEL_NAME_DESCRIPTION,
    /**
     * 添加自定义动作
     */
    RULE_VIEWMODEL_ADD_CUSTOM_ACTION,
    /**
     * 添加自定义动作
     */
    RULE_VIEWMODEL_ADD_CUSTOM_ACTION_DESCRIPTION,
    /**
     * 修改自定义动作
     */
    RULE_VIEWMODEL_NODIFY_CUSTOM_ACTION,
    /**
     * 修改自定义动作
     */
    RULE_VIEWMODEL_NODIFY_CUSTOM_ACTION_DESCRIPTION,
    /**
     * 添加自定义变量
     */
    RULE_VIEWMODEL_ADD_VARIABLE,
    /**
     * 添加自定义变量
     */
    RULE_VIEWMODEL_ADD_VARIABLE_DESCRIPTION,
    /**
     * 修改自定义变量
     */
    RULE_VIEWMODEL_MODIFY_VARIABLE,
    /**
     * 修改自定义变量
     */
    RULE_VIEWMODEL_MODIFY_VARIABLE_DESCRIPTION,
    /**
     * 名称
     */
    RULE_OBJECT_NAME,
    /**
     * 节点名称
     */
    RULE_OBJECT_NAME_DESCRIPTION,
    /**
     * 添加子节点
     */
    RULE_OBJECT_ADD_CHILD_ENTITY,
    /**
     * 添加子节点
     */
    RULE_OBJECT_ADD_CHILD_ENTITY_DESCRIPTION,
    /**
     * 修改子节点信息
     */
    RULE_OBJECT_MODIFY_CHILD_ENTITY,
    /**
     * 修改子节点信息
     */
    RULE_OBJECT_MODIFY_CHILD_ENTITY_DESCRIPTION,
    /**
     * 添加字段
     */
    RULE_OBJECT_ADD_FIELD,
    /**
     * 添加字段
     */
    RULE_OBJECT_ADD_FIELD_DESCRIPTION,
    /**
     * 修改字段信息
     */
    RULE_OBJECT_MODIFY_FIELD,
    /**
     * 修改字段信息
     */
    RULE_OBJECT_MODIFY_FIELD_DESCRIPTION,


    /**
     * 字段必填设置
     */
    RULE_FIELD_REQUIRED_DESCRIPTION,
    /**
     * 流程实例标识符
     */
    COL_PROCESSINSTANCEID_NAME,
    /**
     * 父流程实例标识符
     */
    COL_ROOTPROCESSINSTANCEID_NAME,
    /**
     * 活动实例标识符
     */
    COL_ACTIVITYINSTANCEID_NAME,
    /**
     * 活动实例名称
     */
    COL_ACTIVITYNAME_NAME,
    /**
     * 工作项标识符
     */
    COL_WORKITEMID_NAME,
    /**
     * 操作人标识符
     */
    COL_OPERATORID_NAME,
    /**
     * 操作人名称
     */
    COL_OPERATORNAME_NAME,
    /**
     * 日志类型
     */
    COL_TYPE_NAME,
    /**
     * 审批动作编号
     */
    COL_ACTIONCODE_NAME,
    /**
     * 审批动作名称
     */
    COL_ACTIONNAME_NAME,
    /**
     * 审批信息
     */
    COL_MESSAGE_NAME,
    /**
     * 审批时间
     */
    COL_TIME_NAME,
    /**
     * 附件标识符
     */
    COL_ATTACHMENT_NAME,
    /**
     * 签名图片
     */
    COL_SIGNATUREIMAGE_NAME,
    /**
     * 审批日志
     */
    APPROVAL_LOG,
    /**
     * 工程[{0}]下的视图对象[{1}]中自定义动作[{2}]依赖了该动作
     */
    VO_ACTION_CONSISTENCY_CHECK_PREFIX,

    /**
     * 工程[{0}]中的视图对象[{1}]中对象[{2}]依赖了当前业务实体对象
     */
    VO_OBJECT_CONSISTENCY_CHECK_PREFIX,
    /**
     * 工程[{0}]中的视图对象[{1}]中对象[{2}]上字段[{3}]依赖了当前业务实体对象
     */
    VO_ELEMENT_RELY_OBJECT_CONSISTENCY_CHECK_PREFIX,
    /**
     * 工程[{0}]中的视图对象[{1}]中对象[{2}]上字段[{3}]依赖了当前业务实体字段
     */
    VO_ELEMENT_RELY_ELEMENT_CONSISTENCY_CHECK_PREFIX,


    /**
     * VO帮助前支持过滤筛选构件
     */
    VOBEFOREHELOFILTER_COMPONENT_NAME,
    /**
     * 视图对象[{0}]的编号不能为空
     */
    VALIDATE_BASICINFO_CODE_ISNULL,
    /**
     * 视图对象[{0}]的 [编号] 属性必须以英文字母或下划线开头, 并且只能由英文字母、数字、下划线(_)组成!
     */
    VALIDATE_BASICINFO_CODE_FORMATE,
    /**
     * 视图对象[{0}]的名称不能为空!
     */
    VALIDATE_BASICINFO_NAME_ISNULL,
    /**
     * 模型[{0}]的变量集合中字段[{1}]和字段[{2}]的[编号]属性不允许重复!
     */
    VALIDATE_VARIABLES_CODE_DUPLICATE,
    /**
     * 对象[{0}]的编号不能为空!
     */
    VALIDATE_OBJECT_CODEISNULL,
    /**
     * 对象[{0}]的编号必须以英文字母或下划线开头, 并且只能由英文字母、数字、下划线(_)组成!
     */
    VALIDATE_OBJECT_CODEFORMATION,
    /**
     * 对象[{0}]的名称不能为空!
     */
    VALIDATE_OBJECT_NAMEISNULL,
    /**
     * 对象[{0}]和对象[{1}]的编号相同，请修改。
     */
    VALIDATE_OBJECT_CODE_DUPLICATE,
    /**
     * 对象[{0}]中字段[{1}]和字段[{2}]的[编号]属性不允许重复!
     */
    VALIDATE_ELEMENT_CODE_DUPLICATE,
    /**
     * 对象[{0}]中字段[{1}]和字段[{2}]的[名称]属性不允许重复!
     */
    VALIDATE_ELEMENT_NAME_DUPLICATE,
    /**
     * 对象[{0}]中字段[{1}]和字段[{2}]的[标签]属性不允许重复!
     */
    VALIDATE_ELEMENT_LABEL_DUPLICATE,

    /**
     * 视图动作[{0}]引用构件元数据不存在
     */
    VALIDATE_COMPONENT_METADATA_NOT_EXIST,
    /**
     * 动作[{0}]与构件[{1}]参数数量不一致:动作参数[{2}]个,构件参数[{3}]个
     */
    VALIDATE_COMPONENT_PARAMETER_NUMBER_NOT_EQUAL,
    /**
     * 动作[{0}]参数[{1}]与构件[{2}]对应参数[{3}]的编号不一致
     */
    VALIDATE_COMPONENT_PARAMETER_CODE_NOT_EQUAL,
    /**
     * 动作[{0}]参数[{1}]与构件[{2}]参数[类型]不一致，分别为[{3}],[{4}]
     */
    VALIDATE_COMPONENT_PARAMETER_TYPE_NOT_EQUAL,
    /**
     * 动作[{0}]的返回类型与对应构件[{1}]不一致
     */
    VALIDATE_ACTION_RETURNVALUE_TYPE_NOT_EQUAL,
    /**
     * 动作[{0}]的返回值集合类型与对应构件[{1}]不一致
     */
    VALIDATE_ACTION_RETURNVALUE_COLLECTION_TYPE_NOT_EQUAL,
    /**
     * J版元数据工程，{0}[{1}]中的返回值的 [类型] 为自定义类型，请完善其Java类名。
     */
    VALIDATE_ACTION_RETURNVALUE_CLASS_ISNULL,

    /**
     * 内置变更集Mapping操作
     */
    ACTION_CHANGESETMAPPING_NAME,
    /**
     * 内置变更集反向Mapping操作
     */
    ACTION_CHANGESETREVERSALMAPPING_NAME,
    /**
     * 内置新增数据操作
     */
    ACTION_CREATE_NAME,
    /**
     * 内置数据Mapping操作
     */
    ACTION_DATAMAPPING_NAME,
    /**
     * 内置数据反向Mapping操作
     */
    ACTION_DATAREVERSALMAPPING_NAME,
    /**
     * 内置删除操作
     */
    ACTION_DELETE_NAME,
    /**
     * 内置修改操作
     */
    ACTION_MODIFY_NAME,
    /**
     * 内置批量删除操作
     */
    ACTION_MULTIDELETE_NAME,
    /**
     * 内置查询操作
     */
    ACTION_QUERY_NAME,
    /**
     * 内置检索操作
     */
    ACTION_RETRIEVE_NAME,
    /**
     * 表单流程配置
     */
    FROM_PROCESS_CONFIG,
    /**
     * 编号为[{0}],名称为[{1}]
     */
    VIEWMODEL_MESSAGE_PREFIX_0,
    /**
     * 编号为[{0}],名称为[{1}],命名空间为[{2}]的视图对象元数据,
     */
    VIEWMODEL_MESSAGE_PREFIX_1,
    /**
     * 编号为[{0}],名称为[{1}],命名空间为[{2}]
     */
    VIEWMODEL_MESSAGE_PREFIX_2,
    /**
     * [ERROR] 当前表单未获取到对应的业务对象，无法推送表单格式至流程设计
     */
    ERROR_PUBLISH_FORM_WITHOUT_BO_TO_PROCESS_DESIGN,
    /**
     * 扩展操作[数据Mapping]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_DATAMAPPING_ACTION,
    /**
     * 扩展操作[查询数据前]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_BEFOREQUERY_ACTION,
    /**
     * 扩展操作[查询数据]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_QUERY_ACTION,
    /**
     * 扩展操作[查询数据后]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_AFTERQUERY_ACTION,
    /**
     * 扩展操作[检索数据前]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_BEFORERETRIEVE_ACTION,
    /**
     * 扩展操作[检索数据]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_RETRIEVE_ACTION,
    /**
     * 扩展操作[检索数据后]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_AFTERRETRIEVE_ACTION,
    /**
     * 扩展操作[修改数据前]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_BEFOREMODIFY_ACTION,
    /**
     * 扩展操作[修改数据]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_MODIFY_ACTION,
    /**
     * 扩展操作[修改数据后]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_AFTERMODIFY_ACTION,
    /**
     * 扩展操作[变更集MAPPING]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_CHANGESETMAPPING_ACTION,
    /**
     * 扩展操作[新增数据前]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_BEFORECREATE_ACTION,
    /**
     * 扩展操作[新增数据]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_CREATE_ACTION,
    /**
     * 扩展操作[新增数据后]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_AFTERCREATE_ACTION,
    /**
     * 扩展操作[删除数据前]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_BEFOREDELETE_ACTION,
    /**
     * 扩展操作[删除数据]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_DELETE_ACTION,
    /**
     * 扩展操作[删除数据后]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_AFTERDELETE_ACTION,
    /**
     * 扩展操作[保存数据前]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_BEFORESAVE_ACTION,
    /**
     * 扩展操作[数据反向Mapping]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_DATAREVERSALMAPPING_ACTION,
    /**
     * 扩展操作[保存数据后]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_AFTERSAVE_ACTION,
    /**
     * 扩展操作[变更集反向Mapping]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_CHANGESETREVERSALMAPPING_ACTION,
    /**
     * 扩展操作[批量删除数据前]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_BEFOREMULTIDELETE_ACTION,
    /**
     * 扩展操作[批量删除数据]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_MULTIDELETE_ACTION,
    /**
     * 扩展操作[批量删除数据后]中的
     */
    VALIDATE_DATAEXTEND_PREFIX_AFTERMULTIDELETE_ACTION,
    /**
     * [帮助集合]中存在多个的[ElementId]属性重复为[{0}]的帮助信息
     */
    VALIDATE_HELP_CONFIG_PREFIX_ELEMENT_DUPLICATE,
    /**
     * [帮助集合]的帮助信息[{0}]的[ElementId]属性不允许为空，请检查!
     */
    VALIDATE_HELP_CONFIG_PREFIX_ELEMENTID_IS_NULL,
    /**
     * [帮助集合]的帮助信息[{0}]的[HelperId] 属性不允许为空，请检查!
     */
    VALIDATE_HELP_CONFIG_PREFIX_HELPID_IS_NULL,
    /**
     * [帮助集合]的帮助[{0}]中的帮助前事件操作
     */
    VALIDATE_HELP_ACTION_PREFIX,
    /**
     * 视图对象操作
     */
    VALIDATE_VM_ACTION_PREFIX,
    /**
     * [变量集合]的联动计算[{0}]的[编号]属性不允许为空!
     */
    VALIDATE_COMMON_OP_BASIC_CODE_IS_NULL,
    /**
     * [变量集合]的联动计算[{0}]的[编号]属性必须以英文字母或下划线开头, 并且只能由英文字母、数字、下划线(_)组成!
     */
    VALIDATE_COMMON_OP_BASIC_CODE_FORMATE,
    /**
     * [变量集合]的联动计算[{0}]的[名称]属性不允许为空!
     */
    VALIDATE_COMMON_OP_BASIC_NAME_IS_NULL,
    /**
     * [变量集合]的联动计算存在编号同为[{0}]的多个操作[{1}],[{2}]。
     */
    VALIDATE_COMMON_OP_CODE_DUPLICATE,

    /**
     * {0}[{1}]的[编号]属性不允许为空!
     */
    VALIDATE_OPERATION_BASIC_CODE_IS_NULL,
    /**
     * {0}[{1}]的 [编号] 属性必须以英文字母或下划线开头, 并且只能由英文字母、数字、下划线(_)组成!
     */
    VALIDATE_OPERATION_BASIC_CODE_FORMATE,
    /**
     * {0}[{1}]的[名称]属性不允许为空!
     */
    VALIDATE_OPERATION_BASIC_NAME_IS_NULL,
    /**
     * {0}[{1}],[{2}]存在相同的编号[{3}]。
     */
    VALIDATE_OPERATION_CODE_DUPLICATE,
    /**
     * {0}[{1}]参数[{2}][编号]不允许为空!
     */
    VALIDATE_ACTION_PARAM_CODE_IS_NULL,
    /**
     * {0}[{1}]中的参数[{2}]的[编号] 属性必须以英文字母或下划线开头, 并且只能由英文字母、数字、下划线(_)组成!
     */
    VALIDATE_ACTION_PARAM_CODE_FORMATE,
    /**
     * {0}[{1}]中的参数[{2}][名称]不允许为空!
     */
    VALIDATE_ACTION_PARAM_NAME_IS_NULL,
    /**
     * 元数据工程，{0}[{1}]中的参数[{2}][参数类型]为自定义类型，请完善其Java类名。
     */
    VALIDATE_ACTION_PARAM_CLASS_IS_NULL,
    /**
     * {0}[{1}]中的存在多个[编号]相同为[{2}]的参数[{3}],[{4}]
     */
    VALIDATE_ACTION_PARAM_CODE_DUPLICATE,
    /**
     * {0}的[默认值] 与[数据类型] 不匹配，请检查是否为[浮点数字]，请检查[精度]是否匹配!
     */
    VALIDATE_ELEMENT_DEFAULT_DECIMAL_NOT_FIT,
    /**
     * {0}的[默认值] 与[数据类型] 不匹配!
     */
    VALIDATE_ELEMENT_DEFAULT_DATATYPE_NOT_FIT,
    /**
     * {0} 的 [编号] 属性不允许为空!
     */
    VALIDATE_ELEMENT_BASIC_CODE_IS_NULL,
    /**
     * {0}的 [编号] 属性必须以英文字母或下划线开头, 并且只能由英文字母、数字、下划线(_)组成!
     */
    VALIDATE_ELEMENT_BASIC_CODE_FORMAT,
    /**
     * {0} 的 [名称] 属性不允许为空!
     */
    VALIDATE_ELEMENT_BASIC_NAME_IS_NULL,
    /**
     * {0} 的 [标签] 属性不允许为空!
     */
    VALIDATE_ELEMENT_BASIC_LABEL_IS_NULL,
    /**
     * {0} 的 [标签] 属性 \n 必须以英文字母或下划线开头, 并且只能由英文字母、数字、下划线(_)、连线(-)和点(.)组成!
     */
    VALIDATE_ELEMENT_BASIC_LABEL_FORMAT,
    /**
     * {0}的 [对象类型] 属性为枚举, [枚举] 属性不允许为空!
     */
    VALIDATE_ELEMENT_BASIC_ENUM_IS_EMPTY,
    /**
     * {0}的 [对象类型] 属性为枚举, [默认值] 应为枚举编号。
     */
    VALIDATE_ELEMENT_BASIC_DEFAULTVALUE_FIT_ENUM,
    /**
     * {0}的 [对象类型] 属性为枚举, [是否必填] 属性应为否。
     */
    VALIDATE_ELEMENT_BASIC_ENUM_REQUIRED_NOT_NO,
    /**
     * {0}的[数据类型] 为[浮点数字]，其 [长度] 不可为0.
     */
    VALIDATE_ELEMENT_BASIC_DECIMAL_LENGTH_IS_ZERO,
    /**
     * {0}的[数据类型] 为[浮点数字]，其 [精度] 不可为0.
     */
    VALIDATE_ELEMENT_BASIC_DECIMAL_PRECISION_IS_ZERO,
    /**
     * {0}的[数据类型] 为[文本]，其 [长度] 不可为0.
     */
    VALIDATE_ELEMENT_BASIC_STRING_LENGTH_IS_ZERO,
    /**
     * 对象[{0}]的字段[{1}]
     */
    VALIDATE_ELEMENT_PREFIX,
    /**
     * 模型[{0}]变量集合的字段[{1}]
     */
    VALIDATE_VARIABLE_PREFIX,
    ;

    @Getter
    private final String errorCode;
    private final static String PREFIX = "GSP_VIEWOBJECT_MSGTEMP_";
    VoResourceKeyNames() {
        this.errorCode = PREFIX + this.name();
    }
}
