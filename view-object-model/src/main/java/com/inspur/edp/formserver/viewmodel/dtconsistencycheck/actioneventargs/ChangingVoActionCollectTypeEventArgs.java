package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs;


public class ChangingVoActionCollectTypeEventArgs extends AbstractVoActionEventArgs {

    protected String originalCollectionType;
    protected String newCollectionType;

    public ChangingVoActionCollectTypeEventArgs() {
    }

    public String getOriginalCollectionType() {
        return originalCollectionType;
    }

    public void setOriginalCollectionType(String originalCollectionType) {
        this.originalCollectionType = originalCollectionType;
    }

    public String getNewCollectionType() {
        return newCollectionType;
    }

    public void setNewCollectionType(String newCollectionType) {
        this.newCollectionType = newCollectionType;
    }
}
