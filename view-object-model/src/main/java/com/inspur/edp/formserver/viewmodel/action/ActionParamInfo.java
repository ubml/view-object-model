package com.inspur.edp.formserver.viewmodel.action;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

public class ActionParamInfo {
    private GspMetadata metadata;

    public GspMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(GspMetadata metadata) {
        this.metadata = metadata;
    }


}
