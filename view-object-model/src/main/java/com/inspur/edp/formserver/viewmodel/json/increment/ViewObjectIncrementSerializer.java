package com.inspur.edp.formserver.viewmodel.json.increment;

import com.inspur.edp.das.commonmodel.json.increment.CommonElementIncrementSerializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementSerializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;
import com.inspur.edp.formserver.viewmodel.json.object.ViewObjectSerializer;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewObjectIncrementSerializer extends CommonObjectIncrementSerializer {
    @Override
    protected CmObjectSerializer getCommonObjectSerializer() {
        return new ViewObjectSerializer();
    }

    @Override
    protected CommonElementIncrementSerializer getCmElementIncrementSerizlizer() {
        return new ViewElementIncrementSerializer();
    }

    @Override
    protected CommonObjectIncrementSerializer getCmObjectIncrementSerializer() {
        return new ViewObjectIncrementSerializer();
    }
}
