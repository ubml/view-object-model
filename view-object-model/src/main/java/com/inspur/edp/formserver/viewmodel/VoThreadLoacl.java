package com.inspur.edp.formserver.viewmodel;

public class VoThreadLoacl {
    public static final ThreadLocal voThreadLocal = new ThreadLocal();

    public static void set(Context user) {
        voThreadLocal.set(user);
    }

    public static void unset() {
        voThreadLocal.remove();
    }

    public static Context get() {
        return (Context) voThreadLocal.get();
    }
}
