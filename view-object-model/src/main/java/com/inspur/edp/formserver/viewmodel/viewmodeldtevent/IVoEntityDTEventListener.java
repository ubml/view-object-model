package com.inspur.edp.formserver.viewmodel.viewmodeldtevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs.ChangingViewObjectCodeEventArgs;
import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs.RemovingViewObjectEventArgs;
import io.iec.edp.caf.commons.event.IEventListener;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IVoEntityDTEventListener extends IEventListener {

    public ChangingViewObjectCodeEventArgs changingViewObjectCode(
            ChangingViewObjectCodeEventArgs args);

    public RemovingViewObjectEventArgs removingViewObject(RemovingViewObjectEventArgs args);
}
