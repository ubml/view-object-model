package com.inspur.edp.formserver.viewmodel.formentity;

import lombok.Getter;

import java.util.List;

@Getter
public class ChildData {
    private String id;
    private String name;
    private String code;
    private List<String> configurableAttrs;
    private List<ChildData> childFields;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setConfigurableAttrs(List<String> configurableAttrs) {
        this.configurableAttrs = configurableAttrs;
    }

    public void setChildFields(List<ChildData> childFields) {
        this.childFields = childFields;
    }
}
