package com.inspur.edp.formserver.viewmodel.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedConfig;

import java.io.IOException;

public class LinkedConfigSerializer extends JsonSerializer<LinkedConfig> {
    protected boolean isFull;

    public LinkedConfigSerializer() {

    }

    public LinkedConfigSerializer(boolean isFull) {
        this.isFull = isFull;
    }

    @Override
    public void serialize(LinkedConfig config, JsonGenerator writer, SerializerProvider serializers) throws IOException {
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.LinkedFieldsMapping, config.getLinkedFieldsMapping());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.SourceFieldInChild, config.getSourceFieldInChild());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.EnableFilterWhenConditionEmpty, config.getEnableFilterWhenConditionEmpty());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.LinkedType, config.getLinkedType());
        SerializerUtils.writeEndObject(writer);
    }
}
