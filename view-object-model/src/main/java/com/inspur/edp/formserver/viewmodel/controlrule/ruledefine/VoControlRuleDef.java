package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser.VoControlRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.serializer.VoControlRuleDefSerializer;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

@JsonSerialize(using = VoControlRuleDefSerializer.class)
@JsonDeserialize(using = VoControlRuleDefParser.class)
public class VoControlRuleDef extends CmControlRuleDef {
    public VoControlRuleDef() {
        super(null, VoControlRuleDefNames.VoControlRuleObjectType);
        init();
    }

    private void init() {

        ControlRuleDefItem nameRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Name);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_NAME));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_NAME_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setNameControlRule(nameRule);

//        setUsingTimeStampControlRule();

        ControlRuleDefItem addActionRule = new ControlRuleDefItem() {
            {
                this.setRuleName(VoControlRuleDefNames.AddCustomAction);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_ADD_CUSTOM_ACTION));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_ADD_CUSTOM_ACTION_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setAddCustomActonControlRule(addActionRule);

        ControlRuleDefItem modifyActionRule = new ControlRuleDefItem() {
            {
                this.setRuleName(VoControlRuleDefNames.ModifyCustomActions);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_NODIFY_CUSTOM_ACTION));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_NODIFY_CUSTOM_ACTION_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyCustomActonControlRule(modifyActionRule);

        ControlRuleDefItem addVarRule = new ControlRuleDefItem() {
            {
                this.setRuleName(VoControlRuleDefNames.AddVariableDtm);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_ADD_VARIABLE));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_ADD_VARIABLE_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setAddVariableDtmControlRule(addVarRule);

        ControlRuleDefItem modifyVarRule = new ControlRuleDefItem() {
            {
                this.setRuleName(VoControlRuleDefNames.ModifyVariableDtm);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_MODIFY_VARIABLE));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_VIEWMODEL_MODIFY_VARIABLE_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyVariableDtmControlRule(modifyVarRule);

        VoObjectControlRuleDef objectRuleDef = new VoObjectControlRuleDef(this);
        getChildControlRules().put(CommonModelNames.MainObject, objectRuleDef);

    }

    public ControlRuleDefItem getAddCustomActonControlRule() {
        return super.getControlRuleItem(VoControlRuleDefNames.AddCustomAction);
    }

    public void setAddCustomActonControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(VoControlRuleDefNames.AddCustomAction, ruleItem);
    }

    public ControlRuleDefItem getModifyCustomActonControlRule() {
        return super.getControlRuleItem(VoControlRuleDefNames.ModifyCustomActions);
    }

    public void setModifyCustomActonControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(VoControlRuleDefNames.ModifyCustomActions, ruleItem);
    }

    public ControlRuleDefItem getAddVariableDtmControlRule() {
        return super.getControlRuleItem(VoControlRuleDefNames.AddVariableDtm);
    }

    public void setAddVariableDtmControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(VoControlRuleDefNames.AddVariableDtm, ruleItem);
    }

    public ControlRuleDefItem getModifyVariableDtmControlRule() {
        return super.getControlRuleItem(VoControlRuleDefNames.ModifyVariableDtm);
    }

    public void setModifyVariableDtmControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(VoControlRuleDefNames.ModifyVariableDtm, ruleItem);
    }
}
