package com.inspur.edp.formserver.viewmodel.json.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoObjectSourceType;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

/**
 * The  Josn Serializer Of View Model Object Mapping
 *
 * @ClassName: GspVoObjectMappingSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoObjectMappingSerializer extends ViewModelMappingSerializer {

    public GspVoObjectMappingSerializer() {
    }

    public GspVoObjectMappingSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected final void writeExtendMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
        GspVoObjectMapping voMapping = (GspVoObjectMapping) mapping;
        if (isFull || voMapping.getSourceType() != GspVoObjectSourceType.BeObject) {
            SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.SourceType, voMapping.getSourceType().toString());
        }
        //扩展模型属性
        writeExtendObjMappingProperty(writer, mapping);
    }

    protected void writeExtendObjMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
    }
}


