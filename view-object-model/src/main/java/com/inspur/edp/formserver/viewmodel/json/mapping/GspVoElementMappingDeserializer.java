package com.inspur.edp.formserver.viewmodel.json.mapping;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoElementMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspVoElementSourceType;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

/**
 * The Josn Deserializer Of View Model Element Mapping
 *
 * @ClassName: GspVoElementMappingDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoElementMappingDeserializer extends ViewModelMappingDeserializer {

    @Override
    protected void beforeVMMappingDeserializer(ViewModelMapping viewModelMapping) {
        GspVoElementMapping voMapping = (GspVoElementMapping) viewModelMapping;
        voMapping.setTargetObjectId("");
        voMapping.setTargetElementId("");
        voMapping.setTargetMetadataId("");
        voMapping.setTargetMetadataPkgName("");
        voMapping.setTargetObjId("");
    }

    @Override
    protected ViewModelMapping createVmMapping() {
        return new GspVoElementMapping();
    }

    @Override
    protected boolean readExtendMappingProperty(JsonParser reader, ViewModelMapping mapping, String propertyName) {
        boolean hasProperty = true;
        GspVoElementMapping voMapping = (GspVoElementMapping) mapping;
        switch (propertyName) {
            case ViewModelJsonConst.SourceType:
                voMapping.setSourceType(SerializerUtils.readPropertyValue_Enum(reader, GspVoElementSourceType.class, GspVoElementSourceType.values(), GspVoElementSourceType.BeElement));
                break;
            case ViewModelJsonConst.TargetObjectId:
                voMapping.setTargetObjectId(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.TargetElementId:
                voMapping.setTargetElementId(SerializerUtils.readPropertyValue_String(reader));
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }
}
