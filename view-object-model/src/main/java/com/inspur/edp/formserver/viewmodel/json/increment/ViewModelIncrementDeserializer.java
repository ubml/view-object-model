package com.inspur.edp.formserver.viewmodel.json.increment;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.json.increment.CommonModelIncrementDeserializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementDeserializer;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;
import com.inspur.edp.formserver.viewmodel.increment.ViewModelIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

import java.io.IOException;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelIncrementDeserializer extends CommonModelIncrementDeserializer {
    @Override
    protected CommonObjectIncrementDeserializer getObjectIncrementDeserializer() {
        return new ViewObjectIncrementDeserializer();
    }

    protected CommonModelIncrement createCommonModelIncrement() {
        return new ViewModelIncrement();
    }

    @Override
    protected void readExtendInfo(CommonModelIncrement value, JsonNode node) {
        ViewModelIncrement increment = (ViewModelIncrement) value;
        JsonNode valueConfigIncrements = node.get(ViewModelJsonConst.HelpIncrements);
        if (valueConfigIncrements != null)
            readValueConfigs(increment, valueConfigIncrements);
        JsonNode actionIncrements = node.get(ViewModelJsonConst.Actions);
        if (actionIncrements != null)
            readActions(increment, actionIncrements);

    }

    private void readValueConfigs(ViewModelIncrement increment, JsonNode node) {

        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array) {
            HelpConfigIncrementDeserializer serializer = new HelpConfigIncrementDeserializer();
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(HelpConfigIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(ViewModelJsonConst.HelpIncrement);

                HelpConfigIncrement helpIncrement = mapper.readValue(mapper.writeValueAsString(valueNode), HelpConfigIncrement.class);
                helpIncrement.setElementId(key);
                increment.getValueHelpConfigs().put(key, helpIncrement);
            } catch (IOException e) {
                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1030, e);
            }
        }

    }

    private void readActions(ViewModelIncrement increment, JsonNode node) {
        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array) {
            VoActionIncrementDeserializer serializer = new VoActionIncrementDeserializer();
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(VoActionIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(ViewModelJsonConst.Action);

                VoActionIncrement voActionIncrement = mapper.readValue(mapper.writeValueAsString(valueNode), VoActionIncrement.class);
                voActionIncrement.setActionId(key);
                increment.getActions().put(key, voActionIncrement);
            } catch (IOException e) {
                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1031, e);
            }
        }
    }

}
