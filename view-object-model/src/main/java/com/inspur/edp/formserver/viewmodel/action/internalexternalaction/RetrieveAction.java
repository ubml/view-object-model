package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

/**
 * The Definition Of Retrieve Action
 *
 * @ClassName: RetrieveAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RetrieveAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String id = "7a02f472-5bbd-424b-9d9e-f82e3f9f448e";
    public static final String code = "Retrieve";

    public RetrieveAction() {
        setID(id);
        setCode(code);
        setName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.ACTION_RETRIEVE_NAME));
    }
}