package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs;

import com.inspur.edp.cef.designtime.api.dtconsistencycheck.AbstractDtEventArgs;


public class AbstractVoActionEventArgs extends AbstractDtEventArgs {
    public String viewModelId;
    public String viewObjectId;
    public String actionId;
    public String metadataPath;

    public String getMetadataPath() {
        return metadataPath;
    }

    public void setMetadataPath(String metadataPath) {
        this.metadataPath = metadataPath;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public AbstractVoActionEventArgs() {
    }

    public String getViewModelId() {
        return viewModelId;
    }

    public void setViewModelId(String viewModelId) {
        this.viewModelId = viewModelId;
    }

    public String getViewObjectId() {
        return viewObjectId;
    }

    public void setViewObjectId(String viewObjectId) {
        this.viewObjectId = viewObjectId;
    }

}
