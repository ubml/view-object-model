package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

/**
 * The Definition Of Data Reversal Mapping Action
 *
 * @ClassName: DataReversalMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DataReversalMappingAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String id = "991bf216-f55b-40bf-bb42-1b831b6ef3e9";
    public static final String code = "DataReversalMapping";

    public DataReversalMappingAction() {
        setID(id);
        setCode(code);
        setName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.ACTION_DATAREVERSALMAPPING_NAME));
    }
}