package com.inspur.edp.formserver.viewmodel.common;

import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;

/**
 * The Definition Of Help Extend Action
 *
 * @ClassName: HelpExtendAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class HelpExtendAction {
    private VMActionCollection beforeHelp;

    /**
     * 帮助前扩展
     */
    public final VMActionCollection getBeforeHelp() {
        if (beforeHelp == null) {
            beforeHelp = new VMActionCollection();
        }
        return beforeHelp;
    }

    public final void setBeforeHelp(VMActionCollection value) {
        beforeHelp = value;
    }

}