package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs;

public class ChangingVoActionCodeEventArgs extends AbstractVoActionEventArgs {

    protected String newActionCode;
    protected String originalActionCode;

    public ChangingVoActionCodeEventArgs() {
    }

    public ChangingVoActionCodeEventArgs(String newActionCode, String originalActionCode) {
        this.newActionCode = newActionCode;
        this.originalActionCode = originalActionCode;
    }

    public String getNewActionCode() {
        return newActionCode;
    }

    public void setNewActionCode(String newActionCode) {
        this.newActionCode = newActionCode;
    }

    public String getOriginalActionCode() {
        return originalActionCode;
    }

    public void setOriginalActionCode(String originalActionCode) {
        this.originalActionCode = originalActionCode;
    }

}
