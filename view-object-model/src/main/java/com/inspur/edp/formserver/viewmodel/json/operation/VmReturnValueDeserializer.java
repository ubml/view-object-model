package com.inspur.edp.formserver.viewmodel.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelReturnValue;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelVoidReturnValue;
/**
 * The Josn Deserializer Of View Model Action Return Value
 *
 * @ClassName: VmReturnValueDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VmReturnValueDeserializer extends VmParameterDeserializer<ViewModelReturnValue> {
    @Override
    protected ViewModelReturnValue createVmPara() {
        return new ViewModelReturnValue();
    }

    @Override
    public ViewModelReturnValue deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        ViewModelReturnValue rez = (ViewModelReturnValue) super.deserialize(jsonParser, null);
        if (!rez.isVoidReturnType()) {
            if (ViewModelVoidReturnValue.assembly.equals(rez.getAssembly())
                    && ViewModelVoidReturnValue.className.equals(rez.getClassName())) {
                ViewModelReturnValue newRez = new ViewModelVoidReturnValue();
                newRez.setID(rez.getID());
                newRez.setParamCode(rez.getParamCode());
                newRez.setParamDescription(rez.getParamDescription());
                newRez.setParameterType(rez.getParameterType());
                return newRez;
            }
            return rez;
        }
        ViewModelVoidReturnValue newRez = getVoidReturnType(rez);
        return newRez;
    }

    private ViewModelVoidReturnValue getVoidReturnType(ViewModelReturnValue viewModelReturnValue) {
        ViewModelVoidReturnValue newRez = new ViewModelVoidReturnValue();
        newRez.setID(viewModelReturnValue.getID());
        newRez.setParamCode(viewModelReturnValue.getParamCode());
        newRez.setParamDescription(viewModelReturnValue.getParamDescription());
        newRez.setParameterType(viewModelReturnValue.getParameterType());
        newRez.setMode(viewModelReturnValue.getMode());
        return newRez;
    }
}
