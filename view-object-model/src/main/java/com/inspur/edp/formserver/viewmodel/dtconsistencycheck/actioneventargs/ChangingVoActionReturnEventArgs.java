package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs;


public class ChangingVoActionReturnEventArgs extends AbstractVoActionEventArgs {
    protected String originalReturnType;
    protected String newReturnType;

    public ChangingVoActionReturnEventArgs() {
    }

    public String getOriginalReturnType() {
        return originalReturnType;
    }

    public void setOriginalReturnType(String originalReturnType) {
        this.originalReturnType = originalReturnType;
    }

    public String getNewReturnType() {
        return newReturnType;
    }

    public void setNewReturnType(String newReturnType) {
        this.newReturnType = newReturnType;
    }
}
