package com.inspur.edp.formserver.viewmodel.common;

import lombok.Getter;
import lombok.Setter;

public class VMAuthConfig {
    @Getter
    @Setter
    private String authType;

    @Getter
    @Setter
    private String extend2;

    @Getter
    @Setter
    private String extend3;

    @Getter
    @Setter
    private String extend4;

    @Getter
    @Setter
    private String extend5;
}
