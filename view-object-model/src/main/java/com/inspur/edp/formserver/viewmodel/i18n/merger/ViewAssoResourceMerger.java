package com.inspur.edp.formserver.viewmodel.i18n.merger;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.merge.GspAssoRefEleResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.GspAssoResourceMerger;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;

public class ViewAssoResourceMerger extends GspAssoResourceMerger {

    public ViewAssoResourceMerger(GspAssociation asso,
                                  ICefResourceMergeContext context) {
        super(asso, context);
    }

    @Override
    protected GspAssoRefEleResourceMerger getAssoRefElementResourcemerger(
            ICefResourceMergeContext context, IGspCommonElement element) {
        return new ViewAssoRefEleResourceMerger((GspViewModelElement) element, context);
    }
}
