package com.inspur.edp.formserver.viewmodel.extendinfo.entity;

import com.inspur.edp.cdp.common.utils.json.JsonUtil;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;
import io.swagger.util.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;

public class VoConfigCollectionInfoConverter implements AttributeConverter<VoConfigCollectionInfo, String> {
    private static Logger logger = LoggerFactory.getLogger(VoConfigCollectionInfo.class);

    @Override
    public String convertToDatabaseColumn(VoConfigCollectionInfo attribute) {
        return Json.pretty(attribute);
        // return JsonSerializerUtils.writeValueAsString(attribute);
    }

    @Override
    public VoConfigCollectionInfo convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.isEmpty()) {
            return new VoConfigCollectionInfo();
        }
        VoConfigCollectionInfo configCollectionInfo = null;
        try {
            configCollectionInfo = JsonUtil.toObject(dbData, VoConfigCollectionInfo.class);
        } catch (Exception e) {
            throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1027, e, dbData);
        }
        return configCollectionInfo;
    }
}
