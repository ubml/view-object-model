package com.inspur.edp.formserver.viewmodel.json.operation;

import com.inspur.edp.formserver.viewmodel.action.mappedbiz.MappedBizActionParameter;

/**
 * The Json  Serializer Of The View Model Action Parameter Of  Mapped Be Action
 *
 * @ClassName: MappedBizActionParaSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionParaSerializer extends VmParameterSerializer<MappedBizActionParameter> {
    public MappedBizActionParaSerializer() {
    }

    public MappedBizActionParaSerializer(boolean full) {
        super(full);
        isFull = full;
    }
}
