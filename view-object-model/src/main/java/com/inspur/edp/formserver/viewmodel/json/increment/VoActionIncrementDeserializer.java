package com.inspur.edp.formserver.viewmodel.json.increment;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionType;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.AddedVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.ModifyVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedBizActionDeserializer;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedCdpActionDeserializer;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionDeserializer;

import java.io.IOException;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoActionIncrementDeserializer extends JsonDeserializer<VoActionIncrement> {

    @Override
    public VoActionIncrement deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(jsonParser);
        String incrementTypeStr = node.get(CefNames.IncrementType).textValue();
        if (incrementTypeStr == null || "".equals(incrementTypeStr))
            return null;
        IncrementType incrementType = IncrementType.valueOf(incrementTypeStr);
        switch (incrementType) {
            case Added:
                return readAddIncrementInfo(node);
            case Modify:
                return readModifyIncrementInfo(node);
            case Deleted:
//                return readDeletedIncrementInfo(node);

        }
        return null;
    }

    private AddedVoActionIncrement readAddIncrementInfo(JsonNode node) {
        AddedVoActionIncrement addIncrement = new AddedVoActionIncrement();
        JsonNode addVauleNode = node.get(ViewModelJsonConst.AddedAction);
        if (addVauleNode == null)
            return addIncrement;
        ViewModelAction action = readBizOperation(addVauleNode);
        addIncrement.setAction(action);
        return addIncrement;
    }

    private ModifyVoActionIncrement readModifyIncrementInfo(JsonNode node) {
        ModifyVoActionIncrement modifyIncrement = new ModifyVoActionIncrement();
        JsonNode modifyVauleNode = node.get(ViewModelJsonConst.ModifyAction);
        if (modifyVauleNode == null)
            return modifyIncrement;
        ViewModelAction action = readBizOperation(modifyVauleNode);
        modifyIncrement.setAction(action);
        return modifyIncrement;
    }

    private ViewModelAction readBizOperation(JsonNode node) {
        String typeJson = node.get("Type").textValue();
        ViewModelActionType type = ViewModelActionType.valueOf(typeJson);
        VmActionDeserializer deserializer = createDeserializer(type);

        SimpleModule module = new SimpleModule();
        module.addDeserializer(ViewModelAction.class, deserializer);
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(module);
        try {
            return mapper.readValue(node.toString(), ViewModelAction.class);
        } catch (IOException e) {
            throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1034, e, node == null ? null : node.toString());
        }
    }

    private VmActionDeserializer createDeserializer(ViewModelActionType type) {
        switch (type) {
            case BEAction:
                return new MappedBizActionDeserializer();
            case VMAction:
                return new MappedCdpActionDeserializer();
            default:
                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1035, null, type == null ? null : type.toString());
        }
    }
}
