package com.inspur.edp.formserver.viewmodel.json.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedConfig;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedFieldsMapping;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedType;

import java.io.IOException;

public class LinkedConfigDeserializer extends JsonDeserializer<LinkedConfig> {
    @Override
    public LinkedConfig deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        LinkedConfig linkedConfig = new LinkedConfig();
        SerializerUtils.readStartObject(parser);
        while (parser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(parser);
            switch (propName) {
                case ViewModelJsonConst.LinkedFieldsMapping:
                    readLinkedFieldsMapping(parser, linkedConfig);
                    break;
                case ViewModelJsonConst.SourceFieldInChild:
                    linkedConfig.setSourceFieldInChild(SerializerUtils.readPropertyValue_boolean(parser));
                    break;
                case ViewModelJsonConst.EnableFilterWhenConditionEmpty:
                    linkedConfig.setEnableFilterWhenConditionEmpty(SerializerUtils.readPropertyValue_boolean(parser));
                    break;
                case ViewModelJsonConst.LinkedType:
                    linkedConfig.setLinkedType(SerializerUtils.readPropertyValue_Enum(parser, LinkedType.class, LinkedType.values(), LinkedType.Normal));
                    break;
                default:
                    throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1039, null, propName);
            }
        }
        SerializerUtils.readEndObject(parser);
        return linkedConfig;
    }

    private void readLinkedFieldsMapping(JsonParser jsonParser, LinkedConfig linkedConfig) throws IOException {
        LinkedFieldsMapping fieldsMapping = SerializerUtils.readPropertyValue_Object(LinkedFieldsMapping.class, jsonParser);
        linkedConfig.setLinkedFieldsMapping(fieldsMapping);
        jsonParser.nextToken();
    }
}
