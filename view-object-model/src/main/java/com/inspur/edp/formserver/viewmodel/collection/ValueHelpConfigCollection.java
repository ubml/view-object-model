package com.inspur.edp.formserver.viewmodel.collection;

import com.inspur.edp.cef.designtime.api.collection.BaseList;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;

import java.io.Serializable;

/**
 * The Collection Of The Help Config
 *
 * @ClassName: ValueHelpConfigCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ValueHelpConfigCollection extends BaseList<ValueHelpConfig> implements Cloneable, Serializable {
    ///#region 构造函数

    /**
     * 构造函数
     */
    public ValueHelpConfigCollection() {
        super();
    }

    ///#endregion

    ///#region 属性

    /**
     * 获取指定ID的值帮助配置
     *
     * @param elementId Element标识
     * @return 值帮助配置
     */
    public final ValueHelpConfig getItem(String elementId) {
        for (ValueHelpConfig item : this) {
            if (item.getElementId().equals(elementId)) {
                return item;
            }
        }
        return null;
//		return Items.firstOrDefault(i => elementId.equals(i.ElementId));
    }

    ///#endregion

    ///#region 方法

    /**
     * 批量添加值帮助配置
     *
     * @param items 值帮助配置集合
     */
    public final void addRange(ValueHelpConfig[] items) {
        for (ValueHelpConfig item : items) {
            add(item);
        }
    }

    /**
     * 批量添加值帮助配置
     *
     * @param items 值帮助配置集合
     */
    public final void addRange(Iterable<ValueHelpConfig> items) {
        for (ValueHelpConfig item : items) {
            add(item);
        }
    }

    /**
     * 克隆
     *
     * @return 值帮助配置集合的副本
     */
//	public final Object clone()
    @Override
    public final ValueHelpConfigCollection clone() {
        ValueHelpConfigCollection newCollection = new ValueHelpConfigCollection();

        for (ValueHelpConfig item : this) {
            newCollection.add(item);
        }

        return newCollection;
    }

    ///#endregion

    @Override
    public boolean remove(Object helpConfig) {
        if (helpConfig == null) {
            return false;
        }
        String id = ((ValueHelpConfig) helpConfig).getElementId();
        if (id == null) {
            return false;
        }
        super.removeIf(item ->
                id.equals(item.getElementId())
        );
        return true;
    }

    @Override
    public boolean contains(Object helpConfig) {
        if (helpConfig == null)
            return false;
        String id = ((ValueHelpConfig) helpConfig).getElementId();
        if (id == null) {
            return false;
        }
        for (ValueHelpConfig item : this) {
            if (item.getElementId().equals(id))
                return true;
        }
        return false;
    }

}