package com.inspur.edp.formserver.viewmodel.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.HelpExtendAction;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionCollectionSerializer;
import com.inspur.edp.formserver.viewmodel.linkedconfig.LinkedConfigCollection;

/**
 * The Josn Serializer Of View Model Help Configuration
 *
 * @ClassName: ValueHelpConfigSerizlizer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ValueHelpConfigSerizlizer extends JsonSerializer<ValueHelpConfig> {

    protected boolean isFull = true;

    public ValueHelpConfigSerizlizer() {
    }

    public ValueHelpConfigSerizlizer(boolean full) {
        isFull = full;
    }

    @Override
    public void serialize(ValueHelpConfig helpConfig, JsonGenerator writer,
                          SerializerProvider serializers) {
        SerializerUtils.writeStartObject(writer);
        SerializerUtils
                .writePropertyValue(writer, ViewModelJsonConst.ElementId, helpConfig.getElementId());
        SerializerUtils
                .writePropertyValue(writer, ViewModelJsonConst.HelperId, helpConfig.getHelperId());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.FilterExpression,
                helpConfig.getFilterExpression());
        SerializerUtils
                .writePropertyValue(writer, CefNames.CustomizationInfo, helpConfig.getCustomizationInfo());
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.EnableCustomHelpAuth);
        SerializerUtils.writePropertyValue_boolean(writer, helpConfig.getEnableCustomHelpAuth());
        writeHelpExtend(writer, helpConfig.getHelpExtend());
        writeLinkedConfigs(writer, helpConfig.getLinkedConfigs());
        SerializerUtils.writeEndObject(writer);
    }

    private void writeLinkedConfigs(JsonGenerator writer, LinkedConfigCollection linkedConfigs) {
        if (isFull || (linkedConfigs != null && linkedConfigs.size() != 0)) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.LinkedConfigs);
            SerializerUtils.writeArray(writer, new LinkedConfigSerializer(isFull), linkedConfigs);
        }
    }

    private void writeHelpExtend(JsonGenerator writer, HelpExtendAction helpExtend) {
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.HelpExtend);

        SerializerUtils.writeStartObject(writer);
        if (helpExtend != null) {
            SerializerUtils.writePropertyName(writer, ViewModelJsonConst.BeforeHelp);
            getVMActionCollectionConvertor().serialize(helpExtend.getBeforeHelp(), writer, null);
        }
        SerializerUtils.writeEndObject(writer);
    }

    private VmActionCollectionSerializer getVMActionCollectionConvertor() {
        return new VmActionCollectionSerializer(isFull);
    }

}
