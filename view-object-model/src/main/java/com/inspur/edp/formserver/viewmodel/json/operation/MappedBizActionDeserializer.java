package com.inspur.edp.formserver.viewmodel.json.operation;

import com.inspur.edp.formserver.viewmodel.action.MappedBizAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.mappedbiz.MappedBizActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParameterCollection;

/**
 * The Json  Deserializer Of The View Model Action Of  Mapped Be Action
 *
 * @ClassName: MappedBizActionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionDeserializer extends VmActionDeserializer<MappedBizAction> {
    @Override
    protected MappedBizAction createOp() {
        return new MappedBizAction();
    }

    @Override
    protected void beforeVnactionDeserializer(ViewModelAction op) {
    }

    @Override
    protected VmParameterDeserializer createPrapDeserializer() {
        return new MappedBizActionParaDeserializer();
    }

    @Override
    protected ViewModelParameterCollection createPrapCollection() {
        return new MappedBizActionParameterCollection();
    }
}
