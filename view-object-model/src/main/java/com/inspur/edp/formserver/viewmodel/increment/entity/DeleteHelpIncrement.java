package com.inspur.edp.formserver.viewmodel.increment.entity;

import com.inspur.edp.cef.designtime.api.increment.DeletedIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

public class DeleteHelpIncrement extends HelpConfigIncrement implements DeletedIncrement {

    public DeleteHelpIncrement(String elementId) {
        setElementId(elementId);
    }

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Deleted;
    }

    @Override
    public String getDeleteId() {
        return getElementId();
    }
}
