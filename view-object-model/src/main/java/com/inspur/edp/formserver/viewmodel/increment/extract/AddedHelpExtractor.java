package com.inspur.edp.formserver.viewmodel.increment.extract;

import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.increment.entity.AddedHelpIncrement;

public class AddedHelpExtractor {

    public AddedHelpExtractor() {
    }

    public AddedHelpIncrement extract(ValueHelpConfig helpConfig) {
        AddedHelpIncrement addedEntityIncrement = this.createIncrement();
        addedEntityIncrement.setHelpConfig(helpConfig);
        return addedEntityIncrement;
    }

    protected AddedHelpIncrement createIncrement() {
        return new AddedHelpIncrement();
    }
}
