package com.inspur.edp.formserver.viewmodel.i18n;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.CommonElementResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.GspAssoResourceExtractor;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;

class ViewEleResourceExtractor extends CommonElementResourceExtractor {

    public ViewEleResourceExtractor(GspViewModelElement field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(field, context, parentResourceInfo);
    }

    @Override
    protected final void extractExtendElementProperties(IGspCommonElement commonField) {
    }

    @Override
    protected final GspAssoResourceExtractor getGSPAssoResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo fieldPrefixInfo,
            GspAssociation asso) {
        return new ViewAssoResourceExtractor(asso, getContext(), fieldPrefixInfo);
    }
}
