package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmFieldControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser.VoFieldControlRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.serializer.VoFieldControlRuleDefSerializer;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

@JsonSerialize(using = VoFieldControlRuleDefSerializer.class)
@JsonDeserialize(using = VoFieldControlRuleDefParser.class)
public class VoFieldControlRuleDef extends CmFieldControlRuleDef {
    public VoFieldControlRuleDef(ControlRuleDefinition parentRuleDefinition) {
        super(parentRuleDefinition);
        super.setRuleObjectType(VoFieldControlRuleNames.VoFieldRuleObjectType);
        init();
    }

    private void init() {
        ControlRuleDefItem nameRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Name);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_NAME));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_NAME_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setNameControlRule(nameRule);

        ControlRuleDefItem lengthRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Length);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_LENGTH));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_LENGTH_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setLengthControlRule(lengthRule);

        ControlRuleDefItem precisionRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Precision);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_PRECISION));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_PRECISION_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setPrecisionControlRule(precisionRule);

        ControlRuleDefItem defaultValueRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.DefaultValue);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_DEFAULTVALUE));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_DEFAULTVALUE_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setDefaultValueControlRule(defaultValueRule);

        ControlRuleDefItem multiLanRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.MultiLanField);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_MULTILANGUAGE));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_MULTILANGUAGE_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setMultiLanFieldControlRule(multiLanRule);

        ControlRuleDefItem readonlyRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Readonly);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_READONLY));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_READONLY_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setReadonlyControlRule(readonlyRule);

        ControlRuleDefItem requiredRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonFieldRuleNames.Required);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_REQUIRED));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_FIELD_REQUIRED_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setRequiredControlRule(requiredRule);
    }
}
