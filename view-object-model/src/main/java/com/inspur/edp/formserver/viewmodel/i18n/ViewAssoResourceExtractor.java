package com.inspur.edp.formserver.viewmodel.i18n;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.GspAssoRefEleResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.GspAssoResourceExtractor;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;

class ViewAssoResourceExtractor extends GspAssoResourceExtractor {

    public ViewAssoResourceExtractor(GspAssociation asso, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(asso, context, parentResourceInfo);
    }

    @Override
    protected final GspAssoRefEleResourceExtractor getAssoRefElementResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo assoPrefixInfo,
            IGspCommonElement field) {
        return new ViewAssoRefEleResourceExtractor((GspViewModelElement) field, context, assoPrefixInfo);
    }

}
