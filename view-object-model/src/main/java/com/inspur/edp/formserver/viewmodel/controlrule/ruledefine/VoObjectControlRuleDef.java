package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser.VoObjectControlRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.serializer.VoObjectControlRuleDefSerializer;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

@JsonSerialize(using = VoObjectControlRuleDefSerializer.class)
@JsonDeserialize(using = VoObjectControlRuleDefParser.class)
public class VoObjectControlRuleDef extends CmEntityControlRuleDef {
    public VoObjectControlRuleDef(ControlRuleDefinition parentRuleDefinition) {
        super(parentRuleDefinition);
        super.setRuleObjectType(VoObjectRuleDefNames.VoObjectRuleObjectType);
        init();
    }

    private void init() {

        ControlRuleDefItem nameRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Name);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_NAME));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_NAME_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setNameControlRule(nameRule);

        ControlRuleDefItem addChildEntityRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmEntityRuleNames.AddChildEntity);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_ADD_CHILD_ENTITY));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_ADD_CHILD_ENTITY_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setAddChildEntityControlRule(addChildEntityRule);

        ControlRuleDefItem modifyChildEntityRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmEntityRuleNames.ModifyChildEntities);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_MODIFY_CHILD_ENTITY));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_MODIFY_CHILD_ENTITY_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyChildEntitiesControlRule(modifyChildEntityRule);

        ControlRuleDefItem addElementRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonDataTypeRuleNames.AddField);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_ADD_FIELD));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_ADD_FIELD_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setAddFieldControlRule(addElementRule);

        ControlRuleDefItem modifyElementRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CommonDataTypeRuleNames.ModifyFields);
                this.setRuleDisplayName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_MODIFY_FIELD));
                this.setDescription(VMI8nResourceUtil.getMessage(VoResourceKeyNames.RULE_OBJECT_MODIFY_FIELD_DESCRIPTION));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyFieldsControlRule(modifyElementRule);

        VoFieldControlRuleDef fieldRuleDef = new VoFieldControlRuleDef(this);
        getChildControlRules().put(CommonModelNames.Element, fieldRuleDef);
    }
}
