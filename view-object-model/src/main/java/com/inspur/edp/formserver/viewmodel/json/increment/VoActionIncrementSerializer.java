package com.inspur.edp.formserver.viewmodel.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionType;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.AddedVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.ModifyVoActionIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.action.VoActionIncrement;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedBizActionSerializer;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedCdpActionSerializer;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionSerializer;

import java.io.IOException;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoActionIncrementSerializer extends JsonSerializer<VoActionIncrement> {

    @Override
    public void serialize(VoActionIncrement value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(VoActionIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.IncrementType, value.getIncrementType().toString());
        switch (value.getIncrementType()) {
            case Added:
                writeAddedIncrement((AddedVoActionIncrement) value, gen);
                break;
            case Modify:
                writeModifyIncrement((ModifyVoActionIncrement) value, gen);
                break;
            case Deleted:
//                writeDeletedIncrement((DeletedEntityIncrement) value, gen);
                break;
            default:
                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1036, null, value.getIncrementType().toString());
        }
    }


    private void writeAddedIncrement(AddedVoActionIncrement increment, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, ViewModelJsonConst.AddedAction);
        writeBaseActionInfo(increment.getAction(), gen);
    }

    private void writeModifyIncrement(ModifyVoActionIncrement increment, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, ViewModelJsonConst.ModifyAction);
        writeBaseActionInfo(increment.getAction(), gen);
    }

    private void writeBaseActionInfo(ViewModelAction action, JsonGenerator gen) {
        VmActionSerializer convertor = getActionConvertor(action.getType());
        convertor.serialize(action, gen, null);
    }

    private VmActionSerializer getActionConvertor(ViewModelActionType type) {
        switch (type) {
            case BEAction:
                return new MappedBizActionSerializer();
            case VMAction:
                return new MappedCdpActionSerializer();
            case Custom:
            default:
                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1037, null, type.toString());
        }
    }
}
