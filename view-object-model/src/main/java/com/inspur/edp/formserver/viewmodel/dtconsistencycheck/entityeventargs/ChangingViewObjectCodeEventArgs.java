package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.entityeventargs;

public class ChangingViewObjectCodeEventArgs extends AbstractVoEntityArgs {

    protected String newCode;
    protected String originalCode;

    public ChangingViewObjectCodeEventArgs() {
    }

    public String getNewCode() {
        return newCode;
    }

    public void setNewCode(String newCode) {
        this.newCode = newCode;
    }

    public String getOriginalCode() {
        return originalCode;
    }

    public void setOriginalCode(String originalCode) {
        this.originalCode = originalCode;
    }
}
