package com.inspur.edp.formserver.viewmodel.json.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.entity.CustomizationInfo;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.ValueHelpConfig;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;
import com.inspur.edp.formserver.viewmodel.json.operation.VmActionCollectionDeserializer;

import java.io.IOException;

/**
 * The Josn Deserializer Of View Model Help Configuration
 *
 * @ClassName: ValueHelpConfigDeserizlizer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ValueHelpConfigDeserizlizer extends JsonDeserializer<ValueHelpConfig> {

    @Override
    public ValueHelpConfig deserialize(JsonParser jsonParser, DeserializationContext ctxt) {
        ValueHelpConfig config = new ValueHelpConfig();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            switch (propName) {
                case ViewModelJsonConst.HelperId:
                    config.setHelperId(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case ViewModelJsonConst.FilterExpression:
                    config.setFilterExpression(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case ViewModelJsonConst.ElementId:
                    config.setElementId(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case ViewModelJsonConst.EnableCustomHelpAuth:
                    config.setEnableCustomHelpAuth(SerializerUtils.readPropertyValue_boolean(jsonParser));
                    break;
                case CefNames.CustomizationInfo:
                    config.setCustomizationInfo((CustomizationInfo) SerializerUtils
                            .readPropertyValue_Object(CustomizationInfo.class, jsonParser));
                    try {
                        jsonParser.nextToken();
                    } catch (IOException e) {
                        throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1040, e, propName);
                    }
                    break;
                case ViewModelJsonConst.HelpExtend:
                    SerializerUtils.readStartObject(jsonParser);
                    if (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
                        String propertyName = SerializerUtils.readPropertyName(jsonParser);
                        switch (propertyName) {
                            case ViewModelJsonConst.BeforeHelp:
                                VmActionCollectionDeserializer deserializer1 = new VmActionCollectionDeserializer();
                                config.getHelpExtend().setBeforeHelp(deserializer1.deserialize(jsonParser, null));
                                break;
                            default:
                                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1041, null, propertyName);
                        }
                    }
                    SerializerUtils.readEndObject(jsonParser);
                    break;
                case ViewModelJsonConst.LinkedConfigs:
                    SerializerUtils
                            .readArray(jsonParser, new LinkedConfigDeserializer(), config.getLinkedConfigs());
                    break;
                default:
                    throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1042, null, propName);
            }
        }
        SerializerUtils.readEndObject(jsonParser);
        return config;
    }
}
