package com.inspur.edp.formserver.viewmodel.common;

/**
 * The Definition Of Element Help Type
 *
 * @ClassName: ElementHelpType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ElementHelpType {
    /**
     * 无
     */
    None(0),

    /**
     * 数据表帮助
     */
    SmartHelp(1);

    private int intValue;
    private static java.util.HashMap<Integer, ElementHelpType> mappings;

    private synchronized static java.util.HashMap<Integer, ElementHelpType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ElementHelpType>();
        }
        return mappings;
    }

    ElementHelpType(int value) {
        intValue = value;
        ElementHelpType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static ElementHelpType forValue(int value) {
        return getMappings().get(value);
    }
}