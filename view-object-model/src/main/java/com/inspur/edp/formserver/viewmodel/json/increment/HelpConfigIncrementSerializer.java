package com.inspur.edp.formserver.viewmodel.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;
import com.inspur.edp.formserver.viewmodel.increment.entity.AddedHelpIncrement;
import com.inspur.edp.formserver.viewmodel.increment.entity.HelpConfigIncrement;
import com.inspur.edp.formserver.viewmodel.json.model.ValueHelpConfigSerizlizer;

import java.io.IOException;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class HelpConfigIncrementSerializer extends JsonSerializer<HelpConfigIncrement> {

    @Override
    public void serialize(HelpConfigIncrement value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(HelpConfigIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.IncrementType, value.getIncrementType().toString());
        switch (value.getIncrementType()) {
            case Added:
                writeAddedIncrement((AddedHelpIncrement) value, gen);
                break;
            case Modify:
//                writeModifyIncrement((ModifyEntityIncrement) value, gen);
                break;
            case Deleted:
//                writeDeletedIncrement((DeletedEntityIncrement) value, gen);
                break;
            default:
                throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1029, null, value.getIncrementType().toString());
        }
    }

    private void writeAddedIncrement(AddedHelpIncrement value, JsonGenerator gen) {
        writeBaseAddedInfo(value, gen);
    }

    private void writeBaseAddedInfo(AddedHelpIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.AddedDataType);
        ValueHelpConfigSerizlizer helpConfigSerizlizer = new ValueHelpConfigSerizlizer();
        helpConfigSerizlizer.serialize(value.getHelpConfig(), gen, null);
    }
}
