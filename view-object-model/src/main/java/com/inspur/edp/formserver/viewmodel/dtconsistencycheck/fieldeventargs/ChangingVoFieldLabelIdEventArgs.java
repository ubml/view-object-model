package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.fieldeventargs;

public class ChangingVoFieldLabelIdEventArgs extends AbstractVofieldEventArgs {
    protected String newLabelId;
    protected String originalLabelId;

    public ChangingVoFieldLabelIdEventArgs() {
    }

    public ChangingVoFieldLabelIdEventArgs(String newLabelId, String originalLabelId) {
        this.newLabelId = newLabelId;
        this.originalLabelId = originalLabelId;
    }

    public String getNewLabelId() {
        return newLabelId;
    }

    public void setNewLabelId(String newLabelId) {
        this.newLabelId = newLabelId;
    }

    public String getOriginalLabelId() {
        return originalLabelId;
    }

    public void setOriginalLabelId(String originalLabelId) {
        this.originalLabelId = originalLabelId;
    }
}
