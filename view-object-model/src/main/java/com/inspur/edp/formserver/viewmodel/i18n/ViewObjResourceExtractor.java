package com.inspur.edp.formserver.viewmodel.i18n;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.i18n.CommonElementResourceExtractor;
import com.inspur.edp.das.commonmodel.i18n.CommonObjectResourceExtractor;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;
import com.inspur.edp.formserver.viewmodel.GspViewObject;

class ViewObjResourceExtractor extends CommonObjectResourceExtractor {

    public ViewObjResourceExtractor(GspViewObject commonDataType, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(commonDataType, context, parentResourceInfo);
    }

    @Override
    protected final void extractExtendObjProperties(IGspCommonObject dataType) {
    }

    @Override
    protected final CommonElementResourceExtractor getCommonEleResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo objPrefixInfo,
            IGspCommonElement field) {
        return new ViewEleResourceExtractor((GspViewModelElement) field, context, objPrefixInfo);
    }

    @Override
    protected final CommonObjectResourceExtractor getObjectResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo modelPrefixInfo,
            IGspCommonObject obj) {
        return new ViewObjResourceExtractor((GspViewObject) obj, context, modelPrefixInfo);
    }
}
