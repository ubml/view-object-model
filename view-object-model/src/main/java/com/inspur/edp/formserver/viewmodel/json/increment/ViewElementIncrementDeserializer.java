package com.inspur.edp.formserver.viewmodel.json.increment;

import com.inspur.edp.das.commonmodel.json.element.CmElementDeserializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonElementIncrementDeserializer;
import com.inspur.edp.formserver.viewmodel.json.element.ViewElementDeserializer;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewElementIncrementDeserializer extends CommonElementIncrementDeserializer {
    @Override
    protected CmElementDeserializer getCmElementDeserializer() {
        return new ViewElementDeserializer();
    }
}
