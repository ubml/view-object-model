package com.inspur.edp.formserver.viewmodel.common;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.GspViewObject;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;

import java.util.ArrayList;

public class LinkedHelpConfigUtil {
    /**
     * 将字段标签转换为字段ID(字段标签-->字段ID/字段标签.XX-->字段ID.XX)
     *
     * @param sourceObject
     * @param sourceField
     * @return
     */
    public static String getSourceFieldId(IGspCommonObject sourceObject, String sourceField) {
        String[] splitFields = getSplitFields(sourceField);
        String fieldPrefix = splitFields[0];
        String fieldSuffix = "";
        String sourceMappingField = "";
        if (splitFields.length > 1)
            fieldSuffix = splitFields[1];
        for (IGspCommonField containElement : sourceObject.getContainElements()) {
            if (fieldPrefix.equalsIgnoreCase(containElement.getLabelID())) {
                sourceMappingField = containElement.getID();
                break;
            }
        }
        if (fieldSuffix == null || "".equals(fieldSuffix))
            return sourceMappingField;
        return sourceMappingField + "." + fieldSuffix;
    }

    private static String[] getSplitFields(String sourceField) {
        String[] sourceFields = sourceField.split("\\.", 2);
        if (sourceFields == null || sourceFields.length == 0)
            throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1025, null);
        return sourceFields;
    }

    /**
     * 根据实体编号获取VO元数据上的指定实体对象
     *
     * @param viewModel
     * @param objCode
     * @return
     */
    public static GspViewObject getObjectByCode(GspViewModel viewModel, String objCode) {
        ArrayList<IGspCommonObject> allObjectList = viewModel.getAllObjectList();
        for (IGspCommonObject commonObject : allObjectList) {
            if (objCode.equalsIgnoreCase(commonObject.getCode())) {
                return (GspViewObject) commonObject;
            }
        }
        return null;
    }

    /**
     * 将字段ID转换为字段标签(字段ID-->字段标签/字段ID.XX-->字段标签.XX)
     *
     * @param sourceObject
     * @param sourceFieldId
     * @return
     */
    public static String getSourceMappingField(IGspCommonObject sourceObject, String sourceFieldId) {
        String[] splitFields = getSplitFields(sourceFieldId);
        String fieldPrefix = splitFields[0];
        String fieldSuffix = "";
        String sourceMappingField = "";
        if (splitFields.length > 1)
            fieldSuffix = splitFields[1];
        for (IGspCommonField containElement : sourceObject.getContainElements()) {
            if (fieldPrefix.equals(containElement.getID())) {
                sourceMappingField = containElement.getLabelID();
                break;
            }
        }
        if (fieldSuffix == null || "".equals(fieldSuffix))
            return sourceMappingField;
        return sourceMappingField + "." + fieldSuffix;
    }
}
