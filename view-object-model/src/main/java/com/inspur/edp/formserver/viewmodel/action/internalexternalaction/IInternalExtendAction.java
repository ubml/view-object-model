package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

/**
 * The Definition Of Internal Extend Action
 *
 * @ClassName: IInternalExtendAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IInternalExtendAction {
}