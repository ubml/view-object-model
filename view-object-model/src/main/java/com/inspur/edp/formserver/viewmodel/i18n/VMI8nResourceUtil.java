/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.i18n;

import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.commons.utils.StringUtils;
import io.iec.edp.caf.i18n.api.ResourceLocalizer;

import java.text.MessageFormat;

public class VMI8nResourceUtil {
    private final static String SU = "pfcommon";
    private final static String RESOURCE_FILE = "viewobject_model_designtime.properties";

    public static String getResourceValue(String su, String resourceFile, String key) {
        ResourceLocalizer localizer = SpringBeanUtils.getBean(ResourceLocalizer.class);
        return localizer.getString(key, resourceFile, su, "");
    }

    public static String getResourceValue(String key) {
        return getResourceValue(SU, RESOURCE_FILE, key);
    }

    /**
     * 获取国际化资源信息
     * @param msgCode 消息编码
     * @param messageParams 消息参数
     * @return 国际化消息
     */
    public static String getMessage(String msgCode, Object... messageParams) {
        // 获取资源文件中对应消息编码的文本内容
        String message = getResourceValue(msgCode);
        if (StringUtils.isEmpty(message)) {
            return msgCode;
        }
        // 如果消息参数为空，则直接返回消息编码
        if (messageParams == null || messageParams.length < 1) {
            return message;
        }
        // 根据消息参数构造消息
        return MessageFormat.format(message, messageParams);
    }


    /**
     * 获取国际化资源信息
     * @param msgCode 消息编码
     * @param messageParams 消息参数
     * @return 国际化消息
     */
    public static String getMessage(VoResourceKeyNames msgCode, Object... messageParams) {
        return getMessage(msgCode.getErrorCode(), messageParams);
    }
}
