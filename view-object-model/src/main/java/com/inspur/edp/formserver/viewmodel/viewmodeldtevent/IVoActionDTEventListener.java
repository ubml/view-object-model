package com.inspur.edp.formserver.viewmodel.viewmodeldtevent;

import com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs.*;
import io.iec.edp.caf.commons.event.IEventListener;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IVoActionDTEventListener extends IEventListener {

    public ChangingVoActionCodeEventArgs changingVoActionCode(ChangingVoActionCodeEventArgs args);

    public ChangingVoActionParamsEventArgs changingVoActionParams(
            ChangingVoActionParamsEventArgs args);

    public ChangingVoActionReturnEventArgs changingVoActionReturn(
            ChangingVoActionReturnEventArgs args);

    public DeletingVoActionEventArgs deletingVoAction(DeletingVoActionEventArgs args);

    public ChangingVoActionCollectTypeEventArgs changingVoActionCollectType(ChangingVoActionCollectTypeEventArgs args);
}
