package com.inspur.edp.formserver.viewmodel.action.viewmodelbase;

import com.inspur.edp.cef.designtime.api.collection.BaseList;
import com.inspur.edp.formserver.viewmodel.action.IViewModelParameter;
import com.inspur.edp.formserver.viewmodel.action.IViewModelParameterCollection;

/**
 * The Collection Of View Model Parameter
 *
 * @ClassName: ViewModelParameterCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelParameterCollection<T extends IViewModelParameter> extends
        BaseList<T> implements IViewModelParameterCollection<T> {

    public ViewModelParameterCollection() {
    }
}