package com.inspur.edp.formserver.viewmodel.json.operation;

import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameter;

/**
 * The Json Deserializer Of Mapped Component Action Parameter Definition
 *
 * @ClassName: MappedCdpParaDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpParaDeserializer extends VmParameterDeserializer<MappedCdpActionParameter> {
    @Override
    protected MappedCdpActionParameter createVmPara() {
        return new MappedCdpActionParameter();
    }
}
