package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmEntityControlRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoObjectControlRuleDef;

public class VoObjectControlRuleDefParser extends CmEntityControlRuleDefParser<VoObjectControlRuleDef> {

    @Override
    protected final CmEntityControlRuleDef createCmEntityRuleDef() {
        return new VoObjectControlRuleDef(null);
    }

    @Override
    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return new VoFieldControlRuleDefParser();
    }

}

