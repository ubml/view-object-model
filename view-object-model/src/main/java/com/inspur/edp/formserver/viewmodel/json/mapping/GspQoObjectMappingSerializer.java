package com.inspur.edp.formserver.viewmodel.json.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.common.mapping.GspQoObjectMapping;
import com.inspur.edp.formserver.viewmodel.json.ViewModelJsonConst;

/**
 * The Josn Serializer Of QO Object Mapping Definition
 *
 * @ClassName: GspQoObjectMappingSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspQoObjectMappingSerializer extends ViewModelMappingSerializer {

    public GspQoObjectMappingSerializer() {
    }

    public GspQoObjectMappingSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected final void writeExtendMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
        GspQoObjectMapping voMapping = (GspQoObjectMapping) mapping;
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IndexVoId, voMapping.getIndexVoId());
    }
}
