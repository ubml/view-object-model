package com.inspur.edp.formserver.viewmodel.dtconsistencycheck.actioneventargs;


public class ChangingVoActionParamsEventArgs extends AbstractVoActionEventArgs {
    protected String voActionParameterType;
    protected String newVoActionParameter;
    protected String originalVoActionParameter;

    public ChangingVoActionParamsEventArgs() {
    }

    public String getVoActionParameterType() {
        return voActionParameterType;
    }

    public void setVoActionParameterType(String voActionParameterType) {
        this.voActionParameterType = voActionParameterType;
    }

    public String getNewVoActionParameter() {
        return newVoActionParameter;
    }

    public void setNewVoActionParameter(String newVoActionParameter) {
        this.newVoActionParameter = newVoActionParameter;
    }

    public String getOriginalVoActionParameter() {
        return originalVoActionParameter;
    }

    public void setOriginalVoActionParameter(String originalVoActionParameter) {
        this.originalVoActionParameter = originalVoActionParameter;
    }
}
