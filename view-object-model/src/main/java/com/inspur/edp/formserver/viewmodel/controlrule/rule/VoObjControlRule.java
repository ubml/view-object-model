package com.inspur.edp.formserver.viewmodel.controlrule.rule;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.parser.VoObjectControlRuleParser;
import com.inspur.edp.formserver.viewmodel.controlrule.rule.serializer.VoObjectControlRuleSerializer;

@JsonSerialize(using = VoObjectControlRuleSerializer.class)
@JsonDeserialize(using = VoObjectControlRuleParser.class)
public class VoObjControlRule extends CmEntityControlRule {
}
