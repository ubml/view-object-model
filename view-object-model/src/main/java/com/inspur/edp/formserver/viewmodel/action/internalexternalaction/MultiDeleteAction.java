package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

/**
 * The Definition Of Mutly Delete Action
 *
 * @ClassName: MultiDeleteAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MultiDeleteAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String id = "7b1c3c4l-t1a4-4dyc-b75b-7695hcb3we7e";
    public static final String code = "MultiDelete";

    public MultiDeleteAction() {
        setID(id);
        setCode(code);
        setName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.ACTION_MULTIDELETE_NAME));
    }
}
