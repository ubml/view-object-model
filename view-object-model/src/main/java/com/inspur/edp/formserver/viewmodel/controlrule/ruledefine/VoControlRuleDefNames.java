package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine;

public class VoControlRuleDefNames {
    public static String VoControlRuleObjectType = "GspViewModel";
    public static String AddCustomAction = "AddCustomAction";
    public static String AddVariableDtm = "AddVariableDtm";
    public static String ModifyCustomActions = "ModifyCustomActions";
    public static String ModifyVariableDtm = "ModifyVariableDtm";

}
