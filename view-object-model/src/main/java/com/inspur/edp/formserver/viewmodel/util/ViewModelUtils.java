package com.inspur.edp.formserver.viewmodel.util;

import java.io.File;
import java.nio.file.Paths;

public class ViewModelUtils {

    /**
     * 检查空对象，空字符串
     */
    public static boolean checkNull(Object propValue) {
        if (propValue == null) {
            return true;
        }
        if (propValue.getClass().isAssignableFrom(String.class)) {
            String stringValue = (String) propValue;
            if (stringValue == null || stringValue.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public static String getDirectoryName(String path) {
        return new File(path).getParent();
    }

    public static String getCombinePath(String path1, String path2) {
        String path = Paths.get(path1).resolve(path2).toString();
        return handlePath(path);
    }

    public static String getCombinePath(String path1, String path2, String path3) {
        String path = Paths.get(path1).resolve(path2).resolve(path3).toString();
        return handlePath(path);
    }

    public static String handlePath(String path) {
        return path.replace("\\", File.separator);
    }

    public static String getSeparator() {
        return File.separator;
    }

    public static String getFileNameWithoutExtension(String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            final String message = path + " is  a directory";
            throw new IllegalArgumentException(message);
        }
        String fileName = file.getName();
        int point = fileName.lastIndexOf(".");
        if (point == -1) {
            return fileName;
        } else {
            return fileName.substring(0, point);
        }
    }
}
