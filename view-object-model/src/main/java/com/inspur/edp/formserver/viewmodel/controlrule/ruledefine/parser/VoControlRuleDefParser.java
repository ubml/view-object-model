package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmRuleDefParser;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoControlRuleDef;

public class VoControlRuleDefParser extends CmRuleDefParser<VoControlRuleDef> {
    @Override
    protected final CmControlRuleDef createCmRuleDef() {
        return new VoControlRuleDef();
    }

//    protected  Class getChildTypes(String childTypeName) {
//        return BeEntityControlRuleDef.class;
//    }

    @Override
    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return new VoObjectControlRuleDefParser();
    }

}
