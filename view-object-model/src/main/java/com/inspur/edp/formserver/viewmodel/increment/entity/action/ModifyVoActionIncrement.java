package com.inspur.edp.formserver.viewmodel.increment.entity.action;

import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;

public class ModifyVoActionIncrement extends VoActionIncrement {

    private ViewModelAction action;

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Modify;
    }

    public ViewModelAction getAction() {
        return action;
    }

    public void setAction(ViewModelAction action) {
        this.action = action;
    }
}
