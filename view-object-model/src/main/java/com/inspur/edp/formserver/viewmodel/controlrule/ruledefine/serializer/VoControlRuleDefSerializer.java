package com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmRuleDefSerializer;
import com.inspur.edp.formserver.viewmodel.controlrule.ruledefine.VoControlRuleDef;

public class VoControlRuleDefSerializer<T extends VoControlRuleDef> extends CmRuleDefSerializer<T> {

    @Override
    protected final void writeCmRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeCmRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
    }

}
