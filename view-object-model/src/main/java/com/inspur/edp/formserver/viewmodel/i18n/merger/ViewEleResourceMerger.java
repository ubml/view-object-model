package com.inspur.edp.formserver.viewmodel.i18n.merger;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.merge.CommonElementResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.GspAssoResourceMerger;
import com.inspur.edp.formserver.viewmodel.GspViewModelElement;

public class ViewEleResourceMerger extends CommonElementResourceMerger {

    public ViewEleResourceMerger(GspViewModelElement element,
                                 ICefResourceMergeContext context) {
        super(element, context);
    }

    @Override
    protected void extractExtendElementProperties(IGspCommonElement element) {

    }

    @Override
    protected GspAssoResourceMerger getGSPAssoResourceMerger(
            ICefResourceMergeContext context, GspAssociation asso) {
        return new ViewAssoResourceMerger(asso, getContext());
    }
}
