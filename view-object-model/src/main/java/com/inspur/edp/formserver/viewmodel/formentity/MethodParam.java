package com.inspur.edp.formserver.viewmodel.formentity;


import lombok.Getter;

@Getter
public class MethodParam {
    private String code;
    private String name;
    private String value;

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
