package com.inspur.edp.formserver.viewmodel.action.internalexternalaction;

import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.i18n.VMI8nResourceUtil;
import com.inspur.edp.formserver.viewmodel.i18n.names.VoResourceKeyNames;

/**
 * The  Definition Of Modify Action
 *
 * @ClassName: ModifyAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ModifyAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String id = "47dd3752-72a3-4c56-81c0-ae8ccfe5eb98";
    public static final String code = "Modify";

    public ModifyAction() {
        setID(id);
        setCode(code);
        setName(VMI8nResourceUtil.getMessage(VoResourceKeyNames.ACTION_MODIFY_NAME));
    }

}