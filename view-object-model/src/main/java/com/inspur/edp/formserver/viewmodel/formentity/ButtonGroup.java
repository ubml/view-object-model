package com.inspur.edp.formserver.viewmodel.formentity;

import lombok.Getter;

import java.util.List;

@Getter
public class ButtonGroup {
    private String id;
    private String name;
    private String code;
    private String type;
    private List<Button> buttons;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setButtons(List<Button> buttons) {
        this.buttons = buttons;
    }
}
