package com.inspur.edp.formserver.viewmodel.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.formserver.viewmodel.formentity.VoFormModel;

import java.io.IOException;
import java.util.HashMap;

public class VoFormModelDeserializer extends JsonDeserializer<HashMap<String, VoFormModel>> {
    @Override
    public HashMap<String, VoFormModel> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        HashMap<String, VoFormModel> map = new HashMap<>();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String key = SerializerUtils.readPropertyName(jsonParser);
            VoFormModel value = SerializerUtils.readPropertyValue_Object(VoFormModel.class, jsonParser);
            map.put(key, value);
        }
        SerializerUtils.readEndObject(jsonParser);
        return map;
    }
}