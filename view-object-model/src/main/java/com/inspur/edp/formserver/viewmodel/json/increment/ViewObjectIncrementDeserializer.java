package com.inspur.edp.formserver.viewmodel.json.increment;

import com.inspur.edp.das.commonmodel.json.increment.CommonElementIncrementDeserializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementDeserializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectDeserializer;
import com.inspur.edp.formserver.viewmodel.json.object.ViewObjectDeserializer;

/**
 * The  Josn Deserializer Of Biz Operation
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewObjectIncrementDeserializer extends CommonObjectIncrementDeserializer {
    @Override
    protected CmObjectDeserializer getCommonObjectDeserializer() {
        return new ViewObjectDeserializer();
    }

    @Override
    protected CommonElementIncrementDeserializer getCmElementIncrementDeserizlizer() {
        return new ViewElementIncrementDeserializer();
    }

    @Override
    protected CommonObjectIncrementDeserializer getCmObjectIncrementDeserializer() {
        return new ViewObjectIncrementDeserializer();
    }

}
