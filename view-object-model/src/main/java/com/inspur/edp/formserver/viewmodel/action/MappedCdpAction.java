package com.inspur.edp.formserver.viewmodel.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.formserver.viewmodel.action.mappedbiz.MappedBizActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedCdpActionDeserializer;
import com.inspur.edp.formserver.viewmodel.json.operation.MappedCdpActionSerializer;

import java.io.Serializable;

/**
 * The Definition Of The Parameter With Component
 *
 * @ClassName: MappedCdpAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = MappedCdpActionSerializer.class)
@JsonDeserialize(using = MappedCdpActionDeserializer.class)
public class MappedCdpAction extends MappedCdpActionBase implements Cloneable, Serializable {
    ///#region 属性

    private MappedCdpActionParameterCollection mappedCdpActionParams;

    /**
     * 类型
     */
    @Override
    public ViewModelActionType getType() {
        return ViewModelActionType.VMAction;
    }

    ///#endregion
    public MappedCdpAction() {
        mappedCdpActionParams = new MappedCdpActionParameterCollection();
    }
    ///#region 方法

    /**
     * 克隆
     *
     * @return VO节点映射
     */
    @Override
    public MappedCdpAction clone() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(this), MappedCdpAction.class);
        } catch (JsonProcessingException e) {
            throw new ViewModelException(e);
        }
    }

    @Override
    protected IViewModelParameterCollection getParameters() {
        return mappedCdpActionParams;
    }
    ///#endregion

    public void setParameters(MappedCdpActionParameterCollection params){
        mappedCdpActionParams = params;
    }
}