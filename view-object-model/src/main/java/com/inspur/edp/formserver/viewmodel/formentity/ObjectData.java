package com.inspur.edp.formserver.viewmodel.formentity;

import lombok.Getter;

import java.util.List;

@Getter
public class ObjectData {
    private String id;
    private String name;
    private String code;
    private List<ChildData> elements;
    private List<ObjectData> childObjects;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setElements(List<ChildData> elements) {
        this.elements = elements;
    }

    public void setChildObjects(List<ObjectData> childObjects) {
        this.childObjects = childObjects;
    }
}
