package com.inspur.edp.formserver.viewmodel.linkedconfig;

import com.inspur.edp.cef.designtime.api.collection.BaseList;

import java.io.Serializable;

public class LinkedConfigCollection extends BaseList<LinkedConfig> implements Cloneable, Serializable {
    public LinkedConfigCollection() {
    }
}
