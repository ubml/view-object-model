package com.inspur.edp.formserver.viewmodel.formentity;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Attachment {
    private String id;
    private String name;
    private String type;
    private List<String> configurableAttrs;
}
