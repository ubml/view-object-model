package com.inspur.edp.formserver.viewmodel.formentity;

import lombok.Getter;

import java.util.List;

@Getter
public class VoFormModel {
    private String id;
    private String projectPath;
    private String code;
    private String name;
    private String urlType;
    private String url;
    private List<Parameters> urlParameters;
    private String formatType;
    private String terminal;
    private List<ButtonGroup> buttonGroup;
    private ObjectData objectAuthInfo;
    private List<MethodGroup> methodGroup;
    private List<Attachment> attachmentGroup;


    public void setId(String id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUrlParameters(List<Parameters> urlParameters) {
        this.urlParameters = urlParameters;
    }

    public void setFormatType(String formatType) {
        this.formatType = formatType;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public void setButtonGroup(List<ButtonGroup> buttonGroup) {
        this.buttonGroup = buttonGroup;
    }

    public void setObjectAuthInfo(ObjectData objectData) {
        this.objectAuthInfo = objectData;
    }

    public void setMethods(List<MethodGroup> methodGroup) {
        this.methodGroup = methodGroup;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    public void setAttachmentGroup(List<Attachment> attachmentGroup) {
        this.attachmentGroup = attachmentGroup;
    }

}
