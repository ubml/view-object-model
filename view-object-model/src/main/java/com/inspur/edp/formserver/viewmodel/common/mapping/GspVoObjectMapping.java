package com.inspur.edp.formserver.viewmodel.common.mapping;

import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.formserver.viewmodel.exception.VoModelErrorCodes;

/**
 * The Definition Of View Model Object Mapping
 *
 * @ClassName: GspVoObjectMapping
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoObjectMapping extends ViewModelMapping {
    private GspVoObjectSourceType type =
            GspVoObjectSourceType.forValue(0);

    /**
     * 数据源（be或qo或vo）
     */
    public GspVoObjectSourceType getSourceType() {
        return type;
    }

    public void setSourceType(GspVoObjectSourceType value) {
        if (value == GspVoObjectSourceType.QoObject) {
            throw new ViewModelException(VoModelErrorCodes.GSP_VIEWOBJECT_MODEL_1002, null);
        }
        type = value;
    }

    public GspVoObjectMapping clone() {
        return (GspVoObjectMapping) super.clone();
    }
}