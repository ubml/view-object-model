package com.inspur.edp.formserver.viewmodel.action;

import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

public class ViewModelActionInfo {
    private String code;
    private String name;
    private GspViewModel viewModel;
    private String compName;
    private String compId;
    private GspMetadata sourceMeta;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GspViewModel getViewModel() {
        return viewModel;
    }

    public void setViewModel(GspViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public GspMetadata getSourceMeta() {
        return sourceMeta;
    }

    public void setSourceMeta(GspMetadata sourceMeta) {
        this.sourceMeta = sourceMeta;
    }


}
