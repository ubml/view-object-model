package com.inspur.edp.formserver.viewmodel.pushchangesetargs;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import java.util.List;

/**
 * 需要推送至表单数据: 元数据集合
 */
public class VmPushChangeSet {


    public VmPushChangeSet(
            List<GspMetadata> metadataList) {
        this.metadataList = metadataList;
    }

    public List<GspMetadata> getMetadataList() {
        return metadataList;
    }

    public void setMetadataList(
            List<GspMetadata> metadataList) {
        this.metadataList = metadataList;
    }

    private List<GspMetadata> metadataList;

}
