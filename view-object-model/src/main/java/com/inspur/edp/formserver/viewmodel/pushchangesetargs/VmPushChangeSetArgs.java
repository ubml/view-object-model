package com.inspur.edp.formserver.viewmodel.pushchangesetargs;

import com.inspur.edp.bef.bizentity.pushchangesetargs.AbstractPushChangeSetArgs;

public class VmPushChangeSetArgs extends AbstractPushChangeSetArgs {
    private VmPushChangeSet changeSet;

    public VmPushChangeSetArgs(VmPushChangeSet changeSet) {
        this.changeSet = changeSet;
    }

    public VmPushChangeSet getChangeSet() {
        return changeSet;
    }

    public void setChangeSet(VmPushChangeSet changeSet) {
        this.changeSet = changeSet;
    }


}
