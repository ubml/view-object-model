package com.inspur.edp.formserver.viewmodel.linkedconfig.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

public interface LinkedConfigService {
    /**
     * 增加帮助联动配置接口
     *
     * @param metadata 当前表单VO元数据
     */
    void handleLinkedConfig(GspMetadata metadata);
}
