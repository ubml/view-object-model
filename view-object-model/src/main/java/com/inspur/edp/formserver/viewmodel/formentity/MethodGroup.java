package com.inspur.edp.formserver.viewmodel.formentity;

import lombok.Getter;

import java.util.List;

@Getter
public class MethodGroup {

    private String id;
    private String methodName;
    private String methodCode;
    private List<MethodParam> methodParam;

    public void setId(String id) {
        this.id = id;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public void setMethodCode(String methodCode) {
        this.methodCode = methodCode;
    }

    public void setMethodParam(List<MethodParam> methodParam) {
        this.methodParam = methodParam;
    }


}
