package com.inspur.edp.formserver.viewmodel.action;

import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;

import java.io.Serializable;

/**
 * The Definition Of The View Model Action Result
 *
 * @ClassName: VMActionResult
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VMActionResult implements Cloneable, Serializable {
    ///#region 属性

    /**
     * 类型名称
     */
    private String privateTypeName;

    public final String getTypeName() {
        return privateTypeName;
    }

    public final void setTypeName(String value) {
        privateTypeName = value;
    }

    ///#endregion

    ///#region 方法

    /**
     * 克隆
     *
     * @return Action执行结果
     */
    public final VMActionResult clone() {
//		return MemberwiseClone();
        Object tempVar = null;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            throw new ViewModelException(e);
        }
        VMActionResult obj = (VMActionResult) ((tempVar instanceof ActionFormatParameter) ? tempVar : null);

        return obj;

        ///#endregion
    }
}