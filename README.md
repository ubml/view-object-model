# view-object-model

#### 介绍
在DDD领域驱动设计中，BFF（Backend For Frontend）层作为应用层，适配特定的应用场景，将领域层的内容进行封装（可以是对领域层的精简，也可以组装多个领域层对象），提供服务给其他微服务单元或者前端UI使用。
视图对象模型（View Object ，简称VO）作为BFF层的模型，承载了特定业务场景下的功能的业务逻辑和数据结构：
    
    -数据结构包含了一主多从多个节点，每个节点下面包含了多个字段以及节点之间的关联定义。
    -业务逻辑包括了与特定前端UI相关的处理逻辑，包括了内置的CRUD操作、引入的业务实体（BE）操作、自定义操作等。

![alt 视图对象模型结构示意图](figures/view-object-model-struct.png)

#### 视图对象与业务实体关系
根据不同的界面需要，视图对象（ VO）与业务实体（BE）有两种关系：

    -对于一个BE，根据不同的界面定义多个VO，VO是BE结构的子集。
    -VO组合多个BE，对多个BE进行编排组装。

![alt 视图对象与业务实体关系](figures/view-object-business-entity-relation.png)

#### 视图对象和前端UI关系
视图对象作为服务于前端的后端，每一个前端界面（表单、帮助、手工开发界面），都对应一个视图对象，视图对象上包含了所有前端界面需要的数据结构和自定义操作。

![alt 视图对象与全段UI关系](figures/view-object-ui-relation.png)  

### 约束
**开发语言：**  
Java，开发框架SpringBoot  
**依赖仓库：**  
   底层开发框架相关：
[caf-framework](https://gitee.com/ubml/caf-framework)、
[caf-boot](https://gitee.com/ubml/caf-boot)  
元数据基础框架：
[metadata-common](https://gitee.com/ubml/metadata-common)、
[metadata-service](https://gitee.com/ubml/metadata-service)、
[metadata-service-dev](https://gitee.com/ubml/metadata-service-dev)  
数据库对象相关：
[database-object-model](https://gitee.com/ubml/database-object-model)、
[database-object-service](https://gitee.com/ubml/database-object-service)  
业务实体：
[business-entity-model](https://gitee.com/ubml/business-entity-model)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

