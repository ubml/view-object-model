package com.inspur.edp.formserver.voextendinfo.server.core.config;


import com.inspur.edp.formserver.vmmanager.extendinfo.cache.GspVoExtendInfoCacheManager;
import com.inspur.edp.formserver.vmmanager.extendinfo.repository.GspVoExtendInfoRepository;
import com.inspur.edp.formserver.voextendinfo.server.api.GspVoExtendInfoRpcService;
import com.inspur.edp.formserver.voextendinfo.server.core.rpcserviceimp.GspVoExtendInfoRpcServiceImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GspVoExtendInfoRpcConfig {
    @Bean("com.inspur.edp.formserver.voextendinfo.server.core.config.GspVoExtendInfoRpcConfig.getVoExtendInfoRpcService")
    public GspVoExtendInfoRpcService getVoExtendInfoRpcService(
            GspVoExtendInfoRepository voExtendInfoRepository,
            GspVoExtendInfoCacheManager gspVoExtendInfoCacheManager) {
        return new GspVoExtendInfoRpcServiceImp(voExtendInfoRepository, gspVoExtendInfoCacheManager);
    }

}
