package com.inspur.edp.formserver.voextendinfo.server.api;

import com.inspur.edp.formserver.viewmodel.extendinfo.entity.GspVoExtendInfo;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;

import java.util.List;


@GspServiceBundle(applicationName = "runtime", serviceUnitName = "Lcm", serviceName = "GspVoExtendInfoRpcService")
public interface GspVoExtendInfoRpcService {
    /**
     * 根据ID获取某条Vo扩展信息
     *
     * @param id
     * @return
     */
    GspVoExtendInfo getVoExtendInfo(@RpcParam(paramName = "id") String id);

    /**
     * 根据configId获取某条Vo扩展信息
     *
     * @param configId
     * @return
     */
    GspVoExtendInfo getVoExtendInfoByConfigId(@RpcParam(paramName = "configId") String configId);

    /**
     * 获取所有BE扩展信息
     *
     * @return
     */
    List<GspVoExtendInfo> getVoExtendInfos();

    /**
     * 根据BEId获取某条VoID
     *
     * @param beId
     * @return
     */
    List<GspVoExtendInfo> getVoId(@RpcParam(paramName = "beId") String beId);

    /**
     * 保存
     *
     * @param infos
     */
    void saveGspVoExtendInfos(List<GspVoExtendInfo> infos);

    /**
     * 根据id删除数据
     *
     * @param id
     */
    void deleteVoExtendInfo(String id);
}
