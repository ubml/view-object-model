package com.inspur.edp.formserver.viewmodel.core.addComponentsByFormFeatures;

public class AddComponentsErrorCodes {
    public final static String GSP_VIEWOBJECT_ADDCOMP_1001 = "GSP_VIEWOBJECT_ADDCOMP_1001";
    public final static String GSP_VIEWOBJECT_ADDCOMP_1002 = "GSP_VIEWOBJECT_ADDCOMP_1002";
}
