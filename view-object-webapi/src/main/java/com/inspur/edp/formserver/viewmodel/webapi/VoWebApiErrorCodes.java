/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.formserver.viewmodel.webapi;

public class VoWebApiErrorCodes {
    public final static String GSP_VIEWOBJECT_WEBAPI_1001 = "GSP_VIEWOBJECT_WEBAPI_1001";
    public final static String GSP_VIEWOBJECT_WEBAPI_1002 = "GSP_VIEWOBJECT_WEBAPI_1002";
    public final static String GSP_VIEWOBJECT_WEBAPI_1003 = "GSP_VIEWOBJECT_WEBAPI_1003";
    public final static String GSP_VIEWOBJECT_WEBAPI_1004 = "GSP_VIEWOBJECT_WEBAPI_1004";
    public final static String GSP_VIEWOBJECT_WEBAPI_1005 = "GSP_VIEWOBJECT_WEBAPI_1005";
    public final static String GSP_VIEWOBJECT_WEBAPI_1006 = "GSP_VIEWOBJECT_WEBAPI_1006";
    public final static String GSP_VIEWOBJECT_WEBAPI_1007 = "GSP_VIEWOBJECT_WEBAPI_1007";
    public final static String GSP_VIEWOBJECT_WEBAPI_1008 = "GSP_VIEWOBJECT_WEBAPI_1008";
    public final static String GSP_VIEWOBJECT_WEBAPI_1009 = "GSP_VIEWOBJECT_WEBAPI_1009";
    public final static String GSP_VIEWOBJECT_WEBAPI_1010 = "GSP_VIEWOBJECT_WEBAPI_1010";
    public final static String GSP_VIEWOBJECT_WEBAPI_1011 = "GSP_VIEWOBJECT_WEBAPI_1011";
    public final static String GSP_VIEWOBJECT_WEBAPI_1012 = "GSP_VIEWOBJECT_WEBAPI_1012";
    public final static String GSP_VIEWOBJECT_WEBAPI_1013 = "GSP_VIEWOBJECT_WEBAPI_1013";
    public final static String GSP_VIEWOBJECT_WEBAPI_1014 = "GSP_VIEWOBJECT_WEBAPI_1014";
    public final static String GSP_VIEWOBJECT_WEBAPI_1015 = "GSP_VIEWOBJECT_WEBAPI_1015";
    public final static String GSP_VIEWOBJECT_WEBAPI_1016 = "GSP_VIEWOBJECT_WEBAPI_1016";
    public final static String GSP_VIEWOBJECT_WEBAPI_1017 = "GSP_VIEWOBJECT_WEBAPI_1017";
    public final static String GSP_VIEWOBJECT_WEBAPI_1018 = "GSP_VIEWOBJECT_WEBAPI_1018";
    public final static String GSP_VIEWOBJECT_WEBAPI_1019 = "GSP_VIEWOBJECT_WEBAPI_1019";
    public final static String GSP_VIEWOBJECT_WEBAPI_1020 = "GSP_VIEWOBJECT_WEBAPI_1020";
    public final static String GSP_VIEWOBJECT_WEBAPI_1021 = "GSP_VIEWOBJECT_WEBAPI_1021";
    public final static String GSP_VIEWOBJECT_WEBAPI_1022 = "GSP_VIEWOBJECT_WEBAPI_1022";
    public final static String GSP_VIEWOBJECT_WEBAPI_1023 = "GSP_VIEWOBJECT_WEBAPI_1023";
    public final static String GSP_VIEWOBJECT_WEBAPI_1024 = "GSP_VIEWOBJECT_WEBAPI_1024";
    public final static String GSP_VIEWOBJECT_WEBAPI_1025 = "GSP_VIEWOBJECT_WEBAPI_1025";
    public final static String GSP_VIEWOBJECT_WEBAPI_1026 = "GSP_VIEWOBJECT_WEBAPI_1026";
    public final static String GSP_VIEWOBJECT_WEBAPI_1027 = "GSP_VIEWOBJECT_WEBAPI_1027";
    public final static String GSP_VIEWOBJECT_WEBAPI_1028 = "GSP_VIEWOBJECT_WEBAPI_1028";
    public final static String GSP_VIEWOBJECT_WEBAPI_1029 = "GSP_VIEWOBJECT_WEBAPI_1029";
    public final static String GSP_VIEWOBJECT_WEBAPI_1030 = "GSP_VIEWOBJECT_WEBAPI_1030";
    public final static String GSP_VIEWOBJECT_WEBAPI_1031 = "GSP_VIEWOBJECT_WEBAPI_1031";
    public final static String GSP_VIEWOBJECT_WEBAPI_1032 = "GSP_VIEWOBJECT_WEBAPI_1032";
    public final static String GSP_VIEWOBJECT_WEBAPI_1033 = "GSP_VIEWOBJECT_WEBAPI_1033";
    public final static String GSP_VIEWOBJECT_WEBAPI_1034 = "GSP_VIEWOBJECT_WEBAPI_1034";
    public final static String GSP_VIEWOBJECT_WEBAPI_1035 = "GSP_VIEWOBJECT_WEBAPI_1035";
}