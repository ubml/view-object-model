package com.inspur.edp.formserver.viewmodel.core.buildformformat;

import com.inspur.edp.formserver.viewmodel.core.addComponentsByFormFeatures.BffAddComponentsByFormFeaturesServiceImpl;
import com.inspur.edp.formserver.vmapi.formconfig.VoBuildFormFormatService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.lcm.metadata.core.config.ServiceConfiguration;
import com.inspur.edp.metadata.businesstype.api.MdBizTypeMappingService;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.edp.formserver.viewmodel.core.buildformformat.VoBuildFormFormatConfig")
public class VoBuildFormFormatConfig {

    public MetadataService getMetadataService() {
        return new ServiceConfiguration().createMetadataService();
    }

    @Bean("com.inspur.edp.formserver.viewmodel.core.addComponentsByFormFeatures.BffAddComponentsByFormFeaturesConfiguration.getBefWebApi")
    public BffAddComponentsByFormFeaturesServiceImpl getBffWebApi() {
        return new BffAddComponentsByFormFeaturesServiceImpl(getMetadataService());
    }

    @Bean("com.inspur.edp.formserver.viewmodel.core.addComponentsByFormFeatures.BffAddComponentsByFormFeaturesConfiguration.addComponentsByFormFeaturesEndPoint")
    public RESTEndpoint addComponentsByFormFeaturesEndPoint(BffAddComponentsByFormFeaturesServiceImpl generateService) {
        return new RESTEndpoint(
                "/dev/main/v1.0/addComponentsByFormFeatures",
                generateService
        );
    }

    @Bean("com.inspur.edp.formserver.vmmanager.config.VMManagerConfig.BuildFormFormat()")
    public VoBuildFormFormatService getBuildFormFormat(MetadataService metadataService, MdBizTypeMappingService mdBizTypeMappingService) {
        return new VoBuildFormFormatServiceImpl(metadataService, mdBizTypeMappingService);
    }

}
