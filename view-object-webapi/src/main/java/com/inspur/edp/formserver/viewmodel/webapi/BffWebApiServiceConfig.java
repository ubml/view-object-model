package com.inspur.edp.formserver.viewmodel.webapi;

import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BffWebApiServiceConfig {
    @Bean
    public ViewModelController getBffWebApi() {
        return new ViewModelController();
    }

    @Bean
    public RESTEndpoint getBffWebApiServiceEndpoint(ViewModelController genService) {
        return new RESTEndpoint(
                "/dev/main/v1.0/viewmodel",
                genService
        );
    }
}
