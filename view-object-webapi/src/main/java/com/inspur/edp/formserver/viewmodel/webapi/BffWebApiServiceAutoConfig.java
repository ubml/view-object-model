package com.inspur.edp.formserver.viewmodel.webapi;

import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BffWebApiServiceAutoConfig {
    @Bean
    public VMCodeGenController testBffWebApi() {
        return new VMCodeGenController();
    }

    @Bean
    public RESTEndpoint getBffWebApiEndpoint(VMCodeGenController genService) {
        return new RESTEndpoint(
                "/dev/main/v1.0/bff",
                genService
        );
    }
}
