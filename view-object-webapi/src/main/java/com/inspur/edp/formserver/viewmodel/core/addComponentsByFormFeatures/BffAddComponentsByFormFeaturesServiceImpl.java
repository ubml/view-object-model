package com.inspur.edp.formserver.viewmodel.core.addComponentsByFormFeatures;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.bemgrcomponent.BizMgrActionParamCollection;
import com.inspur.edp.bef.bizentity.operation.bemgrcomponent.BizMgrActionParameter;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizReturnValue;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizCollectionParameterType;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterMode;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterType;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.formserver.viewmodel.GspViewModel;
import com.inspur.edp.formserver.viewmodel.action.MappedBizAction;
import com.inspur.edp.formserver.viewmodel.action.MappedCdpAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelAction;
import com.inspur.edp.formserver.viewmodel.action.ViewModelActionType;
import com.inspur.edp.formserver.viewmodel.action.mappedbiz.MappedBizActionParameter;
import com.inspur.edp.formserver.viewmodel.action.mappedbiz.MappedBizActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameter;
import com.inspur.edp.formserver.viewmodel.action.mappedcdp.MappedCdpActionParameterCollection;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParActualValue;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelParActualValueType;
import com.inspur.edp.formserver.viewmodel.action.viewmodelbase.ViewModelReturnValue;
import com.inspur.edp.formserver.viewmodel.collection.VMActionCollection;
import com.inspur.edp.formserver.viewmodel.common.MappingType;
import com.inspur.edp.formserver.viewmodel.common.VMCollectionParameterType;
import com.inspur.edp.formserver.viewmodel.common.VMParameterMode;
import com.inspur.edp.formserver.viewmodel.common.VMParameterType;
import com.inspur.edp.formserver.viewmodel.common.ViewModelMapping;
import com.inspur.edp.formserver.viewmodel.core.buildformformat.FormFormatErrorCodes;
import com.inspur.edp.formserver.viewmodel.exception.ViewModelException;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataHeader;
import com.inspur.edp.lcm.metadata.api.entity.MetadataReference;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.exception.ExceptionLevel;


import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.UUID;

public class BffAddComponentsByFormFeaturesServiceImpl {

    private final MetadataService metadataService;

    public BffAddComponentsByFormFeaturesServiceImpl(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    @Path("addComponentMethod")
    @PUT
    public void addComponentMethod(String info) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(info);
            String voId = node.get("voId").textValue();
            String dataRule = node.get("dataRule").textValue();
            String projectPath = node.get("projectPath").textValue();
            addComponentMethods(voId, dataRule, projectPath);
        } catch (JsonProcessingException e) {
            throw new ViewModelException(AddComponentsErrorCodes.GSP_VIEWOBJECT_ADDCOMP_1001, null, ExceptionLevel.Error, false);
        }
    }

    /**
     * 根据传入的表单特性标识添加构件方法
     *
     * @param voId-VO元数据ID
     * @param dataRule-表单特性标识
     * @param projectPath-工程路径
     */
    private void addComponentMethods(String voId, String dataRule, String projectPath) {
        if (dataRule.length() != 3) {
            throw new ViewModelException(AddComponentsErrorCodes.GSP_VIEWOBJECT_ADDCOMP_1002, null, ExceptionLevel.Error, false);
        }
        for (int i = 0; i < dataRule.length(); i++) {
            char bit = dataRule.charAt(i);
            if (bit == '1') {
                switch (i) {
                    case 0:
                        addImport(voId, projectPath);
                        break;
                    case 1:
                        checkPathHierarchyType(voId, projectPath);
                        break;
                    case 2:
                        addAttachmentComp(voId, projectPath);
                        break;
                }
            }
        }
    }

    /**
     * 检测VO上是否有导入导出动作，没有添加，无不做处理
     *
     * @param voId-VO元数据ID
     * @param projectPath-工程路径
     */
    private void addImport(String voId, String projectPath) {
        GspMetadata metadata = metadataService.getRefMetadata(projectPath, voId);
        GspViewModel voMetadata = (GspViewModel) metadata.getContent();
        if (checkActionOnVo(voMetadata.getActions(), "DataExportVMAction")) {
            addExportAction(metadata, voMetadata);
        }
        if (checkActionOnVo(voMetadata.getActions(), "DataImportVMAction")) {
            addImportAction(metadata, voMetadata);
        }
        metadata.setContent(voMetadata);
        metadataService.saveMetadata(metadata, metadata.getRelativePath() + "/" + metadata.getHeader().getFileName());
    }

    /**
     * VO上添加数据导入构件
     *
     * @param metadata-元数据，为了往header里添加依赖信息
     * @param voMetadata-VO元数据
     */
    private void addImportAction(GspMetadata metadata, GspViewModel voMetadata) {
        //VO增加对应refs
        MetadataReference importReference = createImportVoRefHeader("f417ce27-0680-486c-b88f-232a04557eb9",
                "DataImportVMAction", "数据导入构件");
        metadata.getRefs().add(importReference);
        //创建数据导入动作
        MappedCdpAction importAction = createImportVoAction("DataImportVMAction", "数据导入动作");
        //添加参数集合
        addImportParameter(importAction, "dataImportContext", "数据导入上下文");
        //添加返回值
        addImportVoActionReturnValue(importAction, "dataImportContext");
        voMetadata.getActions().add(importAction);
    }

    /**
     * VO上添加数据导出构件
     *
     * @param metadata-元数据，为了往header里添加依赖信息
     * @param voMetadata-VO元数据
     */
    private void addExportAction(GspMetadata metadata, GspViewModel voMetadata) {
        //VO增加对应refs
        MetadataReference exportReference = createImportVoRefHeader("e3978416-a258-4a1f-b1f2-5416f63638e0",
                "DataExportVMAction", "数据导出构件");
        metadata.getRefs().add(exportReference);
        //创建数据导出动作
        MappedCdpAction exportAction = createImportVoAction("DataExportVMAction", "数据导出动作");
        //添加参数集合
        addImportParameter(exportAction, "exportContext", "数据导出上下文");
        //添加返回值
        addImportVoActionReturnValue(exportAction, "exportContext");
        voMetadata.getActions().add(exportAction);
    }

    /**
     * 检测VO上是否有对应的分级码或父节点业务字段，有则添加对应的构件方法
     *
     * @param voId-VO元数据ID
     * @param projectPath-工程路径
     */
    private void checkPathHierarchyType(String voId, String projectPath) {
        //获取VO元数据
        GspMetadata metadata = metadataService.getRefMetadata(projectPath, voId);
        GspViewModel voMetadata = (GspViewModel) metadata.getContent();
        String beId = voMetadata.getMapping().getTargetMetadataId();
        ArrayList<IGspCommonElement> eles = voMetadata.getAllElementList(true);
        for (IGspCommonElement ele : eles) {
            if ("分级码分级信息".equals(ele.getUdtName())) {
                addPathHierarchyOnBe(beId, metadata, projectPath);
            } else if ("父节点分级信息".equals(ele.getUdtName())) {
                addPathHierarchyOnBeWithParent(beId, metadata, projectPath);
            }
        }
    }

    /**
     * BE上添加分级码构件方法
     *
     * @param beId-BE元数据ID
     * @param metadata-VO元数据，后续往直接VO添加方法传参
     * @param projectPath-工程路径
     */
    private void addPathHierarchyOnBe(String beId, GspMetadata metadata, String projectPath) {
        // 获取BE元数据
        GspMetadata beMetadata = metadataService.getRefMetadata(projectPath, beId);
        //获取BE
        GspBusinessEntity be = (GspBusinessEntity) beMetadata.getContent();
        String siblingActionId;
        String childLayerActionId;
        if (checkActionOnBe(be.getBizMgrActions(), "PathHierarchyCreateSibling")) {
            siblingActionId = addSiblingActionOnBe(beMetadata, be);
        } else {
            siblingActionId = getActionIdOnBe(be.getBizMgrActions(), "PathHierarchyCreateSibling");
        }
        if (checkActionOnBe(be.getBizMgrActions(), "PathHierarchyCreateChildLayer")) {
            childLayerActionId = addChildLayerActionOnBe(beMetadata, be);
        } else {
            childLayerActionId = getActionIdOnBe(be.getBizMgrActions(), "PathHierarchyCreateChildLayer");
        }
        beMetadata.setContent(be);
        metadataService.saveMetadata(beMetadata, beMetadata.getRelativePath() + "/" + beMetadata.getHeader().getFileName());
        addPathHierarchyOnVo(metadata, beId, childLayerActionId, siblingActionId);
    }

    /**
     * BE上添加分级码分级方式新增同级构件方法
     *
     * @param metadata-元数据，实际对应的BE，为了向header内添加依赖信息
     * @param be-BE元数据
     * @return 分级码分级方式新增同级的方法ID，为了与VO上对应构件的Mapping内对应
     */
    private String addSiblingActionOnBe(GspMetadata metadata, GspBusinessEntity be) {
        // 分级码新增同级ref创建
        MetadataReference siblingReference = createRefHeader("344519d9-b271-4aa0-8800-48a8762fbe20",
                "PathHierarchyCreateSibling", "分级码分级方式新增同级");
        metadata.getRefs().add(siblingReference);
        // 新增同级动作
        BizMgrAction createSiblingAction = createBizMgrAction("PathHierarchyCreateSibling",
                "分级码分级方式新增同级", "344519d9-b271-4aa0-8800-48a8762fbe20");
        // 添加参数
        addPathHierarchyParameter(createSiblingAction, "b6f39c6d-f42b-4a3e-8bb5-8939b56f4604", "当前焦点dataID");
        //添加返回值
        addPathHierarchyReturnValue(createSiblingAction, "fb86619c-0c76-40e6-998b-2d0c5514d701", "新增的同级树节点信息");
        be.getBizMgrActions().add(createSiblingAction);
        return createSiblingAction.getID();
    }

    /**
     * BE上添加分级码分级方式新增子级构件方法
     *
     * @param metadata-元数据，实际对应的BE，为了向header内添加依赖信息
     * @param be-BE元数据
     * @return 分级码分级方式新增子级方法的ID，为了与VO上对应构件的Mapping内对应
     */
    private String addChildLayerActionOnBe(GspMetadata metadata, GspBusinessEntity be) {
        // 分级码新增子级ref创建
        MetadataReference childLayerReference = createRefHeader("2f446058-90be-4cfb-8870-580b82dbc991",
                "PathHierarchyCreateChildLayer", "分级码分级方式新增子级");
        metadata.getRefs().add(childLayerReference);
        // 新增子级动作
        BizMgrAction createChildLayerAction = createBizMgrAction("PathHierarchyCreateChildLayer",
                "分级码分级方式新增子级", "2f446058-90be-4cfb-8870-580b82dbc991");
        //添加参数
        addPathHierarchyParameter(createChildLayerAction, "7606bff4-5531-47ce-bd38-8e14c13339f4", "要新增子级的父节点dataID");
        //添加返回值
        addPathHierarchyReturnValue(createChildLayerAction, "8c083897-3dfa-44a2-94dd-b21f65b4671d", "新增的子级树节点信息");
        be.getBizMgrActions().add(createChildLayerAction);
        return createChildLayerAction.getID();
    }

    /**
     * 检测VO上是否有分级码构件方法并调用添加
     *
     * @param voMetadata-元数据，实际对应的VO，为了向header内添加依赖信息
     * @param beId-BE的ID,VO上构件方法的Mapping需要
     * @param actionId1-新增子级方法的ID，为了与BE上对应构件的Mapping内对应
     * @param actionId2-新增同级方法的ID，为了与BE上对应构件的Mapping内对应
     */
    private void addPathHierarchyOnVo(GspMetadata voMetadata, String beId, String actionId1, String actionId2) {
        GspViewModel vo = (GspViewModel) voMetadata.getContent();
        if (checkActionOnVo(vo.getActions(), "PathHierarchyCreateChildLayer")) {
            addChildLayerActionOnVo(vo, beId, actionId1);
        }
        if (checkActionOnVo(vo.getActions(), "PathHierarchyCreateSibling")) {
            addSiblingActionOnVo(vo, beId, actionId2);
        }
        voMetadata.setContent(vo);
        metadataService.saveMetadata(voMetadata, voMetadata.getRelativePath() + "/" + voMetadata.getHeader().getFileName());
    }

    /**
     * VO上添加分级码新增同级构件方法
     *
     * @param vo-VO元数据
     * @param beId-BE的ID,VO上构件方法的Mapping需要
     * @param actionId-新增同级方法的ID，为了与BE上对应构件的Mapping内对应
     */
    private void addSiblingActionOnVo(GspViewModel vo, String beId, String actionId) {
        //创建新增同级方法
        MappedBizAction addSiblingAction = createVOAction("PathHierarchyCreateSibling", "分级码分级方式新增同级");
        //添加参数集合
        addPathHierarchyParameters(addSiblingAction, "当前焦点dataID");
        //添加返回值
        addVoActionReturnValue(addSiblingAction, "新增的同级树节点信息");
        //增加Mapping
        addVoActionMapping(addSiblingAction, beId, actionId);
        vo.getActions().add(addSiblingAction);
    }

    /**
     * VO上添加分级码新增子级构件方法
     *
     * @param vo-VO元数据
     * @param beId-BE的ID,VO上构件方法的Mapping需要
     * @param actionId-新增子级方法的ID，为了与BE上对应构件的Mapping内对应
     */
    private void addChildLayerActionOnVo(GspViewModel vo, String beId, String actionId) {
        //创建新增子级方法
        MappedBizAction addChildLayerAction = createVOAction("PathHierarchyCreateChildLayer", "分级码分级方式新增子级");
        //添加参数集合
        addPathHierarchyParameters(addChildLayerAction, "要新增子级的父节点dataID");
        //添加返回值
        addVoActionReturnValue(addChildLayerAction, "新增的子级树节点信息");
        //增加Mapping
        addVoActionMapping(addChildLayerAction, beId, actionId);
        vo.getActions().add(addChildLayerAction);
    }

    /**
     * BE上添加父节点构件方法
     *
     * @param beId-BE元数据ID
     * @param metadata-VO元数据，后续往直接VO添加方法传参
     * @param projectPath-工程路径
     */
    private void addPathHierarchyOnBeWithParent(String beId, GspMetadata metadata, String projectPath) {
        // 获取BE元数据
        GspMetadata beMetadata = metadataService.getRefMetadata(projectPath, beId);
        //获取BE
        GspBusinessEntity be = (GspBusinessEntity) beMetadata.getContent();
        String siblingActionId;
        String childLayerActionId;
        if (checkActionOnBe(be.getBizMgrActions(), "ParentHierarchyCreateSibling")) {
            siblingActionId = addSiblingActionOnBeWithParent(beMetadata, be);
        } else {
            siblingActionId = getActionIdOnBe(be.getBizMgrActions(), "ParentHierarchyCreateSibling");
        }
        if (checkActionOnBe(be.getBizMgrActions(), "ParentHierarchyCreateChildLayer")) {
            childLayerActionId = addChildLayerActionOnBeWithParent(beMetadata, be);
        } else {
            childLayerActionId = getActionIdOnBe(be.getBizMgrActions(), "ParentHierarchyCreateChildLayer");
        }
        beMetadata.setContent(be);
        metadataService.saveMetadata(beMetadata, beMetadata.getRelativePath() + "/" + beMetadata.getHeader().getFileName());
        addPathHierarchyOnVoWithParent(metadata, beId, childLayerActionId, siblingActionId);
    }

    /**
     * BE上添加父节点新增同级构件方法
     *
     * @param metadata-元数据，实际对应的BE，为了向header内添加依赖信息
     * @param be-BE元数据
     * @return -父节点分级方式新增同级方法的ID，为了与VO上对应构件的Mapping内对应
     */
    private String addSiblingActionOnBeWithParent(GspMetadata metadata, GspBusinessEntity be) {
        // 分级码新增同级ref创建
        MetadataReference siblingReference = createRefHeader("dac98710-78c1-42cd-8954-857f7117ff1e",
                "ParentHierarchyCreateSibling", "父节点分级方式新增同级");
        metadata.getRefs().add(siblingReference);
        // 新增同级动作
        BizMgrAction createSiblingAction = createBizMgrAction("ParentHierarchyCreateSibling",
                "父节点分级方式新增同级", "dac98710-78c1-42cd-8954-857f7117ff1e");
        // 添加参数
        addPathHierarchyParameter(createSiblingAction, "f0f500ef-871b-41d5-9eee-8aae3fad168d", "当前焦点dataID");
        //添加返回值
        addPathHierarchyReturnValue(createSiblingAction, "0c01f601-1ffe-4fc4-aae1-fb40848e1120", "新增的同级树节点信息");
        be.getBizMgrActions().add(createSiblingAction);
        return createSiblingAction.getID();
    }

    /**
     * BE上添加父节点新增子级构件方法
     *
     * @param metadata-元数据，实际对应的BE，为了向header内添加依赖信息
     * @param be-BE元数据
     * @return -父节点分级方式新增子级方法的ID，为了与VO上对应构件的Mapping内对应
     */
    private String addChildLayerActionOnBeWithParent(GspMetadata metadata, GspBusinessEntity be) {
        // 分级码新增子级ref创建
        MetadataReference childLayerReference = createRefHeader("bd5cd392-ce3d-4ad6-b3ce-2616a756a237",
                "ParentHierarchyCreateChildLayer", "父节点分级方式新增子级");
        metadata.getRefs().add(childLayerReference);
        // 新增子级动作
        BizMgrAction createChildLayerAction = createBizMgrAction("ParentHierarchyCreateChildLayer",
                "父节点分级方式新增子级", "bd5cd392-ce3d-4ad6-b3ce-2616a756a237");
        //添加参数
        addPathHierarchyParameter(createChildLayerAction, "6bc42b63-19ed-4d1c-9183-0784d32a7bb7", "要新增子级的父节点dataID");
        //添加返回值
        addPathHierarchyReturnValue(createChildLayerAction, "f9e3ecf9-d309-4344-a23d-ec43bc5eb2a0", "新增的子级树节点信息");
        be.getBizMgrActions().add(createChildLayerAction);
        return createChildLayerAction.getID();
    }

    /**
     * VO上检测原来是否有父节点对应构件方法，没有添加
     *
     * @param voMetadata-VO元数据
     * @param beId-BE的ID,VO上构件方法的Mapping需要
     * @param actionId1-新增子级方法的ID，为了与VO上对应构件的Mapping内对应
     * @param actionId2-新增同级方法的ID，为了与VO上对应构件的Mapping内对应
     */
    private void addPathHierarchyOnVoWithParent(GspMetadata voMetadata, String beId, String actionId1, String actionId2) {
        //获取VO元数据
        GspViewModel vo = (GspViewModel) voMetadata.getContent();
        if (checkActionOnVo(vo.getActions(), "ParentHierarchyCreateChildLayer")) {
            addChildLayerActionOnVoWithParent(vo, beId, actionId1);
        }
        if (checkActionOnVo(vo.getActions(), "ParentHierarchyCreateSibling")) {
            addSiblingActionOnVoWithParent(vo, beId, actionId2);
        }
        voMetadata.setContent(vo);
        metadataService.saveMetadata(voMetadata, voMetadata.getRelativePath() + "/" + voMetadata.getHeader().getFileName());
    }

    /**
     * VO上添加父节点分级方式新增同级构件
     *
     * @param vo-VO元数据
     * @param beId-BE的ID,VO上构件方法的Mapping需要
     * @param actionId-新增同级方法的ID，为了与VO上对应构件的Mapping内对应
     */
    private void addSiblingActionOnVoWithParent(GspViewModel vo, String beId, String actionId) {
        //创建新增同级方法
        MappedBizAction addSiblingAction = createVOAction("ParentHierarchyCreateSibling", "父节点分级方式新增同级");
        //添加参数集合
        addPathHierarchyParameters(addSiblingAction, "当前焦点dataID");
        //添加返回值
        addVoActionReturnValue(addSiblingAction, "新增的同级树节点信息");
        //增加Mapping
        addVoActionMapping(addSiblingAction, beId, actionId);
        vo.getActions().add(addSiblingAction);
    }

    /**
     * VO上添加父节点分级方式新增子级构件
     *
     * @param vo-VO元数据
     * @param beId-BE的ID,VO上构件方法的Mapping需要
     * @param actionId-新增子级方法的ID，为了与VO上对应构件的Mapping内对应
     */
    private void addChildLayerActionOnVoWithParent(GspViewModel vo, String beId, String actionId) {
        //创建新增子级方法
        MappedBizAction addChildLayerAction = createVOAction("ParentHierarchyCreateChildLayer", "父节点分级方式新增子级");
        //添加参数集合
        addPathHierarchyParameters(addChildLayerAction, "要新增子级的父节点dataID");
        //添加返回值
        addVoActionReturnValue(addChildLayerAction, "新增的子级树节点信息");
        //增加Mapping
        addVoActionMapping(addChildLayerAction, beId, actionId);
        vo.getActions().add(addChildLayerAction);
    }

    /**
     * 检测BE上是否有附件相关构件方法，没有则添加，并获取对应方法的ID给VO使用
     *
     * @param voId-VO的ID
     * @param projectPath-工程路径
     */
    private void addAttachmentComp(String voId, String projectPath) {
        //获取VO元数据
        GspMetadata metadata = metadataService.getRefMetadata(projectPath, voId);
        GspViewModel voMetadata = (GspViewModel) metadata.getContent();
        //获取BE的ID
        String beId = voMetadata.getMapping().getTargetMetadataId();
        GspMetadata beMetadata = metadataService.getRefMetadata(projectPath, beId);
        //获取BE
        GspBusinessEntity be = (GspBusinessEntity) beMetadata.getContent();
        String updateAttachmentActionId;
        String batchUploadAttachmentActionId;
        String updateAttachmentWithPropertyActionId;
        String batchUploadAttachmentWithPropertyActionId;
        if (checkActionOnBe(be.getBizMgrActions(), "UpdateAttachment")) {
            updateAttachmentActionId = updateAttachmentActionOnBe(beMetadata, be);
        } else {
            updateAttachmentActionId = getActionIdOnBe(be.getBizMgrActions(), "UpdateAttachment");
        }
        if (checkActionOnBe(be.getBizMgrActions(), "BatchUploadAttachment")) {
            batchUploadAttachmentActionId = batchUploadAttachmentActionOnBe(beMetadata, be);
        } else {
            batchUploadAttachmentActionId = getActionIdOnBe(be.getBizMgrActions(), "BatchUploadAttachment");
        }
        if (checkActionOnBe(be.getBizMgrActions(), "UpdateAttachmentWithProptyName")) {
            updateAttachmentWithPropertyActionId = updateAttachmentWithPropertyNameOnBe(beMetadata, be);
        } else {
            updateAttachmentWithPropertyActionId = getActionIdOnBe(be.getBizMgrActions(), "UpdateAttachmentWithProptyName");
        }
        if (checkActionOnBe(be.getBizMgrActions(), "BatchUploadAttachmentWithProptyName")) {
            batchUploadAttachmentWithPropertyActionId = batchUploadAttachmentWithPropertyNameOnBe(beMetadata, be);
        } else {
            batchUploadAttachmentWithPropertyActionId = getActionIdOnBe(be.getBizMgrActions(), "BatchUploadAttachmentWithProptyName");
        }
        beMetadata.setContent(be);
        metadataService.saveMetadata(beMetadata, beMetadata.getRelativePath() + "/" + beMetadata.getHeader().getFileName());
        //VO上增加方法
        addAttachmentOnVo(metadata, beId, updateAttachmentActionId, batchUploadAttachmentActionId, updateAttachmentWithPropertyActionId, batchUploadAttachmentWithPropertyActionId);
    }

    /**
     * BE添加更新附件信息方法
     *
     * @param metadata-元数据,实际对应BE，为了header内添加对应依赖
     * @param be-BE元数据
     * @return -更新附件信息方法ID
     */
    private String updateAttachmentActionOnBe(GspMetadata metadata, GspBusinessEntity be) {
        MetadataReference updateAttachmentReference = createRefHeader("b5ce8542-7084-4bf1-ab00-eda1412efc5f",
                "UpdateAttachment", "更新附件信息");
        metadata.getRefs().add(updateAttachmentReference);
        //创建更新附件信息动作
        BizMgrAction updateAttachmentAction = createBizMgrAction("UpdateAttachment", "更新附件信息", "b5ce8542-7084-4bf1-ab00-eda1412efc5f");
        //增加参数
        addAttachmentBizActionParameter(updateAttachmentAction, "a7ff2e59-dba8-4e6a-a5e6-cead4a98f856", "updateAttachInfo", "更新附件信息");
        //增加返回值
        addAttachmentBizActionReturnValue(updateAttachmentAction, "c17137b6-07d5-425c-b192-f4898316b059");
        be.getBizMgrActions().add(updateAttachmentAction);
        return updateAttachmentAction.getID();
    }

    /**
     * BE添加批量新增附件信息方法
     *
     * @param metadata-元数据,实际对应BE，为了header内添加对应依赖
     * @param be-BE元数据
     * @return -批量新增附件信息方法ID
     */
    private String batchUploadAttachmentActionOnBe(GspMetadata metadata, GspBusinessEntity be) {
        MetadataReference batchUploadAttachmentReference = createRefHeader("aa213406-922e-473b-997b-017a82321b4d",
                "BatchUploadAttachment", "批量新增附件信息");
        metadata.getRefs().add(batchUploadAttachmentReference);
        //创建批量新增附件信息动作
        BizMgrAction batchUploadAttachmentAction = createBizMgrAction("BatchUploadAttachment", "批量新增附件信息", "aa213406-922e-473b-997b-017a82321b4d");
        //增加参数
        addAttachmentBizActionParameter(batchUploadAttachmentAction, "3a97e1ad-6f14-4a09-99fa-e6d37c82b486", "batchUploadInfo", "批量上传参数信息");
        //增加返回值
        addAttachmentBizActionReturnValue(batchUploadAttachmentAction, "ad37d589-0416-4b58-8dc0-09aa6cbe3979");
        be.getBizMgrActions().add(batchUploadAttachmentAction);
        return batchUploadAttachmentAction.getID();
    }

    /**
     * BE添加根据附件字段标签更新附件信息
     *
     * @param metadata-元数据,实际对应BE，为了header内添加对应依赖
     * @param be-BE元数据
     * @return -根据附件字段标签更新附件信息方法ID
     */
    private String updateAttachmentWithPropertyNameOnBe(GspMetadata metadata, GspBusinessEntity be) {
        MetadataReference updateAttachmentWithProptyNameReference = createRefHeader("d4e5e036-08dc-469e-a9d9-18506bddb9fe",
                "UpdateAttachmentWithProptyName", "根据附件字段标签更新附件信息");
        metadata.getRefs().add(updateAttachmentWithProptyNameReference);
        //创建更新附件信息动作
        BizMgrAction updateAttachmentAction = createBizMgrAction("UpdateAttachmentWithProptyName", "根据附件字段标签更新附件信息", "d4e5e036-08dc-469e-a9d9-18506bddb9fe");
        //增加参数
        addAttachmentPropertyBizActionParameters(updateAttachmentAction);
        //增加返回值
        addAttachmentPropertyBizActionReturnValue(updateAttachmentAction);
        be.getBizMgrActions().add(updateAttachmentAction);
        return updateAttachmentAction.getID();
    }

    /**
     * BE添加根据附件字段标签新增附件信息
     *
     * @param metadata-元数据,实际对应BE，为了header内添加对应依赖
     * @param be-BE元数据
     * @return -根据附件字段标签新增附件信息方法ID
     */
    private String batchUploadAttachmentWithPropertyNameOnBe(GspMetadata metadata, GspBusinessEntity be) {
        MetadataReference batchUploadAttachmentWithPropertyNameReference = createRefHeader("c5f9be27-1995-4bbf-a482-4771e14150e6",
                "BatchUploadAttachmentWithProptyName", "根据附件字段标签新增附件信息");
        metadata.getRefs().add(batchUploadAttachmentWithPropertyNameReference);
        //创建更新附件信息动作
        BizMgrAction batchUploadAction = createBizMgrAction("BatchUploadAttachmentWithProptyName", "根据附件字段标签新增附件信息", "c5f9be27-1995-4bbf-a482-4771e14150e6");
        //增加参数
        batchUploadAttachmentPropertyBizActionParameters(batchUploadAction);
        //增加返回值
        addAttachmentBizActionReturnValue(batchUploadAction, "bea95edd-7e46-4bf9-b705-249aefdafc64");
        be.getBizMgrActions().add(batchUploadAction);
        return batchUploadAction.getID();
    }

    /**
     * VO上增加附件对应构件
     *
     * @param voMetadata-VO元数据
     * @param beId-BE元数据ID
     * @param actionId1-更新附件信息方法ID
     * @param actionId2-批量新增附件信息方法ID
     * @param actionId3-根据附件字段标签新增附件信息方法ID
     * @param actionId4-根据附件字段标签更新附件信息方法ID
     */
    private void addAttachmentOnVo(GspMetadata voMetadata, String beId, String actionId1, String actionId2, String actionId3, String actionId4) {
        GspViewModel vo = (GspViewModel) voMetadata.getContent();
        if (checkActionOnVo(vo.getActions(), "UpdateAttachment")) {
            updateAttachmentActionOnVo(vo, beId, actionId1);
        }
        if (checkActionOnVo(vo.getActions(), "BatchUploadAttachment")) {
            batchUploadAttachmentActionOnVo(vo, beId, actionId2);
        }
        if (checkActionOnVo(vo.getActions(), "UpdateAttachmentWithProptyName")) {
            updateAttachmentWithPropertyNameOnVo(vo, beId, actionId3);
        }
        if (checkActionOnVo(vo.getActions(), "BatchUploadAttachmentWithProptyName")) {
            batchUploadAttachmentWithPropertyNameOnVo(vo, beId, actionId4);
        }
        voMetadata.setContent(vo);
        metadataService.saveMetadata(voMetadata, voMetadata.getRelativePath() + "/" + voMetadata.getHeader().getFileName());
    }

    /**
     * VO上增加更新附件信息方法
     *
     * @param vo-VO元数据
     * @param beId-BE元数据ID
     * @param actionId-更新附件信息方法ID
     */
    private void updateAttachmentActionOnVo(GspViewModel vo, String beId, String actionId) {
        //增加更新附件信息方法
        MappedBizAction updateAttachmentAction = createVOAction("UpdateAttachment", "更新附件信息");
        //添加参数
        addAttachmentParameter(updateAttachmentAction, "updateAttachInfo", "更新附件信息");
        //添加返回值
        addVoActionReturnValue(updateAttachmentAction, null);
        //添加mapping
        addVoActionMapping(updateAttachmentAction, beId, actionId);
        vo.getActions().add(updateAttachmentAction);
    }

    /**
     * VO上增加批量上传附件信息方法
     *
     * @param vo-VO元数据
     * @param beId-BE元数据ID
     * @param actionId-批量上传附件信息方法ID
     */
    private void batchUploadAttachmentActionOnVo(GspViewModel vo, String beId, String actionId) {
        //增加批量上传附件信息方法
        MappedBizAction batchUploadAttachmentAction = createVOAction("BatchUploadAttachment", "批量新增附件信息");
        //添加参数
        addAttachmentParameter(batchUploadAttachmentAction, "batchUploadInfo", "批量上传参数信息");
        //添加返回值
        addVoActionReturnValue(batchUploadAttachmentAction, null);
        //添加mapping
        addVoActionMapping(batchUploadAttachmentAction, beId, actionId);
        vo.getActions().add(batchUploadAttachmentAction);
    }

    /**
     * VO上增加根据附件字段标签更新附件信息方法
     *
     * @param vo-VO元数据
     * @param beId-BE元数据ID
     * @param actionId-根据附件字段标签更新附件信息方法ID
     */
    private void updateAttachmentWithPropertyNameOnVo(GspViewModel vo, String beId, String actionId) {
        //增加批量上传附件信息方法
        MappedBizAction updateAttachmentWithProptyNameAction = createVOAction("UpdateAttachmentWithProptyName", "根据附件字段标签更新附件信息");
        //添加参数
        addUpdateAttachmentWithPropertyNameParameters(updateAttachmentWithProptyNameAction);
        //添加返回值
        addUpdateVoActionReturnValue(updateAttachmentWithProptyNameAction);
        //添加mapping
        addVoActionMapping(updateAttachmentWithProptyNameAction, beId, actionId);
        vo.getActions().add(updateAttachmentWithProptyNameAction);
    }

    /**
     * VO上增加根据附件字段标签新增附件信息方法
     *
     * @param vo-VO元数据
     * @param beId-BE元数据ID
     * @param actionId-根据附件字段标签新增附件信息方法ID
     */
    private void batchUploadAttachmentWithPropertyNameOnVo(GspViewModel vo, String beId, String actionId) {
        //增加批量上传附件信息方法
        MappedBizAction batchUploadAttachmentWithProptyNameAction = createVOAction("BatchUploadAttachmentWithProptyName", "根据附件字段标签新增附件信息");
        //添加参数
        addBatchUploadAttachmentWithPropertyNameParameters(batchUploadAttachmentWithProptyNameAction);
        //添加返回值
        addBatchUploadVoActionReturnValue(batchUploadAttachmentWithProptyNameAction);
        //添加mapping
        addVoActionMapping(batchUploadAttachmentWithProptyNameAction, beId, actionId);
        vo.getActions().add(batchUploadAttachmentWithProptyNameAction);
    }

    /**
     * 增加VO上引用的导入导出header依赖，其内的ID、code、name为固定值
     *
     * @param id-依赖信息ID
     * @param code-依赖信息code
     * @param name-依赖信息name
     * @return 返回添加对应依赖信息后的refs
     */
    private MetadataReference createImportVoRefHeader(String id, String code, String name) {
        MetadataReference reference = new MetadataReference();
        MetadataHeader header = new MetadataHeader();
        header.setId(id);
        header.setNameSpace("Inspur.GS.Gsp.Common.DataIE");
        header.setCode(code);
        header.setName(name);
        header.setType("VMComponent");
        header.setBizobjectID("DataIE");
        reference.setDependentMetadata(header);
        return reference;
    }

    /**
     * 增加VO上导入导出action，其内的code、name决定创建的是导入还是导出动作
     *
     * @param code-导入或导出动作编号
     * @param name-导入或导出动作名称
     * @return -返回对应导入导出动作方法构件
     */
    private MappedCdpAction createImportVoAction(String code, String name) {
        MappedCdpAction action = new MappedCdpAction();
        action.setID(UUID.randomUUID().toString());
        action.setCode(code);
        action.setName(name);
        action.setComponentName(code);
        action.setType(ViewModelActionType.VMAction);
        if ("DataExportVMAction".equals(code)) {
            action.setComponentEntityId("e3978416-a258-4a1f-b1f2-5416f63638e0");
        } else {
            action.setComponentEntityId("f417ce27-0680-486c-b88f-232a04557eb9");
        }
        action.setComponentPkgName("Inspur.GS.Gsp.Common.DataIE");
        action.setIsGenerateComponent(false);
        return action;
    }

    /**
     * 增加VO上导入导出action的参数
     *
     * @param mappedCdpAction-导入导出action构件
     * @param paramCode-参数编号
     * @param paramName-参数名称
     */
    private void addImportParameter(MappedCdpAction mappedCdpAction, String paramCode, String paramName) {
        MappedCdpActionParameterCollection cdpActionParameters = new MappedCdpActionParameterCollection();
        MappedCdpActionParameter cdpActionParameter = new MappedCdpActionParameter();
        if (("exportContext").equals(paramCode)) {
            cdpActionParameter.setID("b1409662-8359-4d2f-9479-a5b7582b0c94");
            cdpActionParameter.setClassName("com.inspur.edp.dataie.context.DataExportContext");
            cdpActionParameter.setDotnetClassName("Inspur.Gsp.Dip.DataIE.Component.DataExportContext");
        } else {
            cdpActionParameter.setID("3ec80a12-e803-4f68-8ccc-1611bf6d1c82");
            cdpActionParameter.setClassName("com.inspur.edp.dataie.context.DataImportContext");
            cdpActionParameter.setDotnetClassName("Inspur.Gsp.Dip.DataIE.Component.DataImportContext");
        }
        cdpActionParameter.setParamCode(paramCode);
        cdpActionParameter.setParamName(paramName);
        cdpActionParameter.setParameterType(VMParameterType.Custom);
        cdpActionParameter.setAssembly("Inspur.Gsp.Dip.DataIE.Component");
        //添加actualValue
        ViewModelParActualValue parActualValue = createParamActualValue();
        cdpActionParameter.setActualValue(parActualValue);
        cdpActionParameters.add(cdpActionParameter);
        mappedCdpAction.setParameters(cdpActionParameters);
    }

    /**
     * 增加VO上导入导出action的返回值
     *
     * @param mappedCdpAction-导入导出action构件
     * @param code-返回值编号
     */
    private void addImportVoActionReturnValue(MappedCdpAction mappedCdpAction, String code) {
        ViewModelReturnValue viewModelReturnValue = new ViewModelReturnValue();
        if (("exportContext").equals(code)) {
            viewModelReturnValue.setID("158ed4a5-0f72-4558-a4ce-0ecb200c5b78");
        } else {
            viewModelReturnValue.setID("d93a6028-2ef8-4abc-bc7e-d2150b6f2fdf");
        }
        viewModelReturnValue.setParameterType(VMParameterType.Custom);
        viewModelReturnValue.setAssembly("Inspur.Gsp.Dip.DataIE.Component");
        viewModelReturnValue.setClassName("com.inspur.edp.dataie.domain.DataIEResult");
        viewModelReturnValue.setDotnetClassName("Inspur.Gsp.Dip.DataIE.Component.DataIEResult");
        viewModelReturnValue.setMode(VMParameterMode.OUT);
        ViewModelParActualValue parActualValue = createParamActualValue();
        viewModelReturnValue.setActualValue(parActualValue);
        mappedCdpAction.setReturnValue(viewModelReturnValue);
    }

    /**
     * 增加BE上引用的组件header依赖，其内的ID、code、name为固定值,由传入值决定创建的是何种依赖
     *
     * @param id-依赖信息ID
     * @param code-依赖信息code
     * @param name-依赖信息name
     * @return -返回添加对应依赖信息后的refs
     */
    private MetadataReference createRefHeader(String id, String code, String name) {
        MetadataReference reference = new MetadataReference();
        MetadataHeader header = new MetadataHeader();
        header.setId(id);
        header.setNameSpace("Inspur.Gsp.Common.CommonCmp");
        header.setCode(code);
        header.setName(name);
        header.setType("BEMgrComponent");
        header.setBizobjectID("CommonCmp");
        reference.setDependentMetadata(header);
        return reference;
    }

    /**
     * 增加BE上引用的组件action，其内的code、name决定创建的是何种action
     *
     * @param code-action编号
     * @param name-action名称
     * @param componentId-action对应的构件ID
     * @return -返回对应方法的构件动作
     */
    private BizMgrAction createBizMgrAction(String code, String name, String componentId) {
        BizMgrAction action = new BizMgrAction();
        action.setID(UUID.randomUUID().toString());
        action.setCode(code);
        action.setName(name);
        action.setComponentId(componentId);
        action.setComponentName(code);
        action.setComponentPkgName("Inspur.Gsp.Common.CommonCmp");
        action.setIsGenerateComponent(false);
        return action;
    }

    /**
     * 增加BE上分级码相关方法的参数
     *
     * @param bizMgrAction-分级码相关方法
     * @param id-参数ID，为定值
     * @param paramDescription-参数描述，为定值
     */
    private void addPathHierarchyParameter(BizMgrAction bizMgrAction, String id, String paramDescription) {
        BizMgrActionParamCollection parameters = new BizMgrActionParamCollection();
        BizMgrActionParameter parameter = new BizMgrActionParameter();
        parameter.setID(id);
        parameter.setParamCode("dataID");
        parameter.setParamName("dataID");
        parameter.setParameterType(BizParameterType.String);
        parameter.setAssembly("mscorlib.dll");
        parameter.setNetClassName("System.String");
        parameter.setClassName("java.lang.String");
        parameter.setParamDescription(paramDescription);
        parameters.add(parameter);
        bizMgrAction.setParameters(parameters);
    }

    /**
     * 增加BE上分级码相关方法的返回值
     *
     * @param bizMgrAction-分级码相关方法
     * @param id-返回值ID，为定值
     * @param paramDescription-返回值描述，为定值
     */
    private void addPathHierarchyReturnValue(BizMgrAction bizMgrAction, String id, String paramDescription) {
        BizReturnValue returnValue = new BizReturnValue();
        returnValue.setID(id);
        returnValue.setParameterType(BizParameterType.Custom);
        returnValue.setAssembly("Inspur.Gsp.Cef.Entity");
        returnValue.setNetClassName("Inspur.Gsp.Cef.Entity.IEntityData");
        returnValue.setClassName("com.inspur.edp.cef.entity.entity.IEntityData");
        returnValue.setMode(BizParameterMode.OUT);
        returnValue.setParamDescription(paramDescription);
        bizMgrAction.setReturnValue(returnValue);
    }

    /**
     * 创建VO上的构件结构
     *
     * @param code-action编号
     * @param name-action名称
     * @return -返回创建的对应action构件，例如分级码新增同级构件
     */
    private MappedBizAction createVOAction(String code, String name) {
        MappedBizAction action = new MappedBizAction();
        action.setID(UUID.randomUUID().toString());
        action.setCode(code);
        action.setName(name);
        action.setComponentName(code);
        action.setType(ViewModelActionType.BEAction);
        return action;
    }

    /**
     * 增加VO上分级码相关方法的参数
     *
     * @param mappedBizAction-VO分级码相关方法
     * @param paramDescription-参数描述，为定值
     */
    private void addPathHierarchyParameters(MappedBizAction mappedBizAction, String paramDescription) {
        MappedBizActionParameterCollection bizActionParameters = new MappedBizActionParameterCollection();
        MappedBizActionParameter bizActionParameter = new MappedBizActionParameter();
        bizActionParameter.setID(UUID.randomUUID().toString());
        bizActionParameter.setParamCode("dataID");
        bizActionParameter.setParamName("dataID");
        bizActionParameter.setParameterType(VMParameterType.String);
        bizActionParameter.setAssembly("mscorlib.dll");
        bizActionParameter.setClassName("java.lang.String");
        bizActionParameter.setDotnetClassName("System.String");
        bizActionParameter.setParamDescription(paramDescription);
        //添加actualValue
        ViewModelParActualValue parActualValue = createParamActualValue();
        bizActionParameter.setActualValue(parActualValue);
        bizActionParameters.add(bizActionParameter);
        mappedBizAction.setParameters(bizActionParameters);
    }

    /**
     * 创建VO上分级码相关方法的ParamActualValue集合
     *
     * @return -返回创建的对应ParamActualValue集合
     */
    private ViewModelParActualValue createParamActualValue() {
        ViewModelParActualValue parActualValue = new ViewModelParActualValue();
        parActualValue.setEnable(false);
        parActualValue.setHasValue(false);
        parActualValue.setValue("");
        parActualValue.setValueType(ViewModelParActualValueType.Constant);
        return parActualValue;
    }

    /**
     * 增加VO上方法的返回值
     *
     * @param mappedBizAction-VO方法
     * @param paramDescription-返回值参数描述，为定值
     */
    private void addVoActionReturnValue(MappedBizAction mappedBizAction, String paramDescription) {
        ViewModelReturnValue viewModelReturnValue = new ViewModelReturnValue();
        viewModelReturnValue.setID(UUID.randomUUID().toString());
        viewModelReturnValue.setParameterType(VMParameterType.Custom);
        viewModelReturnValue.setAssembly("Inspur.Gsp.Cef.Entity");
        viewModelReturnValue.setClassName("com.inspur.edp.cef.entity.entity.IEntityData");
        viewModelReturnValue.setDotnetClassName("Inspur.Gsp.Cef.Entity.IEntityData");
        viewModelReturnValue.setMode(VMParameterMode.OUT);
        if (paramDescription != null) {
            viewModelReturnValue.setParamDescription(paramDescription);
        }
        ViewModelParActualValue parActualValue = createParamActualValue();
        viewModelReturnValue.setActualValue(parActualValue);
        mappedBizAction.setReturnValue(viewModelReturnValue);
    }

    /**
     * 增加VO上批量上传附件相关方法的返回值
     *
     * @param mappedBizAction-VO方法
     */
    private void addBatchUploadVoActionReturnValue(MappedBizAction mappedBizAction) {
        ViewModelReturnValue viewModelReturnValue = new ViewModelReturnValue();
        viewModelReturnValue.setID(UUID.randomUUID().toString());
        viewModelReturnValue.setParameterType(VMParameterType.Custom);
        viewModelReturnValue.setAssembly("Inspur.Gsp.Cef.Entity");
        viewModelReturnValue.setClassName("com.inspur.edp.cef.entity.entity.IEntityData");
        viewModelReturnValue.setDotnetClassName("Inspur.Gsp.Cef.Entity.IEntityData");
        viewModelReturnValue.setMode(VMParameterMode.OUT);
        viewModelReturnValue.setCollectionParameterType(VMCollectionParameterType.List);
        ViewModelParActualValue parActualValue = createParamActualValue();
        viewModelReturnValue.setActualValue(parActualValue);
        mappedBizAction.setReturnValue(viewModelReturnValue);
    }

    /**
     * 增加VO上更新附件相关方法的返回值
     *
     * @param mappedBizAction-VO方法
     */
    private void addUpdateVoActionReturnValue(MappedBizAction mappedBizAction) {
        ViewModelReturnValue viewModelReturnValue = new ViewModelReturnValue();
        viewModelReturnValue.setID(UUID.randomUUID().toString());
        viewModelReturnValue.setParameterType(VMParameterType.Custom);
        viewModelReturnValue.setAssembly("nope");
        viewModelReturnValue.setClassName("com.inspur.edp.cef.entity.entity.IEntityData");
        viewModelReturnValue.setDotnetClassName("nope");
        viewModelReturnValue.setMode(VMParameterMode.OUT);
        ViewModelParActualValue parActualValue = createParamActualValue();
        viewModelReturnValue.setActualValue(parActualValue);
        mappedBizAction.setReturnValue(viewModelReturnValue);
    }

    /**
     * 增加VO方法与BE方法之间的ID映射
     *
     * @param mappedBizAction-VO方法
     * @param targetMetadataId-BE的ID
     * @param targetObjId-BE方法的ID
     */
    private void addVoActionMapping(MappedBizAction mappedBizAction, String targetMetadataId, String targetObjId) {
        ViewModelMapping addChildLayerActionMapping = new ViewModelMapping();
        addChildLayerActionMapping.setMapType(MappingType.BizOperation);
        addChildLayerActionMapping.setTargetMetadataId(targetMetadataId);
        addChildLayerActionMapping.setTargetObjId(targetObjId);
        mappedBizAction.setMapping(addChildLayerActionMapping);
    }

    /**
     * BE附件方法添加对应方法参数
     *
     * @param bizMgrAction-BE方法
     * @param id-参数ID
     * @param paramCode-参数编号
     * @param paramName-参数名称
     */
    private void addAttachmentBizActionParameter(BizMgrAction bizMgrAction, String id, String paramCode, String paramName) {
        BizMgrActionParamCollection parameters = new BizMgrActionParamCollection();
        BizMgrActionParameter parameter = new BizMgrActionParameter();
        parameter.setID(id);
        parameter.setParamCode(paramCode);
        parameter.setParamName(paramName);
        parameter.setParameterType(BizParameterType.Custom);
        parameter.setAssembly("Inspur.Gsp.Common.CommonCmp");
        parameter.setNetClassName("Inspur.Gsp.Common.CommonCmp.Attachment.AttachInfo");
        parameter.setClassName("com.inspur.edp.common.component.attachment.AttachInfo");
        parameters.add(parameter);
        bizMgrAction.setParameters(parameters);
    }

    /**
     * 增加BE根据附件字段标签更新附件信息方法参数
     *
     * @param bizMgrAction-BE方法
     */
    private void addAttachmentPropertyBizActionParameters(BizMgrAction bizMgrAction) {
        BizMgrActionParamCollection parameters = new BizMgrActionParamCollection();

        BizMgrActionParameter updateParameter = new BizMgrActionParameter();
        updateParameter.setID("8fed7de9-e54d-4f60-82c3-0405ee6b83d4");
        updateParameter.setParamCode("updateAttachInfo");
        updateParameter.setParamName("更新附件信息");
        updateParameter.setParameterType(BizParameterType.Custom);
        updateParameter.setAssembly("nope");
        updateParameter.setNetClassName("nope");
        updateParameter.setClassName("com.inspur.edp.common.component.attachment.AttachInfo");

        BizMgrActionParameter propertyNameParameter = new BizMgrActionParameter();
        propertyNameParameter.setID("466b0a98-7ea7-4b23-8ea2-bfc120dc924b");
        propertyNameParameter.setParamCode("propertyName");
        propertyNameParameter.setParamName("附件业务字段标签");
        propertyNameParameter.setParameterType(BizParameterType.String);
        propertyNameParameter.setAssembly("mscorlib.dll");
        propertyNameParameter.setNetClassName("System.String");
        propertyNameParameter.setClassName("java.lang.String");

        parameters.add(updateParameter);
        parameters.add(propertyNameParameter);
        bizMgrAction.setParameters(parameters);
    }

    /**
     * 增加BE根据附件字段标签新增附件信息方法参数
     *
     * @param bizMgrAction-BE方法
     */
    private void batchUploadAttachmentPropertyBizActionParameters(BizMgrAction bizMgrAction) {
        BizMgrActionParamCollection parameters = new BizMgrActionParamCollection();

        BizMgrActionParameter batchUploadParameter = new BizMgrActionParameter();
        batchUploadParameter.setID("26fb5ca7-83bb-4c4b-b230-0e43e9581bb6");
        batchUploadParameter.setParamCode("batchUploadInfo");
        batchUploadParameter.setParamName("附件上传参数");
        batchUploadParameter.setParameterType(BizParameterType.Custom);
        batchUploadParameter.setAssembly("nope");
        batchUploadParameter.setNetClassName("nope");
        batchUploadParameter.setClassName("com.inspur.edp.common.component.attachment.AttachInfo");

        BizMgrActionParameter propertyNameParameter = new BizMgrActionParameter();
        propertyNameParameter.setID("58e41c3b-5f2a-451a-bae9-e1af32459a25");
        propertyNameParameter.setParamCode("propertyName");
        propertyNameParameter.setParamName("附件业务字段名称");
        propertyNameParameter.setParameterType(BizParameterType.String);
        propertyNameParameter.setAssembly("mscorlib.dll");
        propertyNameParameter.setNetClassName("System.String");
        propertyNameParameter.setClassName("java.lang.String");

        parameters.add(batchUploadParameter);
        parameters.add(propertyNameParameter);
        bizMgrAction.setParameters(parameters);
    }

    /**
     * 增加BE根据附件字段标签更新附件信息方法返回值
     *
     * @param bizMgrAction -BE方法
     */
    private void addAttachmentPropertyBizActionReturnValue(BizMgrAction bizMgrAction) {
        BizReturnValue returnValue = new BizReturnValue();
        returnValue.setID("9d09d1b3-59a2-4543-a886-67f6f05e5072");
        returnValue.setParameterType(BizParameterType.Custom);
        returnValue.setAssembly("nope");
        returnValue.setNetClassName("nope");
        returnValue.setClassName("com.inspur.edp.cef.entity.entity.IEntityData");
        returnValue.setMode(BizParameterMode.OUT);
        bizMgrAction.setReturnValue(returnValue);
    }

    /**
     * 增加BE附件方法返回值
     *
     * @param bizMgrAction-BE方法
     * @param id-返回值ID
     */
    private void addAttachmentBizActionReturnValue(BizMgrAction bizMgrAction, String id) {
        BizReturnValue returnValue = new BizReturnValue();
        returnValue.setID(id);
        returnValue.setParameterType(BizParameterType.Custom);
        returnValue.setCollectionParameterType(BizCollectionParameterType.List);
        returnValue.setAssembly("Inspur.Gsp.Cef.Entity");
        returnValue.setNetClassName("Inspur.Gsp.Cef.Entity.IEntityData");
        returnValue.setClassName("com.inspur.edp.cef.entity.entity.IEntityData");
        returnValue.setMode(BizParameterMode.OUT);
        bizMgrAction.setReturnValue(returnValue);
    }

    /**
     * 增加VO附件方法参数
     *
     * @param mappedBizAction-VO构件方法
     * @param paramCode-参数编号
     * @param paramName-参数名称
     */
    private void addAttachmentParameter(MappedBizAction mappedBizAction, String paramCode, String paramName) {
        MappedBizActionParameterCollection bizActionParameters = new MappedBizActionParameterCollection();
        MappedBizActionParameter bizActionParameter = new MappedBizActionParameter();
        bizActionParameter.setID(UUID.randomUUID().toString());
        bizActionParameter.setParamCode(paramCode);
        bizActionParameter.setParamName(paramName);
        bizActionParameter.setParameterType(VMParameterType.Custom);
        bizActionParameter.setAssembly("Inspur.Gsp.Common.CommonCmp");
        bizActionParameter.setClassName("com.inspur.edp.common.component.attachment.AttachInfo");
        bizActionParameter.setDotnetClassName("Inspur.Gsp.Common.CommonCmp.Attachment.AttachInfo");
        //添加actualValue
        ViewModelParActualValue parActualValue = createParamActualValue();
        bizActionParameter.setActualValue(parActualValue);
        bizActionParameters.add(bizActionParameter);
        mappedBizAction.setParameters(bizActionParameters);
    }

    /**
     * 增加VO根据附件字段标签新增附件信息方法参数
     *
     * @param mappedBizAction-VO构件方法
     */
    private void addBatchUploadAttachmentWithPropertyNameParameters(MappedBizAction mappedBizAction) {
        MappedBizActionParameterCollection bizActionParameters = new MappedBizActionParameterCollection();

        MappedBizActionParameter batchUploadActionParameter = new MappedBizActionParameter();
        batchUploadActionParameter.setID(UUID.randomUUID().toString());
        batchUploadActionParameter.setParamCode("batchUploadInfo");
        batchUploadActionParameter.setParamName("附件上传参数");
        batchUploadActionParameter.setParameterType(VMParameterType.Custom);
        batchUploadActionParameter.setAssembly("nope");
        batchUploadActionParameter.setClassName("com.inspur.edp.common.component.attachment.AttachInfo");
        batchUploadActionParameter.setDotnetClassName("nope");
        //添加actualValue
        ViewModelParActualValue parActualValue = createParamActualValue();
        batchUploadActionParameter.setActualValue(parActualValue);
        bizActionParameters.add(batchUploadActionParameter);

        MappedBizActionParameter propertyNameActionParameter = new MappedBizActionParameter();
        propertyNameActionParameter.setID(UUID.randomUUID().toString());
        propertyNameActionParameter.setParamCode("propertyName");
        propertyNameActionParameter.setParamName("附件业务字段名称");
        propertyNameActionParameter.setParameterType(VMParameterType.String);
        propertyNameActionParameter.setAssembly("mscorlib.dll");
        propertyNameActionParameter.setClassName("java.lang.String");
        propertyNameActionParameter.setDotnetClassName("System.String");
        //添加actualValue
        ViewModelParActualValue parActualValue1 = createParamActualValue();
        batchUploadActionParameter.setActualValue(parActualValue1);
        bizActionParameters.add(propertyNameActionParameter);

        mappedBizAction.setParameters(bizActionParameters);
    }

    /**
     * 增加VO根据附件字段标签更新附件信息方法参数
     *
     * @param mappedBizAction-VO构件方法
     */
    private void addUpdateAttachmentWithPropertyNameParameters(MappedBizAction mappedBizAction) {
        MappedBizActionParameterCollection bizActionParameters = new MappedBizActionParameterCollection();

        MappedBizActionParameter batchUploadActionParameter = new MappedBizActionParameter();
        batchUploadActionParameter.setID(UUID.randomUUID().toString());
        batchUploadActionParameter.setParamCode("updateAttachInfo");
        batchUploadActionParameter.setParamName("更新附件信息");
        batchUploadActionParameter.setParameterType(VMParameterType.Custom);
        batchUploadActionParameter.setAssembly("nope");
        batchUploadActionParameter.setClassName("com.inspur.edp.common.component.attachment.AttachInfo");
        batchUploadActionParameter.setDotnetClassName("nope");
        //添加actualValue
        ViewModelParActualValue parActualValue = createParamActualValue();
        batchUploadActionParameter.setActualValue(parActualValue);
        bizActionParameters.add(batchUploadActionParameter);

        MappedBizActionParameter propertyNameActionParameter = new MappedBizActionParameter();
        propertyNameActionParameter.setID(UUID.randomUUID().toString());
        propertyNameActionParameter.setParamCode("propertyName");
        propertyNameActionParameter.setParamName("附件业务字段标签");
        propertyNameActionParameter.setParameterType(VMParameterType.String);
        propertyNameActionParameter.setAssembly("mscorlib.dll");
        propertyNameActionParameter.setClassName("java.lang.String");
        propertyNameActionParameter.setDotnetClassName("System.String");
        //添加actualValue
        ViewModelParActualValue parActualValue1 = createParamActualValue();
        batchUploadActionParameter.setActualValue(parActualValue1);
        bizActionParameters.add(propertyNameActionParameter);

        mappedBizAction.setParameters(bizActionParameters);
    }

    /**
     * 检查VO上是否存在对应构件方法
     *
     * @param actions-VO构件方法集合
     * @param actionCode-要检查的构件方法编号
     * @return 存在返回false，不存在返回true
     */
    private boolean checkActionOnVo(VMActionCollection actions, String actionCode) {
        for (ViewModelAction action : actions) {
            if (actionCode.equals(action.getCode())) {
                return false;
            }
        }
        return true;
    }

    /**
     * 检查BE上是否存在对应方法
     *
     * @param actions-BE构件方法集合
     * @param actionCode-要检查的方法编号
     * @return -存在返回false，不存在返回true
     */
    private boolean checkActionOnBe(BizMgrActionCollection actions, String actionCode) {
        for (BizOperation action : actions) {
            if (actionCode.equals(action.getCode())) {
                return false;
            }
        }
        return true;
    }

    /**
     * 获取BE上对应方法的ID
     *
     * @param actions-BE构件方法集合
     * @param actionCode-要获取的方法编号
     * @return -方法ID
     */
    private String getActionIdOnBe(BizMgrActionCollection actions, String actionCode) {
        String id = null;
        for (BizOperation action : actions) {
            if (actionCode.equals(action.getCode())) {
                id = action.getID();
            }
        }
        return id;
    }
}

