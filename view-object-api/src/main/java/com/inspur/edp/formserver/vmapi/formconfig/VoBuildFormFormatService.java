package com.inspur.edp.formserver.vmapi.formconfig;

import com.inspur.edp.formserver.viewmodel.formentity.VoFormModel;

public interface VoBuildFormFormatService {
    /**
     * 设计时在表单保存以及推送到工作流时接受表单传入的界面规则转换为流程格式推送至流程设计
     *
     * @param voFormModel（界面规则）
     * @param voId（表单对应的VOID）
     * @param bizCategory（业务种类ID）
     */
    void buildFormFormat(VoFormModel voFormModel, String voId, String bizCategory);
}
